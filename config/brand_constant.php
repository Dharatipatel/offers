<?php

$apiPath = url("/") . "/api/";
$appPath = url("/") . "/";
return [
    'script' => [
        'jquery' => $appPath . 'resources/assets/brand/vendor/jquery/jquery.min.js',
        'popper' => $appPath . 'resources/assets/brand/vendor/popper.js/umd/popper.min.js',
        'bootstrap' => $appPath . 'resources/assets/brand/vendor/bootstrap/js/bootstrap.min.js',
        'jquerycookie' => $appPath . 'resources/assets/brand/vendor/jquery.cookie/jquery.cookie.js',
        'chart' => $appPath . 'resources/assets/brand/vendor/chart.js/Chart.min.js',
        'validate' => $appPath . 'resources/assets/brand/vendor/jquery-validation/jquery.validate.min.js',
        'front' => $appPath . 'resources/assets/brand/js/front.js',
        'login' => [
            'login' => $appPath . 'resources/assets/brand/js/login.js',
        ],
        'offer' => [
            'offer' => $appPath . 'resources/assets/brand/js/offer/offer.js',
        ],
        'profile' => [
            'changepassword' => $appPath . 'resources/assets/brand/js/profile/changepassword.js',
            'view' => $appPath . 'resources/assets/brand/js/profile/view.js',
            'wallet' => $appPath . 'resources/assets/brand/js/profile/wallet.js',
            'bankdetails' => $appPath . 'resources/assets/brand/js/profile/bankdetails.js',
        ],
        'coupon' => [
            'coupon' => $appPath . 'resources/assets/brand/js/coupon/coupon.js',
        ],
    ],
    'css' => [
        'bootstrap' => $appPath . 'resources/assets/brand/vendor/bootstrap/css/bootstrap.min.css',
        'fontastic' => $appPath . 'resources/assets/brand/css/fontastic.css',
        'fontawesome' => $appPath . 'resources/assets/brand/vendor/font-awesome/css/font-awesome.min.css',
        'theme' => $appPath . 'resources/assets/brand/css/style.default.css',
        'custom' => $appPath . 'resources/assets/brand/css/custom.css',
    ],
    'image' => [
        'favicon' => $appPath . 'resources/assets/brand/img/favicon.ico',
    ],
    'api' => [
        'profile' => [
            'changepassword' => $apiPath . "profile/changepassword",
            'view' => $apiPath . "brand/id",
            'update' => $apiPath . "brand/update",
            'wallet' => $apiPath . "brand/wallet",
            'transcation' => $apiPath . "brand/transcation",
            'balance' => $apiPath . "brand/balance",
        ],
    ],
];
