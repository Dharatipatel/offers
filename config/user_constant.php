<?php

$apiPath = url("/") . "/api/";
$appPath = url("/") . "/";
return [
    'script' => [
        'jquery' => $appPath . 'resources/assets/user/js/jquery.js',
        'bootstrap' => $appPath . 'resources/assets/user/js/bootstrap.min.js',
        'modernizr' => $appPath . 'resources/assets/user/js/modernizr.min.js',
        'pace' => $appPath . 'resources/assets/user/js/pace.min.js',
        'wow' => $appPath . 'resources/assets/user/js/wow.min.js',
        'scrollTo' => $appPath . 'resources/assets/user/js/jquery.scrollTo.min.js',
        'nicescroll' => $appPath . 'resources/assets/user/js/jquery.nicescroll.js',
        'moment' => $appPath . 'resources/assets/user/plugins/chat/moment-2.2.1.js',
        'waypoints' => $appPath . 'resources/assets/user/js/waypoints.min.js',
        'counterup' => $appPath . 'resources/assets/user/js/jquery.counterup.min.js',
        'datatables' => [
            "jquery" => $appPath . 'resources/assets/user/js/jquery.dataTables.min.js',
            "bootstrap" => $appPath . 'resources/assets/user/js/dataTables.bootstrap.min.js',
        ],
        'validate' => $appPath . 'resources/assets/user/js/jquery.validate.min.js',
        'sweet-alert' => [
            "min" => $appPath . 'resources/assets/user/plugins/sweet-alert/sweet-alert.min.js',
            "init" => $appPath . 'resources/assets/user/plugins/sweet-alert/sweet-alert.init.js',
        ],
        'app' => $appPath . 'resources/assets/user/js/jquery.app.js',
        'chat' => $appPath . 'resources/assets/user/js/jquery.chat.js',
        'main' => $appPath . 'resources/assets/user/js/main.js',
        'todo' => $appPath . 'resources/assets/user/js/jquery.todo.js',
        'login' => $appPath . 'resources/assets/user/js/auth/login.js',
    ],
    'css' => [
        'bootstrap' => [
            "min" => $appPath . 'resources/assets/user/css/bootstrap.min.css',
            "reset" => $appPath . 'resources/assets/user/css/bootstrap-reset.css',
        ],
        "animate" => $appPath . 'resources/assets/user/css/animate.css',
        "font-awesome" => $appPath . 'resources/assets/user/plugins/font-awesome/css/font-awesome.css',
        "ionicons" => $appPath . 'resources/assets/user/plugins/ionicon/css/ionicons.min.css',
        "sweet-alert" => $appPath . 'resources/assets/user/plugins/sweet-alert/sweet-alert.min.css',
        "style" => $appPath . 'resources/assets/user/css/style.css',
        "helper" => $appPath . 'resources/assets/user/css/helper.css',
        "dataTables" => $appPath . 'resources/assets/user/css/dataTables.bootstrap.min.css',
        "wysihtml5" => $appPath . 'resources/assets/user/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css',
        "dropzone" => $appPath . 'resources/assets/user/plugins/dropzone/dropzone.css',
    ],
    'api' => [
        'login' => $apiPath . "user/login",
        'register' => $apiPath . "user/register",
    ]
];
