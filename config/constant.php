<?php

$apiPath = url("/") . "/api/";
$appPath = url("/") . "/";
return [
    'api' => [
        'login' => $apiPath . 'user/login',
        'brand' => [
            'list' => $apiPath . 'brand',
            'add' => $apiPath . 'brand',
            'update' => $apiPath . 'brand/update',
            'delete' => $apiPath . 'brand/delete',
            'active' => $apiPath . 'brand/active',
            'deactive' => $apiPath . 'brand/deactive',
            'brandoffer' => $apiPath . 'brand/brandoffer',
            'email' => [
                'send' => $apiPath . 'brand/email/send',
            ]
        ],
        'address' => [
            'add' => $apiPath . 'brand/address',
            'update' => $apiPath . 'brand/address/update',
        ],
        'bank' => [
            'add' => $apiPath . 'bank',
            'get' => $apiPath . 'bank',
            'update' => $apiPath . 'bank/update',
        ],
        'category' => [
            'list' => $apiPath . 'category',
            'add' => $apiPath . 'category/add',
            'update' => $apiPath . 'category/update',
            'delete' => $apiPath . 'category/delete',
            'active' => $apiPath . 'category/active',
            'deactive' => $apiPath . 'category/deactive',
        ],
        'corporate' => [
            'list' => $apiPath . 'corporate',
            'add' => $apiPath . 'corporate',
            'update' => $apiPath . 'corporate/update',
            'delete' => $apiPath . 'corporate/delete',
            'active' => $apiPath . 'corporate/active',
            'deactive' => $apiPath . 'corporate/deactive',
        ],
        'country' => [
            'list' => $apiPath . 'country',
            'add' => $apiPath . 'country',
            'update' => $apiPath . 'country/update',
            'delete' => $apiPath . 'country/delete',
            'active' => $apiPath . 'country/active',
            'deactive' => $apiPath . 'country/deactive',
            'address' => $apiPath . 'country/address',
        ],
        'state' => [
            'list' => $apiPath . 'state',
            'add' => $apiPath . 'state',
            'update' => $apiPath . 'state/update',
            'delete' => $apiPath . 'state/delete',
            'active' => $apiPath . 'state/active',
            'deactive' => $apiPath . 'state/deactive',
        ],
        'city' => [
            'list' => $apiPath . 'city',
            'add' => $apiPath . 'city',
            'update' => $apiPath . 'city/update',
            'delete' => $apiPath . 'city/delete',
            'active' => $apiPath . 'city/active',
            'deactive' => $apiPath . 'city/deactive',
        ],
        'offer' => [
            'list' => $apiPath . 'offer',
            'add' => $apiPath . 'offer',
            'update' => $apiPath . 'offer/update',
            'delete' => $apiPath . 'offer/delete',
            'active' => $apiPath . 'offer/active',
            'approve' => $apiPath . 'offer/approve',
            'deactive' => $apiPath . 'offer/deactive',
        ],
        'employee' => [
            'list' => $apiPath . 'employee',
            'add' => $apiPath . 'employee',
            'update' => $apiPath . 'employee/update',
            'delete' => $apiPath . 'employee/delete',
            'active' => $apiPath . 'employee/active',
            'deactive' => $apiPath . 'employee/deactive',
        ],
        'reward' => [
            'list' => $apiPath . 'reward',
            'add' => $apiPath . 'reward',
            'statistics' => $apiPath . 'reward/statistics',
            'orderhistory' => $apiPath . 'reward/orderhistory',
            'orderhistorystatistic' => $apiPath . 'reward/orderhistorystatistic',
            'brandorderhistory' => $apiPath . 'reward/brandorderhistory',
        ],
        'coupon' => [
            'coupon' => $apiPath . 'coupon',
            'redeem' => $apiPath . 'coupon/redeem',
        ],
        'profile' => [
            'view' => $apiPath . 'profile/view',
            'update' => $apiPath . 'profile/update',
        ],
        'sms_template' => [
            'list' => $apiPath . 'sms_template',
            'add' => $apiPath . 'sms_template',
            'update' => $apiPath . 'sms_template/update',
            'delete' => $apiPath . 'sms_template/delete',
            'active' => $apiPath . 'sms_template/active',
            'deactive' => $apiPath . 'sms_template/deactive',
        ],
        'slider' => [
            'list' => $apiPath . 'slider',
            'add' => $apiPath . 'slider',
            'update' => $apiPath . 'slider/update',
            'delete' => $apiPath . 'slider/delete',
        ],
    ],
    'script' => [
        'common' => [
            'helper' => $appPath . 'resources/assets/common/js/helper.js',
            'jquerymd5' => $appPath . 'resources/assets/common/js/jquery.md5.js',
        ],
        'datatables' => [
            'jquery' => $appPath . 'resources/assets/admin/assets/datatables/jquery.dataTables.min.js',
            'bootstrap' => $appPath . 'resources/assets/admin/assets/datatables/dataTables.bootstrap.js',
        ],
        'tinymce' => [
            'js' => $appPath . 'resources/assets/admin/assets/tinymce/js/tinymce/tinymce.min.js',
        ],
        'validate' => [
            'js' => $appPath . 'resources/assets/admin/assets/jquery.validate/jquery.validate.min.js',
        ],
        'reward' => [
            'orderhistory' => $appPath . 'resources/assets/brand/js/reward/orderhistory.js',
            'list' => $appPath . 'resources/assets/admin/js/reward/list.js',
        ],
        'profile' => [
            'changepassword' => $appPath . 'resources/assets/admin/js/profile/changepassword.js',
            'view' => $appPath . 'resources/assets/admin/js/profile/view.js',
        ],
        'ko-model' => $appPath . 'resources/assets/common/js/ko-model.js',
        'category' => $appPath . 'resources/assets/admin/js/category/category.js',
        'brand' => $appPath . 'resources/assets/admin/js/brand/brand.js',
        'offer' => $appPath . 'resources/assets/admin/js/offer/offer.js',
        'corporate' => $appPath . 'resources/assets/admin/js/corporate/corporate.js',
        'location' => $appPath . 'resources/assets/admin/js/location/location.js',
        'employee' => $appPath . 'resources/assets/admin/js/employee/employee.js',
        'sms_template' => $appPath . 'resources/assets/admin/js/sms_template/sms_template.js',
    ],
    'web' => [
        'setSession' => $appPath . 'setsession',
        'updatecategory' => $appPath . 'category/id',
        'category' => [
            'brand' => $appPath . 'admin/category/brand',
            'corporate' => $appPath . 'admin/category/corporate',
            'reward' => $appPath . 'admin/category/reward',
        ],
    ],
];
