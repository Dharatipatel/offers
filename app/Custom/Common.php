<?php

namespace App\Custom;

use App\Http\Models\User;

class Common {

    //use for transaction id too...
    public static function refer_code($length = 8) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';

        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }

    public static function get_id() {

        $randomCode = Common::refer_code(30);
        return $randomCode;
    }

    public static function generateCoupenCode($digit = 6) {
        $random_number = ''; // set up a blank st*ring
        $count = 0;
        while ($count < $digit) {
            $random_digit = mt_rand(0, 9);

            $random_number .= $random_digit;
            $count++;
        }
        return $random_number;
    }

    public static function get_QR_code($string) {

        include(app_path() . "/libs/phpqrcode/qrlib.php");

        $error_correction_level = 'H';
        $size = 4;
        $margin = 4;
        $dir = "/uploads/qr/";

        $file_name = $string . ".png";
        if (!file_exists(public_path() . $dir)) {
            mkdir(public_path() . $dir, 0777, true);
        }

        if (file_exists(public_path() . $dir . $file_name)) {
            return asset(url($dir . $file_name));
        }

        $path = public_path() . $dir . $file_name;
        \QRcode::png($string, $path, $error_correction_level, $size, $margin);

        return url('uploads/qr/' . $file_name);
    }

    public static function getToken($length = 32) {
        $randomCode = Common::refer_code($length);
        $check = User::where("token", "=", $randomCode)->count();
        while ($check > 0) {
            $randomCode = Common::refer_code($length);
            $check = User::where("token", "=", $randomCode)->count();
        }
        return $randomCode;
    }

}
