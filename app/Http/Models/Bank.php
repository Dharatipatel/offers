<?php 
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Bank extends Model
{
   
    public function addBankDetails($data){
    	$bank_details = DB::table('bank_details')->insertGetId($data);
    	return $bank_details;
    }
    
    
    public function getBankDetails($data){
    	$bank_details = DB::table('bank_details')
                ->when(array_key_exists('bid', $data), function($query) use ($data){
                    return $query->where('bank_details.brand_id','=', $data['bid']);
                  })
                ->first();
    	return $bank_details;
    }
    public function updateBankDetails($data,$id){
    	$update_bank_details = DB::table('bank_details')->where('id', $id)->update($data);
    	return $update_bank_details;	
    }
}