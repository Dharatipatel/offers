<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Requested extends Model {

    protected $fillable = [
        'name',
        'address'
    ];
    protected $table = "requests";

}
