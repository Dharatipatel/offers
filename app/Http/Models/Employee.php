<?php 
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Employee extends Model
{
    public function addEmployee($data){
    	$employee = DB::table('employee')->insertGetId($data);
    	return $employee;
    }

    public function getEmployee($data){
    	$employees = DB::table('employee')
                    ->when(array_key_exists('cid', $data), function($query) use ($data){
                            return $query->where('employee.corporate_id', $data['cid']);
                    })
                    ->when(array_key_exists('status', $data), function($query) use ($data){
                        return $query->whereIn('employee.status', array($data['status']));
                    })
                    ->when(array_key_exists('q', $data), function($query) use ($data){
                        return $query->where('employee.first_name', 'like', '%'.$data['q'].'%');
                                    
                    })
                    ->when(array_key_exists('eid', $data), function($query) use ($data){
                        return $query->whereIn('employee.id',$data['eid'] );
                                    
                    })
                    ->paginate();
    	return $employees;
    }

    public function activeEmployee($data,$id){

    	$active_user = DB::table('employee')->where('id', $id)->update($data);
    	return $active_user;	
    }

    public function deactiveEmployee($data,$id){
    	$deactive_user = DB::table('employee')->where('id', $id)->update($data);
    	return $deactive_user;	
    }

    public function updateEmployee($data){
    	$update_employee = DB::table('employee')->where('id', $data['id'])->update($data);
    	return $update_employee;	
    }

    public function deleteEmployee($data){
    	$delete_employee = DB::table('employee')->where('id', '=', $data['id'])->delete();
    	// $delete_employee = DB::table('employee')->where('id', $data['id'])->update($data);
    	return $delete_employee;
     }
    public function getEmployeeCount($data){

        return DB::table('employee')
                ->when(array_key_exists('status', $data), function($query) use ($data){
                    return $query->where('status', $data['status']);
                })
                ->when(array_key_exists('cid', $data), function($query) use ($data){
                    return $query->where('employee.corporate_id', $data['cid']);
                })
                ->count();
    }
}