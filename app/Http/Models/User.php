<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class User extends Model {

    public $timestamps = false;

    public function register($data) {
        $user = DB::table('users')->insertGetId($data);
        return $user;
    }

    public function login($data) {
        $user = DB::table('users')->where($data)->first();
        return $user;
    }

    public function checkToken($data) {
        $user = DB::table('users')->where($data)->first();
        return $user;
    }

    public function updateUser($data, $id) {
        $user = DB::table('users')->where('id', $id)->update($data);
        return $user;
    }

    public function updateToken($data) {
        $user = DB::table('users')->where('id', $data['id'])->update(['token' => $data['token'], 'modified' => $data['modified']]);
        return $user;
    }

    public function checkPassword($data) {
        $password = DB::table('users')->where($data)->first();
        return $password;
    }

    public function changePassword($data, $id) {
        $update_password = DB::table('users')->where('id', $id)->update($data);
        return $update_password;
    }

    public function getUserById($id) {
        $user = DB::table('users')
                        ->leftjoin('brand', 'users.id', '=', 'brand.uid')
                        ->select('brand.*', 'users.email', 'users.contact', 'brand.id as bid')
                        ->where(['users.id' => $id])->first();
        return $user;
    }

    public function getCorporateById($id) {
        $corporate = DB::table('users')
                        ->leftjoin('corporate', 'users.id', '=', 'corporate.uid')
                        ->select('corporate.*', 'users.email', 'users.contact', 'corporate.id as cid')
                        ->where(['users.id' => $id])->first();
        return $corporate;
    }

    public function getAdminDetail($data) {
        $user = DB::table('users')->where($data)->first();
        return $user;
    }

    public function updateAdminDetail($data, $id) {
        $user = DB::table('users')->where('id', $id)->update($data);
        return $user;
    }

    public function getUser($id) {
        $user = DB::table('users')
                        ->select("first_name", "last_name", "email", "contact")
                        ->where(['users.id' => $id])->first();
        return $user;
    }
    public function getMerchant($id) {
        $user = DB::table('users')
                        ->select("first_name", "last_name", "email", "contact")
                        ->where(['users.id' => $id])->first();
        return $user;
    }

    public function updateUserProfile($data, $where) {
        $user = DB::table('users')->where($where)->update($data);
        return $user;
    }

    public function getUserProfile($where) {
        $user = DB::table('users')
                        ->select("first_name", "last_name", "email", "contact")
                        ->where($where)->first();
        return $user;
    }

}
