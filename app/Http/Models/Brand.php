<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Exception;

class Brand extends Model {

    protected $table = "brand";
    public $timestamps = false;

    public function addBrand($data) {
        $brand = DB::table('brand')->insertGetId($data);
        return $brand;
    }

    public function addWallet($data) {
        $brand = DB::table('withdraw_request')->insertGetId($data);
        return $brand;
    }

    public function getWalletTransaction($data) {
        $transaction = DB::table('withdraw_request')
                ->when(array_key_exists('user_id', $data), function($query) use ($data) {
                    return $query->where('user_id', '=', $data['user_id']);
                })
                ->paginate();
        return $transaction;
    }

    public function getBalance($data) {
        $amount = DB::table('user_wallet')
                ->when(array_key_exists('user_id', $data), function($query) use ($data) {
                    return $query->where('user_id', '=', $data['user_id']);
                })
                ->paginate();
        return $amount;
    }

    public function getBrand($data) {
        $brands = DB::table('brand')
                ->leftjoin('users', 'brand.uid', '=', 'users.id')
                ->leftjoin('category', 'brand.brand_category', '=', 'category.id')
                // ->leftjoin('brand_address', 'brand.id', '=', 'brand_address.brand_id')
                ->select('brand.*', 'users.email', 'users.contact', 'users.status as status' , 'category.name as brand_category_name', DB::raw("(SELECT COUNT(offer.id) FROM offer WHERE offer.brand_id =brand.id) as    offer_count")
                )
                ->paginate($data);
        return $brands;
    }

    public function getBrandOffer($data) {
        $brand_offers = DB::table('brand')
                ->join('offer', 'brand.id', '=', 'offer.brand_id')
                ->join('category', 'brand.brand_category', '=', 'category.id')
                ->join('users', 'brand.uid', '=', 'users.id')
                ->select('brand.id as brand_id', 'brand.logo as brand_logo', 'brand.name as brand_name', 'category.name as brand_category_name', 'offer.brand_id', DB::raw("(SELECT COUNT(`offer`.`id`) FROM `offer`  WHERE `offer`.`brand_id` = `brand`.`id` and `offer`.`price` = 0 ) as freecoupen"), DB::raw("(SELECT COUNT(`offer`.`id`) FROM `offer`  WHERE `offer`.`brand_id` = `brand`.`id` and `offer`.`price` != 0 ) as fixcoupen")
                )
                ->where('offer.offer_status_id', 1)
                ->where('users.status', 1)
                ->where('category.status', 1)
                ->groupBy('offer.brand_id')
                ->paginate();
        return $brand_offers;
    }

    public function activeBrand($data, $id) {

        $active_brand = DB::table('users')->where('id', $id)->update($data);
        return $active_brand;
    }

    public function deactiveBrand($data, $id) {
        $deactive_brand = DB::table('users')->where('id', $id)->update($data);
        return $deactive_brand;
    }

    public function updateBrand($data, $id) {
        $update_brand = DB::table('brand')->where('id', $id)->update($data);
        return $update_brand;
    }

    public function deleteBrand($data) {
        $delete_brand = DB::table('brand')->where('id', '=', $data['id'])->delete();
        return $delete_brand;
    }

    public function getBrandById($id) {
        $brand = DB::table('brand')
                        ->leftjoin('users', 'brand.uid', '=', 'users.id')
                        ->leftjoin('category', 'brand.brand_category', '=', 'category.id')
                        ->leftjoin('brand_address', 'brand.id', '=', 'brand_address.brand_id')
                        ->select('brand.*', 'brand_address.*', 'users.email', 'users.contact', 'category.name as brand_category_name')
                        ->where('users.id', $id)->first();
        return $brand;
    }

    public function getAddress($brand) {

        $address = DB::table('brand_address')
                ->leftjoin('city', 'brand_address.city', '=', 'city.id')
                ->leftjoin('state', 'brand_address.state', '=', 'state.id')
                ->leftjoin('country', 'brand_address.country', '=', 'country.id')
                ->select('brand_address.id', 'brand_address.address', 'brand_address.pincode', 'brand_address.city as city_id', 'brand_address.state as state_id', 'brand_address.country as country_id', 'city.name as city', 'state.name as state', 'country.name as country')
                ->where('brand_address.brand_id', '=', $brand)
                ->paginate();
        return $address;
    }

    public function addAddress($brand_address) {
        $brand_address = DB::table('brand_address')->insert($brand_address);
        return $brand_address;
    }

    public function removeAddress($brand) {
        $brand_address = DB::table('brand_address')
                ->where('brand_address.brand_id', '=', $brand)
                ->delete();
        return $brand_address;
    }

    public function getBrandCount($data) {
        return DB::table('brand')
               ->leftjoin('users', 'brand.uid', '=', 'users.id')
              ->where('users.status', $data['status'])->count();
    }

    public function savePassword($data) {
        try {
            $model = Brand::find($data->id);
            $user = User::find($model->uid);
            $user->password = bcrypt($data->password);
            $user->modified = date("Y-m-d H:i:s");
            $user->is_mail_sent = 1;
            $user->save();

            $brand = Brand::join("users", "users.id", "=", "brand.uid")
                    ->select("users.email", "brand.contact_person_name as user_name")
                    ->first();
            return $brand;
        } catch (Exception $e) {
            return false;
        }
    }

}
