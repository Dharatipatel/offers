<?php 
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Reward extends Model
{
   
    public function addReward($data){
    	$offer = DB::table('reward')->insertGetId($data);
    	return $offer;
    }
    
    public function getReward($data){
      $category_condition ="";
      if(array_key_exists('type', $data)){
          if($data['type'] == 'general'){
              $category_condition .= "and category.user_id IS NULL";
          } else if($data['type'] == 'user'){
              $category_condition .= "and category.user_id IS NOT NULL";
          }
      }
    	$offers = DB::table('order_history')
                ->leftjoin('employee', 'order_history.employee_id', '=', 'employee.id')
                ->leftjoin('reward','order_history.reward_id', '=' ,'reward.id')
                ->select('order_history.bill_amount','reward.created','issue_date','redeem_date','employee.first_name','employee.designation','employee.last_name' ,'reward.category_id' ,'reward.offer_id' , 
                    DB::raw("(SELECT  name FROM  category where category.id = reward.category_id ". $category_condition.") as category_name"), 
                    DB::raw("(SELECT offer.value FROM offer where offer.id = reward.offer_id)  as coupon_value "),
                    DB::raw("(SELECT brand.name FROM offer JOIN brand on offer.brand_id = brand.id where offer.id = reward.offer_id)  as brand_name") 
                )
                ->when(array_key_exists('cid', $data), function($query) use ($data){
                    return $query->where('employee.corporate_id', $data['cid']);
                })
                ->when(array_key_exists('status', $data), function($query) use ($data){
                    return $query->where('order_history.redeem', $data['status']);
                })
                ->paginate();

    	return $offers;
    }
    
    public function getRewardUsed($data){
        return DB::table('order_history')
              ->leftjoin('employee', 'order_history.employee_id', '=', 'employee.id')
              ->when(array_key_exists('redeem', $data), function($query) use ($data){
                    return $query->where('redeem', $data['redeem']);
                })
              ->when(array_key_exists('cid', $data), function($query) use ($data){
                    return $query->where('employee.corporate_id', $data['cid']);
                })
              ->count();
    }

    public function getInvestedAmount($data){
        return DB::table('order_history')
              ->leftjoin('employee', 'order_history.employee_id', '=', 'employee.id')
              // ->when(array_key_exists('redeem', $data), function($query) use ($data){
              //       return $query->where('redeem', $data['redeem']);
              //   })
              ->when(array_key_exists('cid', $data), function($query) use ($data){
                    return $query->where('employee.corporate_id', $data['cid']);
                })
              ->sum('coupon_price');
    }
    public function getBrandOrderHistory($data){
      /*SELECT (SELECT brand.name from offer join brand on offer.brand_id =brand.id WHERE offer.id =reward.offer_id) as brand_name ,order_history.* ,employee.first_name ,employee.last_name FROM `order_history` JOIN employee on order_history.employee_id = employee.id JOIN reward on order_history.reward_id = reward.id WHERE order_history.redeem =1*/

        return DB::table('order_history')
              ->join('employee', 'order_history.employee_id', '=', 'employee.id')
              ->join('reward', 'order_history.reward_id', '=', 'reward.id')
              ->select( 'order_history.*','employee.first_name' ,'employee.last_name',
                DB::raw("(SELECT brand.name from offer join brand on offer.brand_id =brand.id WHERE offer.id =reward.offer_id) as brand_name") 
              )
              ->when(array_key_exists('redeem', $data), function($query) use ($data){
                    return $query->where('order_history.redeem', $data['redeem']);
                })
              ->paginate();
        
    }
}