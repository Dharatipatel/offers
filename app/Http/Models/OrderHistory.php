<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class OrderHistory extends Model {

    protected $table = "order_history";

}
