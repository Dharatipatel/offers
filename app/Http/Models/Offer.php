<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Http\Models\Coupon;
use App\Custom\Common;

class Offer extends Model {

    protected $table = "offer";

    public function addOffer($data) {
        $offer = DB::table('offer')->insertGetId($data);
        return $offer;
    }

    public function getCoupen($data) {
        $coupon_detail = DB::table('order_history')
                ->join('employee', 'order_history.employee_id', '=', 'employee.id')
                ->select('order_history.*', 'employee.corporate_id', 'employee.first_name', 'employee.last_name', DB::raw("(select corporate.name from corporate where corporate.id = employee.corporate_id) as company_name"), DB::raw("(SELECT offer.valid_to from reward JOIN offer on reward.offer_id = offer.id WHERE reward.id = order_history.reward_id ) as expiry_date"), DB::raw("(SELECT offer.value from reward JOIN offer on reward.offer_id = offer.id WHERE reward.id = order_history.reward_id ) as coupen_value ")
                )
                ->where('order_history.coupen', '=', $data['coupen'])
                ->where('order_history.redeem', '!=', 1)
                ->where('employee.email', '=', $data['email_phone'])
                ->paginate();
        return $coupon_detail;
    }

    public function getOffer($data) {
        $offers = DB::table('offer')
                ->leftjoin('brand', 'offer.brand_id', '=', 'brand.id')
                ->leftjoin('category', 'brand.brand_category', '=', 'category.id')
                ->select('brand.name as brand_name', 'category.name as category', 'offer.*')
                ->when(array_key_exists('bid', $data), function($query) use ($data) {
                    return $query->where('offer.brand_id', $data['bid']);
                })
                ->when(array_key_exists('offer_status_id', $data), function($query) use ($data) {
                    return $query->whereIn('offer.offer_status_id', array($data['offer_status_id']));
                })
                ->when(array_key_exists('brand_category', $data), function($query) use ($data) {
                    return $query->where('brand.brand_category', $data['brand_category']);
                })
                ->when(array_key_exists('coupentype', $data) && in_array(0, $data['coupentype']) && count($data['coupentype']) == 1, function($query) use ($data) {
                    return $query->where('offer.price', '=', 0);
                })
                ->when(array_key_exists('coupentype', $data) && in_array(1, $data['coupentype']) && count($data['coupentype']) == 1, function($query) use ($data) {
                    return $query->where('offer.price', '>', 0);
                })
                ->when(array_key_exists('pricerange', $data) && in_array(0, $data['pricerange']) && count($data['pricerange']) == 1, function($query) use ($data) {
                    return $query->whereBetween('offer.price', [0, 500]);
                })
                ->when(array_key_exists('pricerange', $data) && in_array(1, $data['pricerange']) && count($data['pricerange']) == 1, function($query) use ($data) {
                    return $query->whereBetween('offer.price', [500, 2000]);
                })
                ->when(array_key_exists('pricerange', $data) && in_array(2, $data['pricerange']) && count($data['pricerange']) == 1, function($query) use ($data) {
                    return $query->whereBetween('offer.price', [2000, 5000]);
                })
                ->when(array_key_exists('pricerange', $data) && in_array(3, $data['pricerange']) && count($data['pricerange']) == 1, function($query) use ($data) {
                    return $query->whereBetween('offer.price', '>', 5000);
                })
                ->paginate();

        return $offers;
    }

    public function approveOffer($data, $id) {

        $approve_brand = DB::table('offer')->where('id', $id)->update($data);
        return $approve_brand;
    }

    public function deactiveOffer($data, $id) {
        $deactive_brand = DB::table('offer')->where('id', $id)->update($data);
        return $deactive_brand;
    }

    public function updateOffer($data, $id) {
        $update_offer = DB::table('offer')->where('id', $id)->update($data);
        return $update_offer;
    }

}
