<?php 
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Order extends Model
{
   
    
    public function addOrders($data){
        $data = DB::table('order_history')->insert($data);
        return $data;
    }
    public function redeemCoupen($data){
        $redeem_coupen = DB::table('order_history')
        				->where('coupen', $data['coupen'])
        				->where('reward_id',$data['reward_id'])
        				->update(array(
        					'bill_amount' =>$data['bill_amount'],
        					'member' =>$data['member'],	
        					'redeem' =>1,	
        					'redeem_date' =>$data['redeem_date']	
        				));
    	return $redeem_coupen;
    }
}