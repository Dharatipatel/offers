<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model {

    public $timestamps = false;
    protected $table = "coupen";
    protected $fillable = [
        'name', 'description', 'value', 'p_value', 'expiry_date', 'type', 'category_id', 'created'
    ];

}
