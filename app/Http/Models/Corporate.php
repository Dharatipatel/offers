<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Corporate extends Model {

    public function addCorporate($data) {
        $corporate = DB::table('corporate')->insertGetId($data);
        return $corporate;
    }

    public function getCorporate($data) {
        $corporate = DB::table('corporate')
                ->join('users', 'corporate.uid', '=', 'users.id')
                ->leftjoin('category', 'corporate.category', '=', 'category.id')
                ->select('corporate.*', 'users.email', 'users.contact', 'users.status as status',
                    'category.name as corporate_category_name',DB::raw("(SELECT COUNT(employee.id) FROM employee WHERE employee.corporate_id = corporate.id) as employee_count")
                 )
                ->paginate($data);
        return $corporate;
    }

    public function activeCorporate($data, $id) {

        $active_corporate = DB::table('users')->where('id', $id)->update($data);
        return $active_corporate;
    }

    public function deactiveCorporate($data, $id) {
        $deactive_corporate = DB::table('users')->where('id', $id)->update($data);
        return $deactive_corporate;
    }

    public function updateCorporate($data, $id) {
        $update_corporate = DB::table('corporate')->where('id', $id)->update($data);
        return $update_corporate;
    }

    public function deleteCorporate($data) {
        $delete_corporate = DB::table('corporate')->where('id', '=', $data['id'])->delete();
        // $delete_corporate = DB::table('employee')->where('id', $data['id'])->update($data);
        return $delete_corporate;
    }

    public function getCorporateById($id) {
        $corporate = DB::table('corporate')
                        ->join('users', 'corporate.uid', '=', 'users.id')
                        // ->join('category', 'brand.brand_category', '=', 'category.id')
                        ->select('corporate.*', 'users.email', 'users.contact')
                        ->where('users.id', $id)->first();
        return $corporate;
    }

}
