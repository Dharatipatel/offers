<?php 
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Slider extends Model
{
   
    public function addSlider($data){
    	$slider = DB::table('sliders')->insertGetId($data);
    	return $slider;
    }

    public function getSlider($data){
    	$slider = DB::table('sliders')
                    ->when(array_key_exists('status', $data), function($query) use ($data){
                        return $query->where('status', '=',$data['status']);
                    })
                    ->paginate();
    	return $slider;
    }

    public function updateSlider($data,$id){
    	$update_slider = DB::table('slider')->where('id', $id)->update($data);
    	return $update_slider;	
    }

    public function deleteSlider($data){
    	$delete_slider = DB::table('slider')->where('id', '=', $data['id'])->delete();
    	// $delete_slider = DB::table('employee')->where('id', $data['id'])->update($data);
    	return $delete_slider;
    }

    public function getSliderById($id){
        $slider = DB::table('slider')->where('id', $id)->first();
        return $slider;
    }
   
}