<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class BrandAddress extends Model {

    protected $table = "brand_address";
    public $timestamps = false;
}
