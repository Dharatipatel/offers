<?php
namespace App\Http\Controllers\Web\brand;

use App\Http\Controllers\Controller;
use Request;
use Session;


class CoupenController extends Controller
{
    public function index(){
        if (Session::has('loginvalue')) {
            $data = array(
                'title' => 'Coupen'
            );
            return view('brand.coupen.index')->with($data);
        } else {
            return redirect('login');
        }
    }
}