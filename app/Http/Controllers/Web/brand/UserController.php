<?php
namespace App\Http\Controllers\Web\brand;

use App\Http\Controllers\Controller;
use Request;
use Session;


class UserController extends Controller
{
    public function login(){
    	return view('brand.login');
    }

    public function logout(){
        $data = Session::flush();
        return redirect('login');
    }
    
    public function setSession(){
    	$input = Request::all();
    	if(!Session::has('loginvalue')){
    		Session::regenerate();
    		if (Request::has('token')) {
 				session(['token' => $input['token']]);
			}
			if (Request::has('_token')) {
 				session(['_token' => $input['_token']]);
			}
			if (Request::has('email')) {
 				session(['email' => $input['email']]);
			}
			if (Request::has('role_id')) {
 				session(['role' => $input['role_id']]);
			}
			if (Request::has('id')) {
                session(['id' => $input['id']]);
            }
            if (Request::has('bid')) {
 				session(['bid' => $input['bid']]);
			}
			session(['loginvalue' => 1]);
    	}
    	$data=Session::all();
    	return $data;
    }
    public function editProfile(){
        
        $data = array(
            'title' => 'Offer',
            'type' =>'edit'
        );
        return view('brand.profile.edit')->with($data);
       
    }

    public function viewProfile(){
        
        $data = array(
            'title' => 'Offer',
            'type' =>'view'
        );
        return view('brand.profile.view')->with($data);
       
    }

    public function changePassword(){
       
        $data = array(
            'title' => 'Offer'
        );
        return view('brand.profile.changepassword')->with($data);
        
    }

    public function myWallet(){
      
        $data = array(
            'title' => 'wallet'
        );
        return view('brand.profile.wallet')->with($data);
    }
    public function bankDetails(){
      
        $data = array(
            'title' => 'Offer'
        );
        return view('brand.profile.bankdetails')->with($data);
      
    }
    
}