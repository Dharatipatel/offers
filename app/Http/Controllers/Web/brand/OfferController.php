<?php
namespace App\Http\Controllers\Web\brand;

use App\Http\Controllers\Controller;
use Request;
use Session;


class OfferController extends Controller
{
    public function index(){
        $data = array(
            'title' => 'Offer',
            'type'  => 'list',
        );
        if(Request::has('pending')){
            $data['status'] = 3;
        } else{
            $data['status'] = 1;
        }
        return view('brand.offer.index')->with($data);
    }
    public function addOffer(){
        $data = array(
            'title' => 'Add Offer',
            'type'  => 'add',
        );
        return view('brand.offer.add')->with($data);
    }
    public function pastOffer(){
        $data = array(
            'title' => 'Past Offer',
            'type'  => 'past',
        );
        return view('brand.offer.past')->with($data);
    }
}