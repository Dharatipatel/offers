<?php
namespace App\Http\Controllers\Web\brand;

use App\Http\Controllers\Controller;
use Request;
use Session;


class DashboardController extends Controller
{
    public function login(){
    	return view('brand.login');
    }

    public function index(){
        if (Session::has('loginvalue')) {
            $data = array(
                'title' => 'Brand Dashboard'
            );
            return view('brand.dashboard')->with($data);
        } else {
            return redirect('login');
        }
    }
}