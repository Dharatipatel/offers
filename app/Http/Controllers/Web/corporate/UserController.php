<?php
namespace App\Http\Controllers\Web\corporate;

use App\Http\Controllers\Controller;
use Request;
use Session;


class UserController extends Controller
{
    public function login(){
    	return view('corporate.login');
    }

    public function logout(){
        $data = Session::flush();
        return redirect('login');
    }
    
    public function setSession(){
    	$input = Request::all();
    	if(!Session::has('loginvalue')){
    		Session::regenerate();
    		if (Request::has('token')) {
 				session(['token' => $input['token']]);
			}
			if (Request::has('_token')) {
 				session(['_token' => $input['_token']]);
			}
			if (Request::has('email')) {
 				session(['email' => $input['email']]);
			}
			if (Request::has('role_id')) {
 				session(['role' => $input['role_id']]);
			}
			if (Request::has('id')) {
 				session(['id' => $input['id']]);
			}
			session(['loginvalue' => 1]);
    	}
    	$data=Session::all();
    	return $data;
    }
    public function editProfile(){
        
            $data = array(
                'title' => 'Corporate',
                'page' =>'edit'
            );
            return view('corporate.profile.edit')->with($data);
        
    }

    public function viewProfile(){  
            $data = array(
                'title' => 'Corporate',
                'page' =>'view'
            );
            return view('corporate.profile.view')->with($data);
    }

    public function changePassword(){
            $data = array(
                'title' => 'Corporate',
                'page' =>'changePassword'
            );
            return view('corporate.profile.changepassword')->with($data);
       
    }
    
}