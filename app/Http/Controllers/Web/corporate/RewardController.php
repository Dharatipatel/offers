<?php
namespace App\Http\Controllers\Web\corporate;

use App\Http\Controllers\Controller;
use Request;
use Session;


class RewardController  extends Controller
{
    public function __construct(){
       
    }
    
    public function reward(){
        $data = array(
            'title' => 'Reward',
            'type' =>'list',
        );
        return view('corporate.reward.index')->with($data);   
    }
    public function addReward(){
        $data = array(
            'title' => 'Reward',
            'type'  => 'add',
        );
        return view('corporate.reward.master')->with($data);   
    }
    public function index(){
        $data = array(
            'title' => 'Reward',
            'type'  => 'perksvilla',
        );
        return view('corporate.reward.perksvilla')->with($data);   
    }
    public function orderHistory(){
        $data = array(
            'title' => 'Reward',
            'type'  => 'orderhistory',
        );
        return view('corporate.reward.orderhistory')->with($data);   
    }
}