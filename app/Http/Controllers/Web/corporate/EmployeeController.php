<?php
namespace App\Http\Controllers\Web\corporate;

use App\Http\Controllers\Controller;
use Request;
use Session;


class EmployeeController extends Controller
{
    public function __construct(){
       
    }
    
    public function employee(){
        $data = array(
            'title' => 'Employee',
            'type' =>'list',
        );
        return view('corporate.employee.index')->with($data);   
    }
    public function addEmployee(){
        $data = array(
            'title' => 'Employee',
            'type'  => 'add',
        );
        return view('corporate.employee.index')->with($data);   
    }
}