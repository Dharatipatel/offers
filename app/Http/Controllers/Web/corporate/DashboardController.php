<?php
namespace App\Http\Controllers\Web\corporate;

use App\Http\Controllers\Controller;
use Request;
use Session;


class DashboardController extends Controller
{

    public function index(){
        if (Session::has('loginvalue')) {
            $data = array(
                'title' => 'Corporate Dashboard'
            );
            return view('corporate.dashboard')->with($data);
        } else {
            return redirect('corporate.login');
        }
    }
}