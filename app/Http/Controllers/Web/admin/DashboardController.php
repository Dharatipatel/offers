<?php
namespace App\Http\Controllers\Web\admin;

use App\Http\Controllers\Controller;
use Request;
use Session;


class DashboardController extends Controller
{
    public function __construct(){
       
    }
    
    public function index(){
        $data = array(
            'title' => 'Dashboard'
        );
        return view('admin.dashboard')->with($data);
    }
}