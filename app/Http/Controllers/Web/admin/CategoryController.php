<?php
namespace App\Http\Controllers\Web\admin;

use App\Http\Controllers\Controller;
use Request;
use Session;


class CategoryController extends Controller
{
    public function __construct(){
       
    }

    public function brand(){
        if (Session::has('loginvalue')) {
            $data = array(
                'title' => 'Brand Category',
                'type' =>1,
            );
            return view('admin.category.master')->with($data);
        } else {
            return redirect('admin/login');
        }
        
    }
    public function employee(){
        if (Session::has('loginvalue')) {
            $data = array(
                'title' => 'Corporate Category',
                'type' =>2,
            );
            return view('admin.category.master')->with($data);
        } else {
            return redirect('admin/login');
        }
        
    }
    public function reward(){
        if (Session::has('loginvalue')) {
            $data = array(
                'title' => 'Reward Category',
                'type' =>3,
            );
            return view('admin.category.master')->with($data);
        } else {
            return redirect('admin/login');
        }
        
    }
}