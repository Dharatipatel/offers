<?php

namespace App\Http\Controllers\Web\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Models\Slider;
use App\Http\Requests\Admin\SliderRequest;
use Yajra\DataTables\Facades\DataTables;
use DB;
use File;

class SliderController extends Controller {

    public function index() {
        $data['title'] = "Slider";
        $data['action'] = "Manage";
        $data['type'] = "list";
        return view('admin.slider.index', $data);
    }

    public function create() {
        $data['title'] = "Slider";
        $data['type'] = "add";
        return view('admin.slider.manage', $data);
    }

    public function get_list() {
        $model = Slider::select("*")->orderBy('id', 'DESC');

        return Datatables::eloquent($model)
                        
                        ->addColumn('action', function ($model) {
                            $edit_url = '<a href=' . route('admin.slider.edit', ['id' => $model->id]) . ' class="edit-list-btn btn btn-primary"><i class="fa fa-edit"></i> Edit</a>';
                            $deletelink = route('admin.slider.delete', ['id' => $model->id]);
                            $remove_url = '<a href="javascript:;" data-url="' . $deletelink . '" onclick="deleteRow(\'' . $model->id . '\');" class="edit-list-btn btn btn-danger" id="del_' . $model->id . '"><i class="fa fa-trash"></i> Delete</a>';

                            return $edit_url . " " . $remove_url;
                        })
                        ->make(true);
    }

    public function edit($id) {
        $data['type'] = "add";
        $slider = Slider::find($id);
        if ($slider != null) {
            return view('admin.slider.manage', ["title" => "Slider", "slider" => $slider]);
        } else {
            return redirect()->route('admin.slider.index')->withErrors(array('message' => 'Slider does not exist!'));
        }
    }

    public function store(SliderRequest $request, $id = '') {
        if ($id != '') {
            $slider = Slider::find($id);
            $old_file = $slider->image;
        } else {
            $slider = new Slider;
        }
        $slider->name = $request->name;
        $slider->url = $request->url;
        $slider->description = $request->description;
        if ($request->hasFile('file')) {
            $dir = base_path() . "/uploads/slider/";
            $image = $request->file('file');
            if (!file_exists($dir)) {
                File::makeDirectory($dir, 0777, true);
            }
            $imagename = time() . mt_rand(1000000, 9999999) . '.' . $image->getClientOriginalExtension();
            $destinationPath = $dir;
            $image->move($destinationPath, $imagename);
            $slider->image = $imagename;
            if (!empty($id) && file_exists($destinationPath . $old_file)) {
                File::delete($destinationPath . $old_file);
            }
        }
        if ($slider->save()) {
            if ($id != '') {
                $request->session()->flash('success', 'Slider Updated successfully!');
            } else {
                $request->session()->flash('success', 'Slider Added successfully!');
            }
            return redirect()->route('admin.slider.index');
        } else {
            if ($id != '') {
                return redirect()->route('admin.slider.index')->withErrors(array('message' => 'Error in slider Update!'));
            } else {
                return redirect()->route('admin.slider.index')->withErrors(array('message' => 'Error in mail template Add!'));
            }
        }
    }

    function delete($id) {
        $slider = Slider::find($id);
        if (count($slider) > 0) {
            if ($slider->delete()) {
                session()->flash('success', 'Slider removed successfully !');
                return redirect()->route('admin.slider.index');
            } else {
                return redirect()->route('admin.slider.index')->withErrors(array('message' => 'Error in Slider remove !'));
            }
        } else {
            return redirect()->route('admin.slider.index')->withErrors(array('message' => 'Slider not exist'));
        }
    }

}
