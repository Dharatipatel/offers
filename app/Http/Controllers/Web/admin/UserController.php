<?php
namespace App\Http\Controllers\Web\admin;

use App\Http\Controllers\Controller;
use Request;
use Session;


class UserController extends Controller
{
    public function login(){
    	return view('admin.login');
    }

    public function logout(){
        $redirect ='admin/login';
        if(Session::get('role') == 1 ){
            $redirect ='admin/login';
        } else if(Session::get('role') == 2){
            $redirect ='brand/login';
        } else if(Session::get('role') == 3){
            $redirect ='corporate/login';
        }
        $data = Session::flush();
        return redirect($redirect);
    }
    
    public function setSession(){
    	$input = Request::all();
    	if(!Session::has('loginvalue')){
    		Session::regenerate();
    		if (Request::has('token')) {
 				session(['token' => $input['token']]);
			}
			if (Request::has('_token')) {
 				session(['_token' => $input['_token']]);
			}
			if (Request::has('email')) {
 				session(['email' => $input['email']]);
			}
			if (Request::has('role_id')) {
 				session(['role' => $input['role_id']]);
			}
			if (Request::has('id')) {
 				session(['id' => $input['id']]);
			}
            if (Request::has('bid')) {
                session(['bid' => $input['bid']]);
            }
            if (Request::has('cid')) {
                session(['cid' => $input['cid']]);
            }
			session(['loginvalue' => 1]);
    	}
    	$data=Session::all();
    	return $data;
    }
    public function editProfile(){
        
        $data = array(
            'title' => 'Profile'
        );
        return view('admin.profile.edit')->with($data);
       
    }
    public function changePassword(){
       
        $data = array(
            'title' => 'Change Password'
        );
        return view('admin.profile.changepassword')->with($data);
        
    }
}