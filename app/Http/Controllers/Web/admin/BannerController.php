<?php

namespace App\Http\Controllers\Web\admin;

use App\Http\Controllers\Controller;
use App\Http\Models\Banner;
use App\Http\Requests\Admin\BannerRequest;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\File;

class BannerController extends Controller {

    public function index() {
        $data['title'] = "Banner";
        $data['action'] = "Manage";
        return view('admin.banner.index', $data);
    }

    public function manage($id = null) {

        if (!empty($id)) {
            $data['banner'] = Banner::find($id);
        }

        $data['title'] = "Transaction Type";

        return view('admin.banner.manage', $data);
    }

    public function lists() {
        $model = Banner::select("*")->orderBy('id', 'DESC');

        return Datatables::eloquent($model)
                        ->addColumn('image', function($model) {
                            return '<img width="250px" src="' . asset("uploads/banner/" . $model->image) . '">';
                        })
                        ->addColumn('action', function($model) {
                            $edit_url = '<a href=' . route("admin.banner.edit", $model->id) . ' class="edit-list-btn btn btn-primary"><i class="fa fa-edit"></i> Edit</a> ';
                            $remove_url = '<a href="javascript:;" data-url="' . route("admin.banner.delete", $model->id) . '" onclick="deleteRow(\'' . $model->id . '\');" class="edit-list-btn btn btn-danger" id="del_' . $model->id . '"><i class="fa fa-trash"></i> Delete</a>';
                            return $edit_url . $remove_url;
                        })
                        ->make(true);
    }

    public function save(BannerRequest $request) {
        try {
            $id = $request->get("id");

            $model = new Banner;
            $msg = "Banner added successfully.";

            if (!empty($id)) {
                $model = Banner::find($id);
                $msg = "Banner updated successfully.";
                $old_file = $model->image;
            }

            if ($request->hasFile('file')) {
                $dir = base_path() . "/uploads/banner/";
                $image = $request->file('file');
                if (!file_exists($dir)) {
                    File::makeDirectory($dir, 0777, true);
                }
                $imagename = time() . mt_rand(1000000, 9999999) . '.' . $image->getClientOriginalExtension();
                $destinationPath = $dir;
                $image->move($destinationPath, $imagename);
                $model->image = $imagename;
                if (!empty($id) && file_exists($destinationPath . $old_file)) {
                    File::delete($destinationPath . $old_file);
                }
            }
            $model->save();

            return redirect(route("admin.banner.index"))->withSuccess($msg);
        } catch (Exception $ex) {
            return redirect()->back();
        }
    }

    function delete($id) {

        $data = Banner::find($id);

        if (count($data) > 0) {
            $dir = base_path() . "/uploads/banner/";
             if (file_exists($dir . $data->image)) {
                    File::delete($dir . $data->image);
                }
            $delete = $data->delete();
            $to = route("admin.banner.index");
            if ($delete) {
                session()->flash('success', 'Banner removed successfully !');
                return redirect($to);
            } else {
                return redirect($to)->withErrors(array('message' => 'Error in Banner remove!'));
            }
        } else {
            return redirect($to)->withErrors(array('message' => "Banner does not exist"));
        }
    }

}
