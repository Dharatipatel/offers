<?php
namespace App\Http\Controllers\Web\admin;

use App\Http\Controllers\Controller;
use Request;
use Session;


class SMSTemplateController extends Controller
{
    public function __construct(){
       
    }

    public function index(){
       
        $data = array(
            'title' => 'SMS Template'
        );
        return view('admin.sms_template.master')->with($data);
    }
}