<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Http\Requests\User\UserLoginRequest;
use Exception;
use Auth;

class LoginController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Login Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles authenticating users for the application and
      | redirecting them to your home screen. The controller uses a trait
      | to conveniently provide its functionality to your applications.
      |
     */

use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = "/user/offers";

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest')->except('logout');
    }

    public function login(UserLoginRequest $request) {
        try {
            if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
                return redirect(route("user.offers"));
            } else {
                return redirect()->back()->withErrors(['error' => 'Invalid username or password']);
            }
        } catch (Exception $e) {
            return redirect()->back()->withErrors(['error' => 'Error while login']);
        }
    }

    public function show() {
        return view("auth.user.login");
    }

    public function user_logout() {
        if (Auth::check()) {
            Auth::logout();
        }
        return redirect(route("auth.user.login.show"));
    }

}
