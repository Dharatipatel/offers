<?php
namespace App\Http\Controllers\Api\admin;

use App\Http\Controllers\Controller;
use Request;
use Validator;
use Carbon\Carbon;
use App\Http\Models\User;
use App\Http\Models\Corporate;
use App\Http\Models\Reward;
use App\Http\Models\Brand;
use App\Http\Models\Employee;
use App\Http\Requests\StoreCorporateRequest;
use App\Http\Requests\UpdateCorporateRequest;
use App\Http\Requests\DeleteBrandRequest;

class CorporateController extends Controller
{
	public function __construct(){
		  $this->corporate = new Corporate;
          $this->user = new User;
          $this->reward = new Reward;
          $this->employee = new Employee;
		  $this->brand = new Brand;
    }

    public function addCorporate(StoreCorporateRequest $request){
    	$input = $request->validated();
    	$token = $this->createToken();   	
    	try{
    		$user_data =array(
    			'email'    => $input['email'],
        		'contact'  => $input['contact'],
        		'password'  => $input['password'],
        		'token'    =>$token,
        		'role_id' => 2,
        		'created'  => Carbon::now()->format('Y-m-d H:i:s'),
        		'modified'  => Carbon::now()->format('Y-m-d H:i:s'),
        		'status'   => $input['status'],
    		);        	
        	$user_result = $this->user->register($user_data);
        	if($user_result){
        		$corporate_data= array(
	        		'uid' => $user_result,
                    'name'    => $input['name'],
	        		'category'    => $input['category'],
	        		'description'    => $input['description'],
	        		'contact_person_name'  => $input['person_name'],
	        		'website_url'  => $input['website_url'],
                    'facebook_page'  => $input['facebook_page'],
	        		'linkedin_page'  => $input['linkedin_page'],
	        		'address'  => $input['address'],
	        		'logo' => "test.png",
	        		'created'  => Carbon::now()->format('Y-m-d H:i:s'),
	        		'modified'  => Carbon::now()->format('Y-m-d H:i:s'),
	        	);
        		$corporate_result = $this->corporate->addCorporate($corporate_data);
        		if($corporate_result){
                    if($request->has('image')){
                        $corporate_image_data = array();
                        $upload=file_put_contents($_SERVER['DOCUMENT_ROOT'].'/perksvilla/uploads/corporate/'.$corporate_result.'.png', base64_decode($request->get('image')));
                        if($upload){
                            $corporate_image_data['logo'] = $corporate_result.".png";
                            $corporate_image_result = $this->corporate->updateCorporate($corporate_image_data,$corporate_result);
                        }
                    }
	        		return response()->json([
					    'error'   => false,
					    'data' =>array('id' => $corporate_result),
					],201);
	        	} else{
	        		return response()->json([
					    'error'   => true,
					    'message' =>"Something went Wrong",
					],400);
        		}
        	} else{
	        		return response()->json([
					    'error'   => true,
					    'message' =>"Something went Wrong",
					],400);
        		}
        } catch(\Exception $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        } catch(\QueryException $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        }

    }
    public function updateCorporate(UpdateCorporateRequest $request){

    	$input = $request->validated();
    	$token = $this->createToken();   	
    	try{
    		$user_data =array(
    			'email'    => $input['email'],
        		'contact'  => $input['contact'],
        		'modified'  => Carbon::now()->format('Y-m-d H:i:s'),
        		'status'   => $input['status'],
    		);        	
        	$user_result = $this->user->updateUser($user_data,$input['uid']);
        	if($user_result){
        		$corporate_data= array(
	        		'name'    => $input['name'],
	        		'company_size'    => $input['company_size'],
	        		'description'    => $input['description'],
	        		'contact_person_name'  => $input['person_name'],
	        		'category'  => $input['category'],
	        		'website_url'  => $input['website_url'],
	        		'facebook_page'  => $input['facebook_page'],
	        		'address'  => $input['address'],
	        		'linkedin_page'  => $input['linkedin_page'],
	        		'modified'  => Carbon::now()->format('Y-m-d H:i:s'),
	        	);
                if($request->has('image')){
                    $upload=file_put_contents($_SERVER['DOCUMENT_ROOT'].'/perksvilla/uploads/corporate/'.$input['id'].'.png', base64_decode($request->get('image')));
                    if($upload)
                        $corporate_data['logo'] = $input['id'].".png";
                }
        		$corporate_result = $this->corporate->updateCorporate($corporate_data,$input['id']);
        		if($corporate_result){
	        		return response()->json([
					    'error'   => false,
					    'data' =>array('id' => $corporate_result),
					],200);
	        	} else{
	        		return response()->json([
					    'error'   => true,
					    'message' =>"Something went Wrong",
					],400);
        		}
        	} else{
	        		return response()->json([
					    'error'   => true,
					    'message' =>"Something went Wrong",
					],400);
        		}
        } catch(\Exception $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        } catch(\QueryException $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        }

    
    }
    public function getCorporate(){
    	$limit = 10000;
		if(Request::has('limit')){
			$limit = Request::get('limit');
		}

    	try{
			$result = $this->corporate->getCorporate($limit);
			return response()->json([
			    'error'   => false,
			    'data' => $result,
			],200);
        } catch(\Exception $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],400);
        } catch(\QueryException $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],400);
        }
    }
    public function getCorporateById($id){
    	
    	try{
            $result = $this->corporate->getCorporateById($id);
            if($result){
                return response()->json([
                    'error'   => false,
                    'data' => $result,
                 ],200);
            }else{
                 return response()->json([
                    'error'   => true,
                    'message' => "Invalid Corporate",
                ],404);
            }
        } catch(\Exception $e) {
            return response()->json([
                    'error'   => true,
                    'message' => $e->getMessage(),
                ],400);
        } catch(\QueryException $e) {
            return response()->json([
                    'error'   => true,
                    'message' => $e->getMessage(),
                ],400);
        } 
    }
    public function activeCorporate(DeleteBrandRequest $request){
    	$input = $request->validated();
    	try{
        	$data= array(
        		'status'    => 1,
        		'modified'  => Carbon::now()->format('Y-m-d H:i:s'),
        	);
        	$result = $this->corporate->activeCorporate($data,$input['id']);
        	if($result){
        		return response()->json([
				    'error'   => false,
				    'message' => "Corporate Activated Successfully",
				],200);
        	} else{
        		return response()->json([
				    'error'   => true,
				    'message' =>"Something went Wrong",
				],500);
        	}
        } catch(\Exception $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        } catch(\QueryException $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        }
    }

    public function deactiveCorporate(DeleteBrandRequest $request){
    	
    	$input = $request->validated();
    	try{
	    	
        	$data= array(
        		'status'    => 0,
        		'modified'  => Carbon::now()->format('Y-m-d H:i:s'),
        	);
        	$result = $this->corporate->deactiveCorporate($data,$input['id']);
        	if($result){
        		return response()->json([
				    'error'   => false,
				    'message' => "Corporate Deactivated Successfully",
				],200);
        	} else{
        		return response()->json([
				    'error'   => true,
				    'message' =>"Something went Wrong",
				],500);
        	}

        } catch(\Exception $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        } catch(\QueryException $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        }
    }
    
    public function deleteCorporate(DeleteBrandRequest $request){
    	try{
	    	$input = $request->validated();
        	$input['status'] = 3;
        	$input['modified'] = Carbon::now()->format('Y-m-d H:i:s');
        	$result = $this->corporate->deleteCorporate($input);
        	if($result){
        		return response()->json([
				    'error'   => false,
				    'message' =>"Corporate deleted Successfully",
				],200);
        	} else{
        		return response()->json([
				    'error'   => true,
				    'message' =>"Something went Wrong",
				],500);
        	}	
        } catch(\Exception $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        } catch(\QueryException $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        }
    }
    public function getStatistics(){
       try{
            $use = array();
            $unuse = array();
            $employee = array();
            if(Request::has('cid')){
                $unuse['cid'] = Request::get('cid');
                $use['cid'] = Request::get('cid');
                $employee['cid'] = Request::get('cid');
                $amount_invested['cid'] = Request::get('cid');  
            }
            $unuse['redeem'] =  0;
            $use['redeem'] =1;
            $employee['status'] = 1;  
            $data = array(
                'employee'        => $this->employee->getEmployeeCount($employee),
                'brand'           => $this->brand->getBrandCount(array('status'=>1)),
                'unused_reward'   => $this->reward->getRewardUsed($unuse),
                'used_reward'     => $this->reward->getRewardUsed($use),
                'offer'           => 2,
                'amount_saved'    => 0,
                'amount_invested' => $this->reward->getInvestedAmount($amount_invested),
                'social'          => 0,
                'availble_deals'  => 0,
            );
            if(!empty($data)){
                return response()->json([
                    'error'   => false,
                    'data' =>$data,
                ],200);
            } else{
                return response()->json([
                    'error'   => true,
                    'message' =>"Something went Wrong",
                ],500);
            }   
        } catch(\Exception $e) {
            return response()->json([
                    'error'   => true,
                    'message' => $e->getMessage(),
                ],500);
        } catch(\QueryException $e) {
            return response()->json([
                    'error'   => true,
                    'message' => $e->getMessage(),
                ],500);
        } 
    }
    private function createToken(){
    	return str_random(32);
    }
}