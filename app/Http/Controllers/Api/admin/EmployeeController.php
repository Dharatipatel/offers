<?php
namespace App\Http\Controllers\Api\admin;

use App\Http\Controllers\Controller;
use Request;
use Validator;
use Carbon\Carbon;
use App\Http\Models\Employee;

class EmployeeController extends Controller
{
	public function __construct(){
		  $this->employee = new Employee;
    }

    public function addEmployee(){
    	$input = Request::all();
    	$validator = Validator::make($input, [
	        'first_name' 		=> 'required',
	        'last_name' 		=> 'required',
	        'designation' 		=> 'required',
	        'gender' 		=> 'required',
	        'contact' 		=> 'required|numeric|unique:employee',
	        'email' => 'required|unique:employee',
	        'joining_date' => 'required',
	        'birth_date' => 'required',
	        'profile_pic' => 'required',
	        'corporate_id' => 'required',
    	]);
    	try{
	    	if ($validator->fails()) {
	    		return response()->json([
				    'error'   => true,
				    'message' => $validator->errors()->first()
				],500);
	        } else {

	        	$data= array(
	        		'first_name'    => $input['first_name'],
	        		'last_name'    => $input['last_name'],
	        		'designation'    => $input['designation'],
	        		'email'    => $input['email'],
	        		'gender'    => $input['gender'],
	        		'contact'  => $input['contact'],
	        		'joining_date'  => $input['joining_date'],
	        		'birth_date'  => $input['birth_date'],
	        		'profile_pic'  => "test.png",
	        		'corporate_id'  => $input['corporate_id'],
	        		'status'   => 0,
	        		'created'  => Carbon::now()->format('Y-m-d H:i:s'),
	        		'modified'  => Carbon::now()->format('Y-m-d H:i:s'),
	        	);
	        	$result = $this->employee->addEmployee($data);
	        	
	        	if($result){
	        		if(Request::has('profile_pic')){
	                    $upload=file_put_contents($_SERVER['DOCUMENT_ROOT'].'/perksvilla/uploads/employee/'.$result.'.png', base64_decode(Request::get('profile_pic')));
	                    if($upload)
	                        $data['profile_pic'] = $result.".png";
	                    	$data['id'] = $result;
	                	}
	                	$update_result = $this->employee->updateEmployee($data);
	                	if($update_result){
	                		return response()->json([
							    'error'   => false,
							    'data' =>array('id' => $result),
							],200);
	                	}else{
	                		return response()->json([
							    'error'   => true,
							    'message' =>"Something went Wrong",
							],500);
	                	}
	        		
	        	} else{
	        		return response()->json([
					    'error'   => true,
					    'message' =>"Something went Wrong",
					],500);
	        	}
        	}
        } catch(\Exception $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        } catch(\QueryException $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        }

    }
    public function getEmployee(){

    	try{
	    	$data = array();
		    if(Request::has('status')){
			     $data['status'] = Request::get('status');
		    }
	        if(Request::has('cid')){
	            $data['cid'] = Request::get('cid');
	        }
	        if(Request::has('q')){
	            $data['q'] = Request::get('q');
	        }
	        if(Request::has('eid')){
	            $data['eid'] = Request::get('eid');
	        }
			$result = $this->employee->getEmployee($data);
			return response()->json([
			    'error'   => false,
			    'data' => $result,
			],200);
        } catch(\Exception $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				]);
        } catch(\QueryException $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				]);
        }
    }
    public function activeEmployee(){
    	$input = Request::all();
    	$validator = Validator::make($input, [
	        'id' => 'required|numeric'
    	]);
    	try{
	    	if ($validator->fails()) {
	    		return response()->json([
				    'error'   => true,
				    'message' => $validator->errors()->first()
				],500);
	        } else {
	        	$data= array(
	        		'status'    => 1,
	        		'modified'  => Carbon::now()->format('Y-m-d H:i:s'),
	        	);
	        	$result = $this->employee->activeEmployee($data,$input['id']);
	        	if($result){
	        		return response()->json([
					    'error'   => false,
					    'message' => "Employee Activated Successfully",
					],200);
	        	} else{
	        		return response()->json([
					    'error'   => true,
					    'message' =>"Something went Wrong",
					],500);
	        	}
        	}
        } catch(\Exception $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],400);
        } catch(\QueryException $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],400);
        }
    }
    public function deactiveEmployee(){
    	
    	$input = Request::all();
    	$validator = Validator::make($input, [
	        'id' => 'required|numeric'
    	]);
    	try{
	    	if ($validator->fails()) {
	    		return response()->json([
				    'error'   => true,
				    'message' => $validator->errors()->first()
				],500);
	        } else {
	        	$data= array(
	        		'status'    => 0,
	        		'modified'  => Carbon::now()->format('Y-m-d H:i:s'),
	        	);
	        	$result = $this->employee->deactiveEmployee($data,$input['id']);
	        	if($result){
	        		return response()->json([
					    'error'   => false,
					    'message' => "Employee Deactivated Successfully",
					],200);
	        	} else{
	        		return response()->json([
					    'error'   => true,
					    'message' =>"Something went Wrong",
					],500);
	        	}
        	}
        } catch(\Exception $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],400);
        } catch(\QueryException $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],400);
        }
    }
    public function updateEmployee(){
    	$input = Request::all();
    	$validator = Validator::make($input, [
    		'id' => 'required|numeric',
	        'first_name' 		=> 'required',
	        'last_name' 		=> 'required',
	        'gender' 		=> 'required',
	        'designation' 		=> 'required',
	        'contact' 		=> 'required|numeric',
	        'email' => 'required',
	        'joining_date' => 'required',
	        'birth_date' => 'required'
    	]);
    	try{
	    	if ($validator->fails()) {
	    		return response()->json([
				    'error'   => true,
				    'message' => $validator->errors()->first()
				],500);
	        } else {
	        	$input['modified'] = Carbon::now()->format('Y-m-d H:i:s');
	        	if(Request::has('profile_pic')){
                    $upload = file_put_contents($_SERVER['DOCUMENT_ROOT'].'/perksvilla/uploads/employee/'.$input['id'].'.png', base64_decode(Request::get('profile_pic')));
	                    if($upload){
	                        $input['profile_pic'] = $input['id'].".png";
	                    }
	                	
                }
	        	$result = $this->employee->updateEmployee($input);
	        	if($result){
	        		return response()->json([
					    'error'   => false,
					    'message' =>"Employee update Successfully",
					],200);
	        	} else{
	        		return response()->json([
					    'error'   => true,
					    'message' =>"Something went Wrong",
					],500);
	        	}
        	}
        } catch(\Exception $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        } catch(\QueryException $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        }

    }
    public function deleteEmployee(){
    	$input = Request::all();
    	$validator = Validator::make($input, [
    		'id' => 'required|numeric',
	       	]);
    	try{
	    	if ($validator->fails()) {
	    		return response()->json([
				    'error'   => true,
				    'message' => $validator->errors()->first()
				]);
	        } else {
	        	$input['status'] = 3;
	        	$input['modified'] = Carbon::now()->format('Y-m-d H:i:s');
	        	$result = $this->employee->deleteEmployee($input);
	        	if($result){
	        		return response()->json([
					    'error'   => false,
					    'message' =>"Employee deleted Successfully",
					]);
	        	} else{
	        		return response()->json([
					    'error'   => true,
					    'message' =>"Something went Wrong",
					]);
	        	}
        	}
        } catch(\Exception $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				]);
        } catch(\QueryException $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				]);
        }
    }
    public function importEmployee(){
    	try{
	    	$name = $_FILES['employeecsv']['name'];
  			$size = $_FILES['employeecsv']['size'];
			$filename = md5(uniqid().time()).".csv";
            $tmp = $_FILES['employeecsv']['tmp_name'];

            if (move_uploaded_file($tmp, $_SERVER['DOCUMENT_ROOT'].'/perksvilla/uploads/csv/'.$filename)) {
            	$file = fopen($_SERVER['DOCUMENT_ROOT'].'/perksvilla/uploads/csv/'.$filename,"r");
            	$i=0;
				while($employee = fgetcsv($file)){
					if($i>0){
						$data = array(
			        		'first_name'    => $employee[0],
			        		'last_name'    => $employee[1],
			        		'gender'    => $employee[2],
			        		'designation'    => $employee[3],
			        		'contact'  => $employee[4],
			        		'email'    => $employee[5],
			        		'joining_date'  => $employee[6],
			        		'birth_date'  => $employee[7],
			        		'profile_pic'  => $employee[8],
			        		'corporate_id'  => $employee[9],
			        		'status'   => $employee[10],
			        		'created'  => Carbon::now()->format('Y-m-d H:i:s'),
			        		'modified'  => Carbon::now()->format('Y-m-d H:i:s'),
		        		); 
					$result = $this->employee->addEmployee($data);
					}
					$i++;	
				}
				exit;
				if($result){
					return response()->json([
					    'error'   => true,
					    'message' => "Employee Inserted Successfully",
					],200);
				}else{
					return response()->json([
					    'error'   => true,
					    'message' => "Something went Wrong",
					],400);
				}
		    } else {
		    	return response()->json([
				    'error'   => true,
				    'message' => "File is not uploaded",
				],500);
		    }    	
        } catch(\Exception $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        } catch(\QueryException $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        }
    }
}