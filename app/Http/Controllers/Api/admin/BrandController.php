<?php

namespace App\Http\Controllers\Api\admin;

use App\Http\Controllers\Controller;
use Request;
use Validator;
use Carbon\Carbon;
use App\Http\Models\User;
use App\Http\Models\Brand;
use App\Http\Models\City;
use App\Http\Requests\StoreBrandRequest;
use App\Http\Requests\UpdateBrandRequest;
use App\Http\Requests\DeleteBrandRequest;
use App\Http\Requests\UpdateWalletBrandRequest;
use App\Http\Requests\AddAddressRequest;
use App\Http\Requests\Admin\EmailSendRequest;
use App\Custom\Email;

class BrandController extends Controller {

    public function __construct() {
        $this->brand = new Brand;
        $this->user = new User;
        $this->city = new City;
    }

    public function addBrand(StoreBrandRequest $request) {
        $input = $request->validated();
        $token = $this->createToken();
        try {
            $user_data = array(
                'email' => $input['email'],
                'contact' => $input['contact'],
                'password' => $input['password'],
                'token' => $token,
                'role_id' => 2,
                'created' => Carbon::now()->format('Y-m-d H:i:s'),
                'modified' => Carbon::now()->format('Y-m-d H:i:s'),
                'status' => $input['status'],
            );
            $user_result = $this->user->register($user_data);
            if ($user_result) {
                $brand_data = array(
                    'uid' => $user_result,
                    'name' => $input['brand_name'],
                    'brand_category' => $input['category_name'],
                    'description' => $input['description'],
                    'contact_person_name' => $input['person_name'],
                    'business_hours' => $input['business_hours'],
                    'website_url' => $input['website_url'],
                    'facebook_page' => $input['facebook_page'],
                    'logo' => "test.png",
                    'created' => Carbon::now()->format('Y-m-d H:i:s'),
                    'modified' => Carbon::now()->format('Y-m-d H:i:s'),
                );
                $brand_result = $this->brand->addBrand($brand_data);
                if ($brand_result) {
                    $brand_img_data = array();
                    if ($request->has('image')) {
                        $upload = file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/perksvilla/uploads/brand/' . $brand_result . '.png', base64_decode($request->get('image')));
                        if ($upload){
                            $brand_img_data['logo'] = $brand_result . ".png";
                            
                        }
                        $brand_update_result = $this->brand->updateBrand($brand_img_data, $brand_result);
                        if($brand_update_result){
                            return response()->json([
                                'error' => false,
                                'data' => array('id' => $brand_result),
                                    ], 201);
                        } else {
                         return response()->json([
                                'error' => true,
                                'message' => "Something went Wrong",
                                    ], 400);
                        }
                    }else{
                        return response()->json([
                                'error' => false,
                                'data' => array('id' => $brand_result),
                                    ], 201);
                    }
                } else {
                    return response()->json([
                                'error' => true,
                                'message' => "Something went Wrong",
                                    ], 400);
                }
            } else {
                return response()->json([
                            'error' => true,
                            'message' => "Something went Wrong",
                                ], 400);
            }
        } catch (\Exception $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 500);
        } catch (\QueryException $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 500);
        }
    }

    public function updateBrand(UpdateBrandRequest $request) {

        $input = $request->validated();
        $token = $this->createToken();
        try {
            $user_data = array(
                'email' => $input['email'],
                'contact' => $input['contact'],
                'modified' => Carbon::now()->format('Y-m-d H:i:s'),
                'status' => $input['status'],
            );
            $user_result = $this->user->updateUser($user_data, $input['uid']);
            if ($user_result) {
                $brand_data = array(
                    'name' => $input['brand_name'],
                    'brand_category' => $input['category_name'],
                    'description' => $input['description'],
                    'contact_person_name' => $input['person_name'],
                    'business_hours' => $input['business_hours'],
                    'website_url' => $input['website_url'],
                    'facebook_page' => $input['facebook_page'],
                    'modified' => Carbon::now()->format('Y-m-d H:i:s'),
                );
                if ($request->has('image') && $request->get('image') != "") {
                        $image_folder =$_SERVER['DOCUMENT_ROOT'] . '/offers/uploads/brand';
                        
                    $upload = file_put_contents($image_folder . '/' . $input['id'] . '.png', base64_decode($request->get('image')));
                    if ($upload){
                        chmod($image_folder.'/' . $input['id'] . '.png', 0777);
                        $brand_data['logo'] = $input['id'] . ".png";
                    }
                }
                if (Request::has('updateaddress')) {
                    $update_address = $this->updateBrandAddress(Request::get('addressArray'), $input['id']);
                }
                $brand_result = $this->brand->updateBrand($brand_data, $input['id']);
                if ($brand_result) {
                    return response()->json([
                                'error' => false,
                                'data' => array('update' => $brand_result),
                                    ], 200);
                } else {
                    return response()->json([
                                'error' => true,
                                'message' => "Something went Wrong",
                                    ], 400);
                }
            } else {
                return response()->json([
                            'error' => true,
                            'message' => "Something went Wrong",
                                ], 400);
            }
        } catch (\Exception $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 500);
        } catch (\QueryException $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 500);
        }
    }

    public function brandWallet(UpdateWalletBrandRequest $request) {
        $input = $request->validated();
        try {
            $user_balance = $this->brand->getBalance(array('user_id' => $input['user_id']));

            if (!empty($user_balance) && $user_balance[0]->balance >= $input['txtAmount']) {
                $user_data = array(
                    'user_id' => $input['user_id'],
                    'withdraw_amount' => $input['txtAmount'],
                    'created' => Carbon::now()->format('Y-m-d H:i:s'),
                    'modified' => Carbon::now()->format('Y-m-d H:i:s'),
                );
                $result = $this->brand->addWallet($user_data);
                if ($result) {
                    return response()->json([
                                'error' => false,
                                'data' => $result,
                                    ], 200);
                } else {
                    return response()->json([
                                'error' => false,
                                'data' => "Something Went wrong",
                                    ], 200);
                }
            } else {
                return response()->json([
                            'error' => true,
                            'message' => "Brand has no suffiecient balance",
                                ], 400);
            }
        } catch (\Exception $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 400);
        } catch (\QueryException $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 400);
        }
    }

    public function brandWalletTranscation() {
        $data = array();
        if (Request::has('user_id')) {
            $data['user_id'] = Request::get('user_id');
        }

        try {
            $result = $this->brand->getWalletTransaction($data);
            return response()->json([
                        'error' => false,
                        'data' => $result,
                            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 400);
        } catch (\QueryException $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 400);
        }
    }

    public function getBalance() {
        $data = array();
        if (Request::has('user_id')) {
            $data['user_id'] = Request::get('user_id');
        }
        try {
            $result = $this->brand->getBalance($data);
            return response()->json([
                        'error' => false,
                        'data' => $result,
                            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 400);
        } catch (\QueryException $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 400);
        }
    }

    public function getBrand() {
        $limit = 100;
        if (Request::has('limit')) {
            $limit = Request::get('limit');
        }

        try {
            $result = $this->brand->getBrand($limit);
            return response()->json([
                        'error' => false,
                        'data' => $result,
                            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 400);
        } catch (\QueryException $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 400);
        }
    }

    public function getBrandOffer() {
        try {
            $data = array();
            $result = $this->brand->getBrandOffer($data);
            return response()->json([
                        'error' => false,
                        'data' => $result,
                            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 400);
        } catch (\QueryException $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 400);
        }
    }

    public function getBrandById($id) {

        try {
            $result = $this->brand->getBrandById($id);
            if ($result) {
                return response()->json([
                            'error' => false,
                            'data' => $result,
                                ], 200);
            } else {
                return response()->json([
                            'error' => true,
                            'message' => "Test",
                                ], 404);
            }
        } catch (\Exception $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 400);
        } catch (\QueryException $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 400);
        }
    }

    public function activeBrand(DeleteBrandRequest $request) {
        $input = $request->validated();
        try {
            $data = array(
                'status' => 1,
                'modified' => Carbon::now()->format('Y-m-d H:i:s'),
            );
            $result = $this->brand->activeBrand($data, $input['id']);
            if ($result) {
                return response()->json([
                            'error' => false,
                            'message' => "Brand Activated Successfully",
                                ], 200);
            } else {
                return response()->json([
                            'error' => true,
                            'message' => "Something went Wrong",
                                ], 500);
            }
        } catch (\Exception $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 500);
        } catch (\QueryException $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 500);
        }
    }

    public function deactiveBrand(DeleteBrandRequest $request) {

        $input = $request->validated();
        try {

            $data = array(
                'status' => 0,
                'modified' => Carbon::now()->format('Y-m-d H:i:s'),
            );
            $result = $this->brand->deactiveBrand($data, $input['id']);
            if ($result) {
                return response()->json([
                            'error' => false,
                            'message' => "Brand Deactivated Successfully",
                                ], 200);
            } else {
                return response()->json([
                            'error' => true,
                            'message' => "Something went Wrong",
                                ], 500);
            }
        } catch (\Exception $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 500);
        } catch (\QueryException $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 500);
        }
    }

    public function deleteBrand(DeleteBrandRequest $request) {
        try {
            $input = $request->validated();
            $input['status'] = 3;
            $input['modified'] = Carbon::now()->format('Y-m-d H:i:s');
            $result = $this->brand->deleteBrand($input);
            if ($result) {
                return response()->json([
                            'error' => false,
                            'message' => "Brand deleted Successfully",
                                ], 200);
            } else {
                return response()->json([
                            'error' => true,
                            'message' => "Something went Wrong",
                                ], 500);
            }
        } catch (\Exception $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 500);
        } catch (\QueryException $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 500);
        }
    }

    private function createToken() {
        return str_random(32);
    }
    public function updateBrandAddress($address, $bid) {
        $brand_address_data = array();
        $address_exist = $this->brand->getAddress($bid);
        if (count($address_exist) > 0) {
            $deleted = $this->brand->removeAddress($bid);
        }
        foreach ($address as $key => $value) {
            $location = $this->city->getStateCountry(array('city_id' => $value['city']));
            $brand_address_data[$key]['brand_id'] = $bid;
            $brand_address_data[$key]['address'] = $value['address'];
            $brand_address_data[$key]['pincode'] = $value['pincode'];
            $brand_address_data[$key]['country'] = $location->country_id;
            $brand_address_data[$key]['state'] = $location->state_id;
            $brand_address_data[$key]['city'] = $value['city'];
        }
        $brand_address_result = $this->brand->addAddress($brand_address_data);
    }
    public function addBrandAddress(AddAddressRequest $request) {
        $input = $request->validated();
        try {
            $brand_address_data = array();
            $address_exist = $this->brand->getAddress($input['bid']);
            if (count($address_exist) > 0) {
                $deleted = $this->brand->removeAddress($input['bid']);
            }
            foreach ($input['addressArray'] as $key => $value) {
                $brand_address_data[$key]['brand_id'] = $input['bid'];
                $brand_address_data[$key]['address'] = $value['address'];
                $brand_address_data[$key]['pincode'] = $value['pincode'];
                $brand_address_data[$key]['country'] = $value['country'];
                $brand_address_data[$key]['state'] = $value['state'];
                $brand_address_data[$key]['city'] = $value['city'];
            }
            $brand_address_result = $this->brand->addAddress($brand_address_data);
            if ($brand_address_result) {
                return response()->json([
                            'error' => false,
                            'data' => array('id' => $brand_address_result),
                                ], 201);
            } else {
                return response()->json([
                            'error' => true,
                            'message' => "Something went Wrong",
                                ], 400);
            }
        } catch (\Exception $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 500);
        } catch (\QueryException $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 500);
        }
    }

    public function emailSend(EmailSendRequest $request) {

        try {
            $data = $request;
            $result = $this->brand->savePassword($data);
            if ($result != null) {

                $email = new Email;
                $mailData = [
                    "name" => $result->user_name,
                    "email" => "udayraj.kachhadiya@gmail.com",
                    "password" => $data->password
                ];

                $email->send("user_password", $mailData);

                return response()->json([
                            'error' => false,
                            'data' => [],
                            'message' => "Email send successfully",
                                ], 200);
            } else {
                return response()->json([
                            'error' => true,
                            'message' => "Something went Wrong",
                                ], 500);
            }
        } catch (\Exception $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 400);
        }
    }

}
