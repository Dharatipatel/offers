<?php

namespace App\Http\Controllers\Api\admin;

use App\Http\Controllers\Controller;
use Request;
use Validator;
use Carbon\Carbon;
use App\Http\Models\SMSTemplate;
use App\Http\Requests\StoreSMSTemplateRequest;
use App\Http\Requests\UpdateSMSTemplateRequest;
use App\Http\Requests\DeleteSMSTemplateRequest;
use DB;

class SMSTemplateController extends Controller {

    public function __construct() {
        $this->sms_template = new SMSTemplate;
    }

    public function addSMSTemplate(StoreSMSTemplateRequest $request) {
        $input = $request->validated();
        $token = $this->createToken();
        try {

            $sms_template_data = array(
                "title" => $input["name"],
                "text" => $input["message"]
            );
            $sms_template_result = $this->sms_template->addSMSTemplate($sms_template_data);
            if ($sms_template_result) {
                return response()->json([
                            'error' => false,
                            'data' => array('id' => $sms_template_result),
                                ], 201);
            } else {
                return response()->json([
                            'error' => true,
                            'message' => "Something went Wrong",
                                ], 400);
            }
        } catch (\Exception $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 500);
        } catch (\QueryException $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 500);
        }
    }

    public function updateSMSTemplate(UpdateSMSTemplateRequest $request) {
        $input = $request->validated();
        $token = $this->createToken();
        try {
            $sms_template_data = array(
                "title" => $input["name"],
                "text" => $input["message"],
            );
            
            $sms_template_result = $this->sms_template->updateSMSTemplate($sms_template_data, $input['id']);
            if ($sms_template_result) {
                return response()->json([
                            'error' => false,
                            'data' => array('id' => $sms_template_result),
                                ], 200);
            } else {
                return response()->json([
                            'error' => true,
                            'message' => "Something went Wrong",
                                ], 400);
            }
        } catch (\Exception $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 500);
        } catch (\QueryException $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 500);
        }
    }

    public function getSMSTemplate() {
        $limit = 10;
        if (Request::has('limit')) {
            $limit = Request::get('limit');
        }

        try {
            $result = $this->sms_template->getSMSTemplate($limit);
            return response()->json([
                        'error' => false,
                        'data' => $result,
                            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 400);
        } catch (\QueryException $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 400);
        }
    }

    public function getSMSTemplateById($id) {

        try {
            $result = $this->sms_template->getSMSTemplateById($id);
            if ($result) {
                return response()->json([
                            'error' => false,
                            'data' => $result,
                                ], 200);
            } else {
                return response()->json([
                            'error' => true,
                            'message' => "Invalid SMS Template",
                                ], 404);
            }
        } catch (\Exception $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 400);
        } catch (\QueryException $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 400);
        }
    }

    public function deleteSMSTemplate(DeleteSMSTemplateRequest  $request) {
        try {
            $input = $request->validated();
            $result = $this->sms_template->deleteSMSTemplate($input);
            if ($result) {
                return response()->json([
                            'error' => false,
                            'message' => "SMS Template deleted Successfully",
                                ], 200);
            } else {
                return response()->json([
                            'error' => true,
                            'message' => "Something went Wrong",
                                ], 500);
            }
        } catch (\Exception $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 500);
        } catch (\QueryException $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 500);
        }
    }

    private function createToken() {
        return str_random(32);
    }

}
