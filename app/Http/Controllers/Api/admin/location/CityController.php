<?php
namespace App\Http\Controllers\Api\admin\location;

use App\Http\Controllers\Controller;
use Request;
use Validator;
use Carbon\Carbon;
use App\Http\Models\City;
use App\Http\Requests\StoreCityRequest;
use App\Http\Requests\UpdateCityRequest;
use App\Http\Requests\DeleteBrandRequest;

class CityController extends Controller
{
	public function __construct(){
		  $this->city = new City;
    }

    public function addCity(StoreCityRequest $request){
    	$input = $request->validated();	
    	try{
        		$city_data= array(
                    'name'    => $input['txtCityName'],
                    'state_id'    => $input['txtState'],
	        		'status'    => $input['status'],
	        		'created'  => Carbon::now()->format('Y-m-d H:i:s'),
	        		'modified'  => Carbon::now()->format('Y-m-d H:i:s'),
	        	);
        		$city_result = $this->city->addCity($city_data);
        		if($city_result){
	        		return response()->json([
					    'error'   => false,
					    'data' =>array('id' => $city_result),
					],201);
	        	} else{
	        		return response()->json([
					    'error'   => true,
					    'message' =>"Something went Wrong",
					],400);
        		}
        } catch(\Exception $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        } catch(\QueryException $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        }

    }
    public function updateCity(UpdateCityRequest $request){

    	$input = $request->validated();
    	  	
    	try{
    		
    		$city_data= array(
                'name'    => $input['txtCityName'],
                'state_id'    => $input['txtState'],
                'status'    => $input['status'],
        		'modified'  => Carbon::now()->format('Y-m-d H:i:s'),
        	);
            
    		$city_result = $this->city->updateCity($city_data,$input['id']);
    		if($city_result){
        		return response()->json([
				    'error'   => false,
				    'data' =>array('id' => $city_result),
				],200);
        	} else{
        		return response()->json([
				    'error'   => true,
				    'message' =>"Something went Wrong",
				],400);
    		}
        } catch(\Exception $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        } catch(\QueryException $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        }

    
    }
    public function getCity(){
		
        $data=array();
        if(Request::has('state')){
            $data['state'] = Request::get('state');
        }
        if(Request::has('status')){
            $data['status'] = Request::get('status');
        }
    	try{
			$result = $this->city->getCity($data);
			return response()->json([
			    'error'   => false,
			    'data' => $result,
			],200);
        } catch(\Exception $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],400);
        } catch(\QueryException $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],400);
        }
    }
    // public function getCountryById($id){
    	
    // 	try{
    //         $result = $this->brand->getBrandById($id);
    //         if($result){
    //             return response()->json([
    //                 'error'   => false,
    //                 'data' => $result,
    //              ],200);
    //         }else{
    //              return response()->json([
    //                 'error'   => true,
    //                 'message' => "Invalid Brand",
    //             ],404);
    //         }
    //     } catch(\Exception $e) {
    //         return response()->json([
    //                 'error'   => true,
    //                 'message' => $e->getMessage(),
    //             ],400);
    //     } catch(\QueryException $e) {
    //         return response()->json([
    //                 'error'   => true,
    //                 'message' => $e->getMessage(),
    //             ],400);
    //     } 
    // }
    public function activeCity(DeleteBrandRequest $request){
    	$input = $request->validated();
    	try{
        	$data= array(
        		'status'    => 1,
        		'modified'  => Carbon::now()->format('Y-m-d H:i:s'),
        	);
        	$result = $this->city->activeCity($data,$input['id']);
        	if($result){
        		return response()->json([
				    'error'   => false,
				    'message' => "City Activated Successfully",
				],200);
        	} else{
        		return response()->json([
				    'error'   => true,
				    'message' =>"Something went Wrong",
				],500);
        	}
        } catch(\Exception $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        } catch(\QueryException $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        }
    }

    public function deactiveCity(DeleteBrandRequest $request){
    	
    	$input = $request->validated();
    	try{
	    	
        	$data= array(
        		'status'    => 0,
        		'modified'  => Carbon::now()->format('Y-m-d H:i:s'),
        	);
        	$result = $this->city->deactiveCity($data,$input['id']);
        	if($result){
        		return response()->json([
				    'error'   => false,
				    'message' => "City Deactivated Successfully",
				],200);
        	} else{
        		return response()->json([
				    'error'   => true,
				    'message' =>"Something went Wrong",
				],500);
        	}

        } catch(\Exception $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        } catch(\QueryException $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        }
    }
    
    public function deleteCity(DeleteBrandRequest $request){
    	try{
	    	$input = $request->validated();
        	$input['status'] = 3;
        	$input['modified'] = Carbon::now()->format('Y-m-d H:i:s');
        	$result = $this->city->deleteCity($input);
        	if($result){
        		return response()->json([
				    'error'   => false,
				    'message' =>"City deleted Successfully",
				],200);
        	} else{
        		return response()->json([
				    'error'   => true,
				    'message' =>"Something went Wrong",
				],500);
        	}	
        } catch(\Exception $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        } catch(\QueryException $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        }
    }
}