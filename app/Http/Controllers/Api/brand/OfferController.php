<?php

namespace App\Http\Controllers\Api\brand;

use App\Http\Controllers\Controller;
use Request;
use Validator;
use Carbon\Carbon;
use App\Http\Models\Offer;
use App\Http\Requests\StoreOfferRequest;
use App\Http\Requests\DeleteBrandRequest;
use File;
use DB;
use App\Http\Models\BrandAddress;

// use App\Http\Requests\UpdateOfferRequest;

class OfferController extends Controller {

    public function __construct() {
        $this->offer = new Offer;
    }

    public function addOffer(StoreOfferRequest $request) {

        $input = $request->validated();
        try {
            $offer_data = array(
                'title' => $input['title'],
                'description' => $input['description'],
                'value' => $input['value'],
                'offer_type' => $input['offer_type'],
                'apply_on' => $input['apply_on'],
                'brand_address_id' => implode(",", $input['brand_address_id']),
                'valid_to' => $input['valid_to'],
                'count' => $input['count'],
                'publish_date' => $input['publish_date'],
                'notes' => $input['notes'],
                'brand_id' => $input['brand_id'],
                'offer_status_id' => 3,
                'created' => Carbon::now()->format('Y-m-d H:i:s'),
                'modified' => Carbon::now()->format('Y-m-d H:i:s'),
            );
            if ($request->has('ads_count') && $request->has('ads_count') != null) {
                $offer_data['ads_count'] = $request->get('ads_count');
            }
            if ($request->has('image') && $request->has('image') != null) {
                $image_folder = $_SERVER['DOCUMENT_ROOT'] . '/offers/uploads/offers';
                if (!is_dir($image_folder)) {
                    mkdir($image_folder, 0777, true);
                }
                $image_name = time() . mt_rand(1000000, 9999999) . '.png';
                $upload = file_put_contents($image_folder . '/' . $image_name, base64_decode($request->get('image')));
                if ($upload) {
                    chmod($image_folder . '/' . $image_name, 0777);
                    $offer_data['image'] = $image_name;
                }
            }

            $offer_result = $this->offer->addOffer($offer_data);
            if ($offer_result) {
                return response()->json([
                            'error' => false,
                            'data' => $offer_result,
                                ], 201);
            } else {
                return response()->json([
                            'error' => true,
                            'message' => "Something went Wrong",
                                ], 400);
            }
        } catch (\Exception $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 500);
        } catch (\QueryException $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 500);
        }
    }

    //   public function updateBrand(UpdateBrandRequest $request){
    //   	$input = $request->validated();
    //   	$token = $this->createToken();   	
    //   	try{
    //   		$user_data =array(
    //   			'email'    => $input['email'],
    //       		'contact'  => $input['contact'],
    //       		'modified'  => Carbon::now()->format('Y-m-d H:i:s'),
    //       		'status'   => $input['status'],
    //   		);        	
    //       	$user_result = $this->user->updateUser($user_data,$input['uid']);
    //       	if($user_result){
    //       		$brand_data= array(
    //        		'name'    => $input['brand_name'],
    //        		'brand_category'    => $input['category_name'],
    //        		'description'    => $input['description'],
    //        		'contact_person_name'  => $input['person_name'],
    //        		'business_hours'  => $input['business_hours'],
    //        		'website_url'  => $input['website_url'],
    //        		'facebook_page'  => $input['facebook_page'],
    //        		'address'  => $input['address'],
    //        		'pincode'  => $input['pincode'],
    //        		'status'   => $input['status'],
    //        		'logo' => "test.png",
    //        		'modified'  => Carbon::now()->format('Y-m-d H:i:s'),
    //        	);
    //               if($request->has('image')){
    //                   $upload=file_put_contents($_SERVER['DOCUMENT_ROOT'].'/perksvilla/uploads/brand/'.$input['id'].'.png', base64_decode($request->get('image')));
    //                   if($upload)
    //                       $brand_data['logo'] = $input['id'].".png";
    //               }
    //       		$brand_result = $this->brand->updateBrand($brand_data,$input['id']);
    //       		if($brand_result){
    //        		return response()->json([
    // 			    'error'   => false,
    // 			    'data' =>array('id' => $brand_result),
    // 			],200);
    //        	} else{
    //        		return response()->json([
    // 			    'error'   => true,
    // 			    'message' =>"Something went Wrong",
    // 			],400);
    //       		}
    //       	} else{
    //        		return response()->json([
    // 			    'error'   => true,
    // 			    'message' =>"Something went Wrong",
    // 			],400);
    //       		}
    //       } catch(\Exception $e) {
    //       	return response()->json([
    // 		    'error'   => true,
    // 		    'message' => $e->getMessage(),
    // 		],500);
    //       } catch(\QueryException $e) {
    //       	return response()->json([
    // 		    'error'   => true,
    // 		    'message' => $e->getMessage(),
    // 		],500);
    //       }
    //   }
    public function getOffer() {
        $data = array();
        if (Request::has('status')) {
            $data['offer_status_id'] = Request::get('status');
        }
        if (Request::has('bid')) {
            $data['bid'] = Request::get('bid');
        }
        if (Request::has('brand_category')) {
            $data['brand_category'] = Request::get('brand_category');
        }
        if (Request::has('coupentype')) {
            $data['coupentype'] = Request::get('coupentype');
        }
        if (Request::has('pricerange')) {
            $data['pricerange'] = Request::get('pricerange');
        }
        try {
            $result = $this->offer->getOffer($data);
            return response()->json([
                        'error' => false,
                        'data' => $result
                            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 400);
        } catch (\QueryException $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 400);
        }
    }

    //   public function getBrandById($id){
    //   	try{
    //           $result = $this->brand->getBrandById($id);
    //           if($result){
    //               return response()->json([
    //                   'error'   => false,
    //                   'data' => $result,
    //                ],200);
    //           }else{
    //                return response()->json([
    //                   'error'   => true,
    //                   'message' => "Invalid Brand",
    //               ],404);
    //           }
    //       } catch(\Exception $e) {
    //           return response()->json([
    //                   'error'   => true,
    //                   'message' => $e->getMessage(),
    //               ],400);
    //       } catch(\QueryException $e) {
    //           return response()->json([
    //                   'error'   => true,
    //                   'message' => $e->getMessage(),
    //               ],400);
    //       } 
    //   }
    public function approveOffer(DeleteBrandRequest $request) {
        $input = $request->validated();
        try {
            $data = array(
                'offer_status_id' => 2,
                'modified' => Carbon::now()->format('Y-m-d H:i:s'),
            );
            $result = $this->offer->approveOffer($data, $input['id']);
            if ($result) {
                return response()->json([
                            'error' => false,
                            'message' => "Offer Approved Successfully",
                                ], 200);
            } else {
                return response()->json([
                            'error' => true,
                            'message' => "Something went Wrong",
                                ], 500);
            }
        } catch (\Exception $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 500);
        } catch (\QueryException $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 500);
        }
    }

    public function deactiveOffer(DeleteBrandRequest $request) {

        $input = $request->validated();
        try {

            $data = array(
                'offer_status_id' => 0,
                'modified' => Carbon::now()->format('Y-m-d H:i:s'),
            );
            $result = $this->offer->deactiveOffer($data, $input['id']);
            if ($result) {
                return response()->json([
                            'error' => false,
                            'message' => "Offer Deactivated Successfully",
                                ], 200);
            } else {
                return response()->json([
                            'error' => true,
                            'message' => "Something went Wrong",
                                ], 500);
            }
        } catch (\Exception $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 500);
        } catch (\QueryException $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 500);
        }
    }

    public function updateOffer() {
        try {

            $data = array(
                'modified' => Carbon::now()->format('Y-m-d H:i:s'),
            );
            if (Request::has('bestOffer')) {
                $data['is_best_offer'] = Request::get('bestOffer');
            }
            if (Request::has('bannerOffer')) {
                $data['is_banner_offer'] = Request::get('bannerOffer');
            }
            $result = $this->offer->updateOffer($data, Request::get('id'));
            if ($result) {
                return response()->json([
                            'error' => false,
                            'message' => "Offer Updated Successfully",
                                ], 200);
            } else {
                return response()->json([
                            'error' => true,
                            'message' => "Something went Wrong",
                                ], 500);
            }
        } catch (\Exception $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 500);
        } catch (\QueryException $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 500);
        }
    }

    //   public function deleteBrand(DeleteBrandRequest $request){
    //   	try{
    //    	$input = $request->validated();
    //       	$input['status'] = 3;
    //       	$input['modified'] = Carbon::now()->format('Y-m-d H:i:s');
    //       	$result = $this->brand->deleteBrand($input);
    //       	if($result){
    //       		return response()->json([
    // 		    'error'   => false,
    // 		    'message' =>"Brand deleted Successfully",
    // 		],200);
    //       	} else{
    //       		return response()->json([
    // 		    'error'   => true,
    // 		    'message' =>"Something went Wrong",
    // 		],500);
    //       	}	
    //       } catch(\Exception $e) {
    //       	return response()->json([
    // 		    'error'   => true,
    // 		    'message' => $e->getMessage(),
    // 		],500);
    //       } catch(\QueryException $e) {
    //       	return response()->json([
    // 		    'error'   => true,
    // 		    'message' => $e->getMessage(),
    // 		],500);
    //       }
    //   }
    //   
    //  API
    public function brandOffers() {
        try {
            $input = Request::all();
            /* $validator = Validator::make($input, [
              //'latitude' => 'required',
              //'longitude' => 'required',
              ], [
              'latitude.required' => 'Location not available',
              "longitude.required" => "Location not available",
              ]
              );

              if ($validator->fails()) {
              return response()->json([
              'error' => true,
              'message' => $validator->errors()->first()
              ]);
              } */

            $path = asset("/uploads/brand") . "/";

            /* $distance = DB::raw("6371 * acos( cos( radians({$input['latitude']}) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians({$input['longitude']}) ) + sin( radians({$input['latitude']}) ) * sin( radians( latitude ) ) ) AS distance");
              $address = DB::table("brand_address")
              ->select("brand_id", "latitude", "longitude", $distance)
              ->having("distance", '<', 25)
              ->get();

              $brand_arr = [];
              $mapping = [];
              $mapping_location = [];
              foreach ($address as $a) {
              array_push($brand_arr, $a->brand_id);
              $mapping[$a->brand_id] = $a->distance;
              $mapping_location[$a->brand_id] = [
              "latitude" => $a->latitude,
              "longitude" => $a->longitude
              ];
              } */



            $model = Offer::join("brand", "brand.id", "=", "offer.brand_id")
                    //->join("coupen", "coupen.offer_id", "=", "offer.id")
                    ->select("brand.name", "brand.id as brand_id", DB::raw("(SELECT COUNT(offer.id) FROM offer WHERE offer.brand_id = brand.id) as offer_count, CONCAT('{$path}',brand.logo) AS brand_image"))
                    //->whereIn("brand.id", $brand_arr)
                    ->groupBy("offer.brand_id");

            if (isset($input['category_id']) && !empty($input['category_id'])) {
                $model->where("brand.brand_category", $input['category_id']);
            }

            $data = $model->get();
            /* foreach ($data as $d) {
              $d->distance = $mapping[$d->brand_id];
              $d->location = $mapping_location[$d->brand_id];
              } */
            return response()->json([
                        'error' => false,
                        'data' => $data,
            ]);
        } catch (\Exception $e) {
            return response()->json([
                        'error' => true,
                        'message' => "Error while fetching offers",
            ]);
        }
    }

    //  API
    public function offerByBrandId($brand_id) {
        try {
            $path = asset("/uploads/offers") . "/";

            $model = Offer::select("offer.title as offer_name", "offer.description", "offer.id as offer_id", "offer.notes", "offer.valid_to", "offer.offer_type", "offer.value", DB::raw("CONCAT('{$path}',offer.image) AS offer_image"), "offer.brand_address_id")
                    ->where("offer.brand_id", $brand_id)
                    //->where("coupen.is_used", 0)
                    ->groupBy("offer.id")
                    ->get();
            
            foreach ($model as $d) {
                $address = BrandAddress::join("city", "city.id", "=", "brand_address.city")
                        ->join("state", "state.id", "=", "brand_address.state")
                        ->join("country", "country.id", "=", "brand_address.country")
                        ->whereIn("brand_address.id", explode(",", $d->brand_address_id))
                        ->select("brand_address.address", "brand_address.pincode", "city.name as city_name", "state.name as state_name", "country.name as country_name", "brand_address.latitude", "brand_address.longitude")
                        ->get();
                $d->address = $address;
            }
            return response()->json([
                        'error' => false,
                        'data' => $model,
            ]);
        } catch (\Exception $e) {
            return response()->json([
                        'error' => true,
                        'message' => "Error while fetching brand offers",
            ]);
        }
    }

    public function offerAddress($offer_id) {
        try {
            $offer = Offer::where("id", $offer_id)
                    ->select("brand_address_id")
                    ->first();

            $address_id = explode(",", $offer->brand_address_id);
            $address = BrandAddress::join("city", "city.id", "=", "brand_address.city")
                    ->join("state", "state.id", "=", "brand_address.state")
                    ->join("country", "country.id", "=", "brand_address.country")
                    ->whereIn("brand_address.id", $address_id)
                    ->select("brand_address.address", "brand_address.pincode", "city.name as city_name", "state.name as state_name", "country.name as country_name", "brand_address.latitude", "brand_address.longitude")
                    ->get();

            return response()->json([
                        'error' => false,
                        'data' => $address,
            ]);
        } catch (\Exception $e) {
            return response()->json([
                        'error' => true,
                        'message' => "Error while fetching offer location",
            ]);
        }
    }

    //  API
    public function brandBestOffers() {
        try {
            $brand_path = asset("/uploads/brand") . "/";
            $offers_path = asset("/uploads/offers") . "/";

            $model = Offer::join("brand", "brand.id", "=", "offer.brand_id")
                    // ->join("coupen", "coupen.offer_id", "=", "offer.id")
                    ->select("brand.name", "brand.id as brand_id", "offer.title as offer_name", "offer.description", "offer.id as offer_id", "offer.notes", "offer.valid_to", "offer.offer_type", "offer.value", DB::raw("(SELECT COUNT(offer.id) FROM offer WHERE offer.brand_id = brand.id) as offer_count, CONCAT('{$brand_path}',brand.logo) AS brand_image, CONCAT('{$offers_path}',offer.image) AS offer_image"), "offer.brand_address_id")
                    ->where("offer.is_best_offer", 1)
                    //->where("coupen.is_used", 0)
                    ->groupBy("offer.id");

            $data = $model->get();
            foreach ($data as $d) {
                $address = BrandAddress::join("city", "city.id", "=", "brand_address.city")
                        ->join("state", "state.id", "=", "brand_address.state")
                        ->join("country", "country.id", "=", "brand_address.country")
                        ->whereIn("brand_address.id", explode(",", $d->brand_address_id))
                        ->select("brand_address.address", "brand_address.pincode", "city.name as city_name", "state.name as state_name", "country.name as country_name", "brand_address.latitude", "brand_address.longitude")
                        ->get();
                $d->address = $address;
            }
            if (count($data) == 0) {
                return response()->json([
                            'error' => true,
                            'message' => "No best offers available",
                ]);
            }
            return response()->json([
                        'error' => false,
                        'data' => $data,
            ]);
        } catch (\Exception $e) {
            return response()->json([
                        'error' => true,
                        'message' => "Error while fetching best offers",
            ]);
        }
    }

    public function getBannerImages() {
        try {
            $offers_path = asset("/uploads/offers") . "/";

            $model = Offer::join("brand", "brand.id", "=", "offer.brand_id")
                    //->join("coupen", "coupen.offer_id", "=", "offer.id")
                    ->select("brand.name", "brand.id as brand_id", "offer.title as offer_name", "offer.description", "offer.id as offer_id", "offer.notes", "offer.valid_to", "offer.offer_type", "offer.value", DB::raw("(SELECT COUNT(offer.id) FROM offer WHERE offer.brand_id = brand.id) as offer_count, CONCAT('{$offers_path}',offer.image) AS offer_image"), "offer.brand_address_id")
                    ->where("offer.is_banner_offer", 1)
                    //->where("coupen.is_used", 0)
                    ->groupBy("offer.id");

            $data = $model->get();
            foreach ($data as $d) {
                $address = BrandAddress::join("city", "city.id", "=", "brand_address.city")
                        ->join("state", "state.id", "=", "brand_address.state")
                        ->join("country", "country.id", "=", "brand_address.country")
                        ->whereIn("brand_address.id", explode(",", $d->brand_address_id))
                        ->select("brand_address.address", "brand_address.pincode", "city.name as city_name", "state.name as state_name", "country.name as country_name", "brand_address.latitude", "brand_address.longitude")
                        ->get();
                $d->address = $address;
            }
            return response()->json([
                        'error' => false,
                        'data' => $data,
            ]);
        } catch (\Exception $e) {
            return response()->json([
                        'error' => true,
                        'message' => "Error while fetching offers banners",
            ]);
        }
    }

}
