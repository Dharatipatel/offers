<?php

namespace App\Http\Controllers\Api\brand;

use App\Http\Controllers\Controller;
use App\Http\Models\Category;
use DB;

class CategoryController extends Controller {

    public function getAllCategories() {
        try {
            $model = Category::join('brand', 'brand.brand_category', '=', 'category.id')
                    ->join("offer", "offer.brand_id", "=", "brand.id")
                    //->join("coupen", "coupen.offer_id", "=", "offer.id")
                    ->select("category.id as category_id", "category.name as category_name", "category.description as category_description", DB::raw("(SELECT COUNT(offer.id) FROM offer WHERE offer.brand_id = brand.id) as offer_count"))
                    ->orderBy("category.name", 'asc')
                    ->groupBy("category.id")
                    ->get();

            if (count($model) > 0) {
                return response()->json([
                            'error' => false,
                            'data' => $model,
                ]);
            }

            return response()->json([
                        'error' => true,
                        'message' => "No categories found",
            ]);
        } catch (\Exception $e) {
            return response()->json([
                        'error' => true,
                        'message' => "Error while fetching categories",
            ]);
        }
    }

}
