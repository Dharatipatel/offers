<?php

namespace App\Http\Controllers\Api\brand;

use App\Http\Controllers\Controller;
use Request;
use Validator;
use Carbon\Carbon;
use App\Http\Models\Offer;
use App\Http\Models\Order;
use App\Http\Requests\GetCoupenRequest;
use App\Http\Requests\RedeemCoupenRequest;
use App\Http\Requests\UserCouponRequest;
use App\Http\Models\Coupon;
use Session;
use App\Http\Models\User;
use DB;
use App\Custom\Common;

class CouponController extends Controller {

    public function __construct() {
        $this->offer = new Offer;
        $this->order = new Order;
    }

    public function getCoupon(GetCoupenRequest $request) {
        $input = $request->validated();

        $data = array(
            'coupen' => $input['txtCoupenCode'],
            'email_phone' => $input['txtEmailPhone'],
        );

        try {
            $result = $this->offer->getCoupen($data);
            return response()->json([
                        'error' => false,
                        'data' => $result
                            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 400);
        } catch (\QueryException $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 400);
        }
    }

    public function redeemCoupon(RedeemCoupenRequest $request) {
        $input = $request->validated();

        $data = array(
            'coupen' => $input['txtRedeemCoupenCode'],
            'bill_amount' => $input['txtBillAmount'],
            'member' => $input['txtMember'],
            'reward_id' => $input['reward_id'],
            'redeem_date' => Carbon::now()->format('Y-m-d'),
        );

        try {
            $result = $this->order->redeemCoupen($data);
            return response()->json([
                        'error' => false,
                        'data' => $result
                            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 400);
        } catch (\QueryException $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 400);
        }
    }

    public function getCouponByOfferId($offer_id) {
        try {
            $model = Coupon::select("id", "code")
                    ->where("is_used", 0)
                    ->whereNull("user_id")
                    ->where("offer_id", $offer_id)
                    ->first();

            $user = User::where("token", Session::get("login_token"))->first();

            $model->user_id = $user->id;
            $model->save();

            return response()->json([
                        'error' => false,
                        'data' => $model,
            ]);
        } catch (\Exception $e) {
            return response()->json([
                        'error' => true,
                        'message' => "Error while fetching Coupons",
            ]);
        }
    }

    public function getUserCoupons(UserCouponRequest $request) {
        try {
            $user = User::where("token", $request->token)->first();
            if ($user === null) {
                return response()->json([
                            'error' => true,
                            'message' => "Invalid token or user not found"
                ]);
            }
            $model = Coupon::join("offer", "offer.id", "=", "coupen.offer_id")
                    ->join("brand", "brand.id", "=", "offer.brand_id")
                    ->select("brand.name as brand_name", "coupen.code as coupon_code", "coupen.expiry_date", "offer.id as offer_id")
                    ->where("coupen.user_id", $user->id)
                    ->orderBy("coupen.expiry_date", "ASC")
                    ->get();

            foreach ($model as $m) {
                $m->expiry_date = Carbon::parse($m->expiry_date)->format("d M Y");
            }

            return response()->json([
                        'error' => false,
                        'data' => $model,
            ]);
        } catch (\Exception $e) {
            return response()->json([
                        'error' => true,
                        'message' => "Error while fetching Coupon details",
            ]);
        }
    }

    public function assignCouponCode() {
        try {
            $input = Request::all();
            $validator = Validator::make($input, [
                        'token' => 'required',
                        'offer_id' => 'required',
                            ], [
                        'token.required' => 'Request token is required',
                        "offer_id.required" => "Offer not available",
                            ]
            );

            if ($validator->fails()) {
                return response()->json([
                            'error' => true,
                            'message' => $validator->errors()->first()
                ]);
            }

            $offer = Offer::find($input['offer_id']);
            if ($offer == null) {
                return response()->json([
                            'error' => true,
                            'message' => "Offer not available"
                ]);
            }

            $user = User::where("token", $input['token'])->first();
            if ($user == null) {
                return response()->json([
                            'error' => true,
                            'message' => "Invalid Request Token"
                ]);
            }

            $coupon = Coupon::where("offer_id", $input['offer_id'])->where("user_id", $user->id)->first();

            if ($coupon == null) {

                $coupon_count = Coupon::where("offer_id", $offer->id)->count();
                if ($coupon_count >= $offer->count) {
                    return response()->json([
                                'error' => true,
                                'message' => "Coupon code not available"
                    ]);
                }

                $coupon = new Coupon();
                $coupon->offer_id = $offer->id;
                $coupon->name = $offer->title;
                $coupon->description = $offer->description;
                $coupon->value = $offer->value;
                $coupon->p_value = $offer->price;
                $coupon->expiry_date = $offer->valid_to;
                $coupon->type = $offer->offer_type;
                $coupon->is_used = 0;
                $coupon->code = Common::generateCoupenCode(6);
                $coupon->category_id = 0;
                $coupon->user_id = $user->id;
                $coupon->created = Carbon::now();
                $coupon->save();
            }

            return response()->json([
                        'error' => false,
                        'data' => $coupon
            ]);
        } catch (\Exception $e) {
            return response()->json([
                        'error' => true,
                        'message' => "Error while assigning Coupon Code",
            ]);
        }
    }

    public function reclaimed() {
        try {
            $input = Request::all();
            $validator = Validator::make($input, [
                        'coupon_code' => 'required',
                            ], [
                        "coupon_code.required" => "Please enter Coupon Code",
                            ]
            );

            if ($validator->fails()) {
                return response()->json([
                            'error' => true,
                            'message' => $validator->errors()->first()
                ]);
            }

            $coupon = Coupon::join("users", "users.id", "=", "coupen.user_id")
                    ->select("coupen.value as discount", DB::raw("concat(users.first_name, ' ', users.last_name) as user_name"))
                    ->where("coupen.code", $input['coupon_code'])
                    ->first();

            if ($coupon == null) {
                return response()->json([
                            'error' => true,
                            'message' => "Coupon Code not assigned to user"
                ]);
            }

            return response()->json([
                        'error' => false,
                        'data' => $coupon
            ]);
        } catch (\Exception $e) {
            return response()->json([
                        'error' => true,
                        'message' => "Error while fetching Coupon Details",
            ]);
        }
    }

    public function apply() {
        try {
            $input = Request::all();
            $validator = Validator::make($input, [
                        'coupon_code' => 'required',
                        'total_amount' => 'required|numeric',
                            ], [
                        "coupon_code.required" => "Please enter Coupon Code",
                        "total_amount.required" => "Please enter Total Amount",
                        "total_amount.numeric" => "Please enter digits only",
                            ]
            );

            if ($validator->fails()) {
                return response()->json([
                            'error' => true,
                            'message' => $validator->errors()->first()
                ]);
            }

            $coupon = Coupon::where("code", $input['coupon_code'])
                    ->where("is_used", 0)
                    ->first();

            if ($coupon == null) {
                return response()->json([
                            'error' => true,
                            'message' => "Coupon Code used"
                ]);
            }

            $coupon->is_used = 1;
            $coupon->save();

            return response()->json([
                        'error' => false,
                        'message' => "Coupon Code applied successfully"
            ]);
        } catch (\Exception $e) {
            return response()->json([
                        'error' => true,
                        'message' => "Error while applying Coupon Code",
            ]);
        }
    }

}
