<?php

namespace App\Http\Controllers\Api\brand;

use App\Http\Controllers\Controller;
use App\Http\Models\Brand;

// use App\Http\Requests\UpdateOfferRequest;

class BrandController extends Controller {

    public function __construct() {
        
    }

    public function getAllBrands() {
        try {
            $model = Brand::get();
            return response()->json([
                        'error' => false,
                        'data' => $model,
            ]);
        } catch (\Exception $e) {
            return response()->json([
                        'error' => true,
                        'message' => "Error while fetching Brands",
            ]);
        }
    }

}
