<?php
namespace App\Http\Controllers\Api\corporate;

use App\Http\Controllers\Controller;
use Request;
use Validator;
use Carbon\Carbon;
use App\Http\Models\Reward;
use App\Http\Models\Order;
use App\Http\Requests\StoreRewardRequest;

class RewardController  extends Controller
{
	public function __construct(){
		  $this->reward = new Reward;
      $this->order = new Order;
    }

    public function addReward(StoreRewardRequest $request){
    	$input = $request->validated();	
    	try{
        
    		  $reward_data= array(
          		'eid' => implode(',' ,array_keys($input['employee'])),
          		'category_id' => $input['category_id'],
              'offer_id' => $input['offer_id'],
              //'coupen' => $this->generateCoupenCode(6),
          		'created'  => Carbon::now()->format('Y-m-d H:i:s'),
          		'modified'  => Carbon::now()->format('Y-m-d H:i:s'),
        	);
    		  $reward_result = $this->reward->addReward($reward_data);
    		  if($reward_result){
              $data=array();
              $i=0;
              foreach($input['employee'] as $key => $value){
                  $data[$i]['employee_id'] = $key;
                  $data[$i]['coupen']  = $this->generateCoupenCode(6);
                  $data[$i]['issue_date']  = $value;
                  $data[$i]['reward_id'] = $reward_result;
                  $data[$i]['redeem'] = 0;
                  $data[$i]['member'] = 0;
                  $data[$i]['bill_amount'] = 0;
                  $data[$i]['coupon_value'] = $input['coupon_value'];
                  $data[$i]['coupon_price'] = $input['coupon_price'];
                  $i++;
              } 
              $order = $this->order->addOrders($data);
              if( $order){
                return response()->json([
                    'error'   => false,
                    'data' =>array('id' => $reward_result),
                ],201);
              } else{
                return response()->json([
                  'error'   => true,
                  'message' =>"Something went Wrong",
                ],400);
              }
        	} else{
          		return response()->json([
      				    'error'   => true,
      				    'message' =>"Something went Wrong",
  				    ],400);
    		  }
      } catch(\Exception $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
      } catch(\QueryException $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
      }

    }
    public function getReward(){
        $data = array();
		    if(Request::has('status')){
			     $data['status'] = Request::get('status');
		    }
        if(Request::has('cid')){
            $data['cid'] = Request::get('cid');
        }
        if(Request::has('type')){
            $data['type'] = Request::get('type');
        }
       
    	try{
			$result = $this->reward->getReward($data);
			return response()->json([
			    'error'   => false,
			    'data' => $result
			],200);
        } catch(\Exception $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],400);
        } catch(\QueryException $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],400);
        }
    }
    public function getRewardStatistics(){
        $data = array();
        if(Request::has('redeem')){
            $data['redeem'] = Request::get('redeem');
        }
        if(Request::has('cid')){
            $data['cid'] = Request::get('cid');
        }
      try{
          $result = $this->reward->getRewardUsed($data);
          return response()->json([
              'error'   => false,
              'data' => $result
          ],200);
            } catch(\Exception $e) {
              return response()->json([
                'error'   => true,
                'message' => $e->getMessage(),
            ],400);
            } catch(\QueryException $e) {
              return response()->json([
                'error'   => true,
                'message' => $e->getMessage(),
            ],400);
            }
    }
    private function generateCoupenCode($digit){
        $random_number=''; // set up a blank st*ring
        $count=0;
        while ( $count < $digit ) {
            $random_digit = mt_rand(0, 9);
            
            $random_number .= $random_digit;
            $count++;
        }
        return $random_number;
    }
    public function getOrderhistory(){
        $data = array();
        if(Request::has('status')){
           $data['status'] = Request::get('status');
        }
        if(Request::has('cid')){
            $data['cid'] = Request::get('cid');
        }       
        try{
            $result = $this->reward->getReward($data);
            return response()->json([
                'error'   => false,
                'data' => $result
            ],200);
        } catch(\Exception $e) {
            return response()->json([
                'error'   => true,
                'message' => $e->getMessage(),
            ],400);
        } catch(\QueryException $e) {
            return response()->json([
                'error'   => true,
                'message' => $e->getMessage(),
            ],400);
        }
    }
    public function getOrderhistoryStatistic(){
        $data = array();
        if(Request::has('cid')){
            $data['cid'] = Request::get('cid');
        }       
        try{
            $result = array(
              'total_reward' => $this->reward->getRewardUsed($data),
              'total_saved'  => 0,
              'total_invested' => $this->reward->getInvestedAmount($data),
            );
            return response()->json([
                'error'   => false,
                'data' => $result
            ],200);
        } catch(\Exception $e) {
            return response()->json([
                'error'   => true,
                'message' => $e->getMessage(),
            ],400);
        } catch(\QueryException $e) {
            return response()->json([
                'error'   => true,
                'message' => $e->getMessage(),
            ],400);
        }
    }
    public function getBrandOrderHistory(){
        $data = array();
        if(Request::has('redeem')){
           $data['redeem'] = Request::get('redeem');
        }
               
        try{
            $result = $this->reward->getBrandOrderHistory($data);
            return response()->json([
                'error'   => false,
                'data' => $result
            ],200);
        } catch(\Exception $e) {
            return response()->json([
                'error'   => true,
                'message' => $e->getMessage(),
            ],400);
        } catch(\QueryException $e) {
            return response()->json([
                'error'   => true,
                'message' => $e->getMessage(),
            ],400);
        }
    }
  
}