<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Request;
use Validator;
use Carbon\Carbon;
use App\Http\Models\User;
use App\Http\Models\Brand;
use App\Http\Models\Corporate;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\UpdateAdminRequest;
use Session;
use App\Custom\Common;
use Auth;
use App\Http\Models\Banner;
use App\Http\Models\Requested;
use DB;

class UserController extends Controller {

    public function __construct() {
        $this->user = new User;
        $this->brand = new Brand;
        $this->corporate = new Corporate;
    }

    public function login() {
        $input = Request::all();
        $validator = Validator::make($input, [
                    'email' => 'required|email',
                    'password' => 'required',
                    'role_id' => 'required',
                        ], [
                    'email.required' => 'Please enter email',
                    'email.email' => 'Please enter a valid email',
                    "password.required" => "Please enter password",
                    "role_id.required" => "User type is required",
                        ]
        );
        try {
            if ($validator->fails()) {
                return response()->json([
                            'error' => true,
                            'message' => $validator->errors()->first()
                ]);
            } else {

                $data = array(
                    'email' => $input['email'],
                    'password' => $input['password'],
                    'role_id' => $input['role_id'],
                );
                $result = $this->user->login($data);
                if ($result) {
                    if ($result->status == 0) {
                        return response()->json([
                                    'error' => true,
                                    'message' => "User is inactive",
                                        ], 400);
                    } else if ($result->status == 1) {
                        $token = $this->createToken();
                        $updated_token = $this->user->updateToken(array('id' => $result->id, 'token' => $token, 'modified' => Carbon::now()->format('Y-m-d H:i:s')));
                        Session::put("login_token", $token);
                        if ($result->role_id == 2) {
                            $user_detail = $this->user->getUserById($result->id);
                        } else if ($result->role_id == 3) {
                            $user_detail = $this->user->getCorporateById($result->id);
                        } else if ($result->role_id == 4) {
                            $user_detail = $this->user->getUser($result->id);
                        } else if ($result->role_id == 5) {
                            $user_detail = $this->user->getMerchant($result->id);
                        } else {
                            $user_detail = array();
                        }
                        if ($updated_token) {
                            return response()->json([
                                        'data' => array('id' => $result->id, 'token' => $token, 'detail' => $user_detail),
                                            ], 200);
                        } else {
                            return response()->json([
                                        'error' => true,
                                        'message' => "Error in token update",
                                            ], 400);
                        }
                    }
                } else {
                    return response()->json([
                                'error' => true,
                                'message' => "Invalid email or password",
                                    ], 404);
                }
            }
        } catch (\Exception $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
            ]);
        } catch (\QueryException $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
            ]);
        }
    }

    public function register() {
        $input = Request::all();
        $validator = Validator::make($input, [
                    'first_name' => 'required',
                    'last_name' => 'required',
                    'email' => 'required|email|unique:users,email',
                    'contact' => 'required|unique:users,contact|numeric|digits:10',
                    'password' => 'required',
                    'role_id' => 'required',
                        ], [
                    'first_name.required' => 'Please enter first name',
                    'last_name.required' => 'Please enter last name',
                    'email.required' => 'Please enter email',
                    'email.email' => 'Please enter a valid email',
                    'email.unique' => 'This email already in use',
                    'contact.required' => 'Please enter mobile number',
                    'contact.unique' => 'This mobile number already in use',
                    'contact.numeric' => 'Please enter 10 digit mobile number',
                    'contact.digits' => 'Please enter 10 digit mobile number',
                    'password.required' => 'Please enter password',
                    "role_id.required" => "User type is required",
        ]);
        try {
            if ($validator->fails()) {
                return response()->json([
                            'error' => true,
                            'message' => $validator->errors()->first()
                ]);
            } else {
                $token = $this->createToken();
                $data = $input;
                $data["token"] = $token;
                $data["status"] = 1;
                $data["password"] = md5($input["password"]);
                $data["created"] = Carbon::now()->format('Y-m-d H:i:s');
                $data["modified"] = Carbon::now()->format('Y-m-d H:i:s');

                $result = $this->user->register($data);
                if ($result) {
                    return response()->json([
                                'error' => false,
                                'data' => [],
                    ]);
                } else {
                    return response()->json([
                                'error' => true,
                                'message' => "Registration failed",
                    ]);
                }
            }
        } catch (\Exception $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
            ]);
        } catch (\QueryException $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
            ]);
        }
    }

    public function changePassword(ChangePasswordRequest $request) {
        $input = $request->validated();
        try {
            $data = array(
                'id' => $input['id'],
                'password' => $input['oldpassword'],
            );
            $password = $this->user->checkPassword($data);
            if ($password) {
                if ($input['oldpassword'] == $input['newpassword']) {
                    return response()->json([
                                'error' => true,
                                'message' => "You haven’t made any changes to your password",
                                    ], 200);
                } else {
                    $update_data = array(
                        'password' => $input['newpassword'],
                        'modified' => Carbon::now()->format('Y-m-d H:i:s'),
                    );
                    $update_password = $this->user->changePassword($update_data, $input['id']);
                    if ($update_password) {
                        return response()->json([
                                    'error' => true,
                                    'message' => "Password updated Successfully",
                                        ], 200);
                    } else {
                        return response()->json([
                                    'error' => true,
                                    'message' => "Something Went Wrong",
                                        ], 400);
                    }
                }
            } else {
                return response()->json([
                            'error' => true,
                            'message' => "Old Password is Wrong",
                                ], 404);
            }
        } catch (\Exception $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 400);
        } catch (\QueryException $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 400);
        }
    }

    private function createToken() {
        return Common::getToken(32);
    }

    public function viewAdminProfile() {
        $data = array();
        if (Request::has('id')) {
            $data['id'] = Request::get('id');
        }
        try {
            $result = $this->user->getAdminDetail($data);
            return response()->json([
                        'error' => true,
                        'data' => $result,
                            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 400);
        } catch (\QueryException $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 400);
        }
    }

    public function updateAdminProfile(UpdateAdminRequest $request) {
        $input = $request->validated();
        $data = array();

        $data['email'] = $input['email'];
        $data['modified'] = Carbon::now()->format('Y-m-d H:i:s');

        try {
            $result = $this->user->updateAdminDetail($data, $input['id']);
            return response()->json([
                        'error' => true,
                        'data' => $result,
                            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 400);
        } catch (\QueryException $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 400);
        }
    }

    public function updateUserProfile() {
        $input = Request::all();
        $validator = Validator::make($input, [
                    'id' => 'required',
                    'token' => 'required',
                    'first_name' => 'required',
                    'last_name' => 'required',
                    'contact' => 'required|numeric|digits:10|unique:users,contact,' . Request::get("id"),
                    'email' => 'required|email|unique:users,email,' . Request::get("id"),
                        ], [
                    'first_name.required' => 'Please enter first name',
                    'last_name.required' => 'Please enter last name',
                    'email.required' => 'Please enter email',
                    'email.email' => 'Please enter a valid email',
                    'email.unique' => 'This email already in use',
                    'contact.required' => 'Please enter mobile number',
                    'contact.unique' => 'This mobile number already in use',
                    'contact.numeric' => 'Please enter 10 digit mobile number',
                    'contact.digits' => 'Please enter 10 digit mobile number',
                    'contact.required' => 'Please enter mobile number',
                    'id.required' => 'User not available',
                    'token.required' => 'Token not available',
        ]);

        try {
            if ($validator->fails()) {
                return response()->json([
                            'error' => true,
                            'message' => $validator->errors()->first()
                ]);
            } else {

                $data = [
                    "first_name" => Request::get("first_name"),
                    "last_name" => Request::get("last_name"),
                    "contact" => Request::get("contact"),
                    "email" => Request::get("email"),
                ];
                $data["modified"] = Carbon::now()->format('Y-m-d H:i:s');
                if (!empty(Request::get("password"))) {
                    $data["password"] = md5(Request::get("password"));
                }


                $result = $this->user->updateUserProfile($data, ["id" => Request::get("id"), "token" => Request::get("token")]);
                if ($result) {
                    return response()->json([
                                'error' => false,
                                'data' => [],
                                'message' => "Profile updated successfully",
                    ]);
                } else {
                    return response()->json([
                                'error' => true,
                                'message' => "User not available",
                    ]);
                }
            }
        } catch (\Exception $e) {
            return response()->json([
                        'error' => true,
                        'message' => "User not found",
            ]);
        } catch (\QueryException $e) {
            return response()->json([
                        'error' => true,
                        'message' => "Error in update profile",
            ]);
        }
    }

    public function getUserProfile() {
        $input = Request::all();
        $validator = Validator::make($input, [
                    'id' => 'required',
                    'token' => 'required',
                        ], [
                    'id.required' => 'ID is required',
                    "token.required" => "Token is required",
                        ]
        );

        try {
            $model = $this->user->getUserProfile($input);
            if ($model != null) {
                return response()->json([
                            'error' => false,
                            'data' => $model,
                ]);
            } else {
                return response()->json([
                            'error' => true,
                            'message' => "User details not found",
                ]);
            }
        } catch (\Exception $e) {
            return response()->json([
                        'error' => true,
                        'message' => "User not found",
            ]);
        } catch (\QueryException $e) {
            return response()->json([
                        'error' => true,
                        'message' => "Error in get profile details",
            ]);
        }
    }

    public function requestSave(Request $request) {
        $input = Request::all();

        $validator = Validator::make($input, [
                    'name' => 'required',
                    'address' => 'required',
                    'token' => 'required',
                        ], [
                    'name.required' => 'Please enter name',
                    'address.required' => 'Please enter address',
                    'token.required' => 'Invalid request token',
                        ]
        );

        try {

            if ($validator->fails()) {
                throw new \Exception(implode("\r\n", $validator->errors()->all()));
            }

            $user = $this->user->checkToken(["token" => $input["token"]]);
            if (!$user) {
                throw new \Exception('Invalid request token');
            }

            $model = new Requested();
            $model->name = Request::get("name");
            $model->address = Request::get("address");
            $model->user_id = $user->id;
            $model->save();

            return response()->json([
                        'error' => false,
                        'message' => "Request saved successfully",
                            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 400);
        }
    }

}
