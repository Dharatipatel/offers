<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use DB;
use App\Http\Models\Offer;
use Exception;
use App\Http\Models\Brand;

class OfferController extends Controller {

    private $outputData = array();

    public function index() {
        try {
            $this->outputData['title'] = "Offers";
            $this->outputData['menu'] = "offers";

            $model = Offer::join("brand", "brand.id", "=", "offer.brand_id")
                    ->join("coupen", "coupen.offer_id", "=", "offer.id")
                    ->select("brand.id as brand_id", "brand.name as brand_name", "brand.logo as brand_logo", DB::raw("(SELECT COUNT(offer.id) FROM offer WHERE offer.brand_id = brand.id) as offers_count"))
                    ->groupBy("offer.brand_id")
                    ->get();
            $this->outputData['offers'] = $model;
            return view('user.offer.index', $this->outputData);
        } catch (Exception $e) {
            dd($e->getMessage());
        }
    }

    public function brand_offers($id) {
        try {
            $this->outputData['title'] = "Brand Offers";
            $this->outputData['menu'] = "offers";
            $model = Offer::join("coupen", "coupen.offer_id", "=", "offer.id")
                    ->select("offer.title as offer_name", "offer.image", "coupen.code", "offer.description")
                    ->where("offer.brand_id", $id)
                    ->where("coupen.is_used", 0)
                    ->groupBy("offer.id")
                    ->get();
            $this->outputData['offers'] = $model;
            return view('user.offer.brand_offers', $this->outputData);
        } catch (Exception $e) {
            dd($e->getMessage());
        }
    }

}
