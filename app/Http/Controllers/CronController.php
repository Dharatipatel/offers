<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Custom\SMSAPI;
use App\Http\Models\OrderHistory;
use DB;
use Carbon\Carbon;
use App\Custom\Email;

class CronController extends Controller {

    public function send_coupon_code() {
        try {
            $model = OrderHistory::join("employee", "employee.id", "=", "order_history.employee_id")
                    ->select(DB::raw("CONCAT(employee.first_name, ' ', employee.last_name) as user_name"), "employee.contact", "employee.email", "coupen as coupon")
                    ->whereDate("issue_date", "=", Carbon::now()->format("Y-m-d"))
                    ->get();

            foreach ($model as $m) {

                //  send mail
                $email = new Email;
                $mailData = [
                    "name" => $m->user_name,
                    "email" => $m->email,
                    "code" => $m->coupon
                ];

                $email->send("coupon_code", $mailData);

                //  send SMS
                $sms = new SMSAPI;
                $smsData["numbers"] = [$m->contact];
                $smsData["details"] = [
                    "name" => $m->user_name,
                    "code" => $m->coupon
                ];

                $result = $sms->send("coupon_code", $smsData);
            }
            dd("Coupon code sent to " . count($model) . " users. SMS: {$result}");
        } catch (\Exception $e) {
            dd($e->getMessage());
        }
    }

}
