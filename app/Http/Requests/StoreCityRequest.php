<?php

namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

class StoreCityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    
    public function rules()
    {
        return [
            "txtCityName"=> "required",
            "txtState"=> "required",
            "status"=> "required",
        ];
    }

    public function messages(){
        return [
            'txtCityName.required' =>'City Name  is required',
            'txtState.required' =>'State Name  is required',
            'status.required' =>'status  is required',
            
        ];
    }
}
