<?php

namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

class UpdateCountryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    
    public function rules()
    {
        return [
            "id"=> "required",
            "txtCountryName"=> "required",
            "status"=> "required",
        ];
    }

    public function messages(){
        return [
            'txtCountryName.required' =>'Country Name  is required',
            'id.required' =>'Id is required',
            'status.required' =>'status is required',
            
        ];
    }
}
