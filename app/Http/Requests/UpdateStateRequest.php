<?php

namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

class UpdateStateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    
    public function rules()
    {
        return [
            "txtStateName"=> "required",
            "txtCountry"=> "required",
            "status"=> "required",
            "id"=> "required",
        ];
    }

    public function messages(){
        return [
            'txtStateName.required' =>'State Name  is required',
            'txtCountry.required' =>'Country Name  is required',
            'status.required' =>'status  is required',
            'id.required' =>'id  is required',
            
        ];
    }
}
