<?php

namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

class StoreCorporateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    
    public function rules()
    {
        return [
            "name"=> "required",
            "company_size"=> "required",
            "description"=> "required",
            "email"=> "bail|required|unique:users",
            "contact"=> "bail|required|size:10|unique:users",
            "password"=> "required",
            "category"=> "required",
            "person_name"=> "required",
            "website_url"=> "required",
            "facebook_page"=> "required",
            "address"=> "required",
            "linkedin_page"=> "required",
            "status"=> "required"
        ];
    }

    public function messages(){
        return [
            'name.required' =>'Brand Name  is required',
            'company_size.required' =>'Category Name  is required',
            'description.required' =>'Description  is required',
            'email.required' =>'Email is required',
            'email.unique' =>'Email is already exist',
            'contact.required' =>'contact is required',
            'contact.size' =>'contact should be 10 digits',
            'contact.unique' =>'contact is already exist',
            'password.required' =>'password is required',
            'category.required' =>'Category is required',
            'website_url.required' =>'website url is required',
            'person_name.required' =>'person name is required',
            'facebook_page.required' =>'facebook page is required',
            'address.required' =>'address is required',
            'linkedin_page.required' =>'Linked In Page is required',
            'status.required' =>'status is required',
        ];
    }
}
