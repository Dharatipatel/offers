<?php

namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

class UpdateWalletBrandRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return boolDeleteSMSTemplateRequest
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    
    public function rules()
    {
        return [
            "user_id" => "required",
            "txtAmount" => "required"
        ];
    }

    public function messages(){
        return [
            'user_id.required' =>"ID not available",
            "txtAmount.required"=> "Amount Should Be Availble"
        ];
    }
}
