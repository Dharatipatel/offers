<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class SliderRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        $rules = [
            "name" => "required",
            "description" => "required",
            "url" => "required",
            "file" => "required",
        ];

        return $rules;
    }

    public function messages() {
        $messages = [
            'file.required' => "Slider image is required",
            'name.required' => 'Slider Name is required',
            'url.required' => 'URL is required',
            'description.required' => 'Description is required',
        ];

        return $messages;
    }

}
