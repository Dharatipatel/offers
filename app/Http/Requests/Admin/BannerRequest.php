<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class BannerRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        $file_type = 'jpeg,jpg,png,gif,bmp,JPG,JPEG,PNG';
        $rules = [
            "file" => empty(\Request::get("id")) ? 'required|mimes:'.$file_type : 'mimes:'.$file_type,
        ];

        return $rules;
    }

    public function messages() {
        $messages = [
            'file.required' => "Banner image is required",
            'file.mimes' => "Invalid file type",
        ];

        return $messages;
    }

}
