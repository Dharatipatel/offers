@extends('brand.master')
    @section('title')
        Order History
    @endsection
@section('content')
    <div class="wraper container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-color panel-primary">
                    <div class="panel-heading"> 
                        <h3 class="panel-title">Order History</h3> 
                    </div> 
                    <div class="panel-body">            
                        <div class="tab-content" data-bind="visible:orderHistoryList">
                            <table id="" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Person Name</th>
                                        <th>Offer Name</th>
                                        <th>Coupon Value</th>
                                        <th>Coupon Code</th>
                                        <th>Redeem Date</th>         
                                        <th>Final Bill</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  <!-- ko  foreach:orderhistory --> 
                                    <tr>
                                        <th><span data-bind="text: ($index()+1)"></span></th>
                                        <td><span data-bind="text: first_name() +' '+ last_name()"></span></td>
                                        <td><span data-bind="text: brand_name()"></span></td>
                                        <td><span data-bind="text: coupen_value()"></span></td>
                                        <td><span data-bind="text: coupon()"></span></td>
                                        <td><span data-bind="text: redeem_date()"></span></td>
                                        <td><span data-bind="text: bill_amount()"></span></td>
                                    </tr>
                                  <!-- /ko-->
                                </tbody>
                            </table>
                        </div>
                        <div data-bind="visible:orderHistoryNoData" class="text-center"><h1>No Data Available</h1></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @section('script')
        <script type="text/javascript">
            var brandOrderHistoryApiPath = "{{config('constant.api.reward.brandorderhistory')}}"
        </script>
        <script type="text/javascript" src="{{config('constant.script.reward.orderhistory')}}"></script>
    @endsection
@endsection