@extends('brand.master')
@section('title')
    Dashboard
@endsection
@section('content')
<div class="wraper container-fluid">
    <div class="page-title"> 
        <h3 class="title">Dashboard</h3> 
    </div>
    <div class="row">
        <div class="col-sm-3">
            <div class="panel">
                <div class="panel-body p-t-10">
                    <div class="media-main">
                        <a class="pull-left" href="#">
                            <i class="fa fa-group text-warning fa-4x"></i>
                        </a>
                        <div class="info">
                            <h4>98</h4>
                            <p class="text-muted">Expected Visits</p>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <hr/>
                    <div class="progress">
                        <div class="progress-bar progress-bar-warning wow animated progress-animated" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
                    </div>
                </div> <!-- panel-body -->
            </div> <!-- panel -->
        </div> <!-- end col -->

        <div class="col-sm-3">
            <div class="panel">
                <div class="panel-body p-t-10">
                    <div class="media-main">
                        <a class="pull-left" href="#">
                            <i class="fa fa-ticket text-primary fa-4x"></i>
                        </a>
                        <div class="info">
                            <h4>47</h4>
                            <p class="text-muted">New Coupons</p>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <hr/>
                    <div class="progress">
                        <div class="progress-bar progress-bar-primary wow animated progress-animated" role="progressbar" aria-valuenow="47" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
                    </div>
                </div> <!-- panel-body -->
            </div> <!-- panel -->
        </div> <!-- end col -->

        <div class="col-sm-3">
            <div class="panel">
                <div class="panel-body p-t-10">
                    <div class="media-main">
                        <a class="pull-left" href="#">
                            <i class="fa fa-user text-danger fa-4x"></i>
                        </a>
                        <div class="info">
                            <h4>70</h4>
                            <p class="text-muted">New Customers</p>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <hr/>
                    <div class="progress">
                        <div class="progress-bar progress-bar-danger wow animated progress-animated" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
                    </div>
                </div> <!-- panel-body -->
            </div> <!-- panel -->
        </div> <!-- end col -->

        <div class="col-sm-3">
            <div class="panel">
                <div class="panel-body p-t-10">
                    <div class="media-main">
                        <a class="pull-left" href="#">
                            <i class="fa fa-server text-success fa-4x"></i>
                        </a>
                        <div class="info">
                            <h4>1250</h4>
                            <p class="text-muted">Total Reach</p>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <hr/>
                    <div class="progress">
                        <div class="progress-bar progress-bar-success wow animated progress-animated" role="progressbar" aria-valuenow="1250" aria-valuemin="0" aria-valuemax="10000" style="width: 60%;"></div>
                    </div>
                </div> <!-- panel-body -->
            </div> <!-- panel -->
        </div> <!-- end col -->
    </div>

    <div class="row">
        <div class="col-md-3">
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel">
                        <div class="panel-body p-t-10">
                            <div class="media-main">
                                <a class="pull-left" href="#">
                                    <i class="fa fa-server text-success fa-4x"></i>
                                </a>
                                <div class="info">
                                    <h4>1250</h4>
                                    <p class="text-muted">Total Reach</p>
                                </div>
                            </div>
                        </div> <!-- panel-body -->
                    </div> <!-- panel -->
                </div> <!-- end col -->
                <div class="col-sm-12">
                    <div class="panel">
                        <div class="panel-body p-t-10">
                            <div class="media-main">
                                <a class="pull-left" href="#">
                                    <i class="fa fa-server text-success fa-4x"></i>
                                </a>
                                <div class="info">
                                    <h4>1250</h4>
                                    <p class="text-muted">Total Reach</p>
                                </div>
                            </div>
                        </div> <!-- panel-body -->
                    </div> <!-- panel -->
                </div> <!-- end col -->
                <div class="col-sm-12">
                    <div class="panel">
                        <div class="panel-body p-t-10">
                            <div class="media-main">
                                <a class="pull-left" href="#">
                                    <i class="fa fa-server text-success fa-4x"></i>
                                </a>
                                <div class="info">
                                    <h4>1250</h4>
                                    <p class="text-muted">Total Reach</p>
                                </div>
                            </div>
                        </div> <!-- panel-body -->
                    </div> <!-- panel -->
                </div> <!-- end col -->
            </div>
        </div>
        <div class="col-md-6">

        </div>
        <div class="col-md-3">
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel">
                        <div class="panel-body p-t-10">
                            <div class="media-main">
                                <a class="pull-left" href="#">
                                    <i class="fa fa-server text-success fa-4x"></i>
                                </a>
                                <div class="info">
                                    <h4>1250</h4>
                                    <p class="text-muted">Total Reach</p>
                                </div>
                            </div>
                        </div> <!-- panel-body -->
                    </div> <!-- panel -->
                </div> <!-- end col -->
                <div class="col-sm-12">
                    <div class="panel">
                        <div class="panel-body p-t-10">
                            <div class="media-main">
                                <a class="pull-left" href="#">
                                    <i class="fa fa-server text-success fa-4x"></i>
                                </a>
                                <div class="info">
                                    <h4>1250</h4>
                                    <p class="text-muted">Total Reach</p>
                                </div>
                            </div>
                        </div> <!-- panel-body -->
                    </div> <!-- panel -->
                </div> <!-- end col -->
                <div class="col-sm-12">
                    <div class="panel">
                        <div class="panel-body p-t-10">
                            <div class="media-main">
                                <a class="pull-left" href="#">
                                    <i class="fa fa-server text-success fa-4x"></i>
                                </a>
                                <div class="info">
                                    <h4>1250</h4>
                                    <p class="text-muted">Total Reach</p>
                                </div>
                            </div>
                        </div> <!-- panel-body -->
                    </div> <!-- panel -->
                </div> <!-- end col -->
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
    <script type="text/javascript">
        $('.has-submenu').removeClass('active');
        $('#dashboardTab').addClass('active');
    </script>
@endsection
