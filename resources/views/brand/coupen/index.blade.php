@extends('brand.master')
    @section('title')
        Redeem Coupon
    @endsection
@section("content")
    <div class="wraper container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-color panel-primary">
                    <div class="panel-heading"> 
                        <h3 class="panel-title">Redeem Coupon</h3> 
                    </div> 
                    <div class="panel-body">
                        <div class="col-lg-8">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Enter Details</h3>
                                </div>
                                <div class="panel-body"> 
                                    <form class="form-inline" id="coupen_form">
                                    <div class="form-group col-lg-4">
                                        <input type="text" name="txtCoupenCode" id="txtCoupenCode" data-bind="value: coupenCode" placeholder="Enter Code" class="form-control">
                                    </div>
                                    <div class="form-group col-lg-4">
                                        <input name="txtEmailPhone" id="txtEmailPhone" type="text" data-bind="value: emailPhone" placeholder="Phone/Email" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <button type="button" data-bind="click:searchCoupenDetails" class="btn btn-primary">Search</button>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8" data-bind="visible:isCouponAvailble">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Coupon Details</h3>
                                </div>
                                <div class="panel-body">
                                    <form class="form-horizontal">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Issued By</label>
                                            <div class="col-sm-9">
                                                <input type="text" disabled="" data-bind="value: issued_by" placeholder="Company Name" class="form-control">
                                            </div>
                                        </div>
                                                    <div class="form-group">
                                            <label class="col-sm-3 control-label">Issued For</label>
                                            <div class="col-sm-9">
                                                <input type="text" disabled="" data-bind="value: issued_for" placeholder="Person Name" class="form-control">
                                            </div>
                                        </div>
                                                    <div class="form-group">
                                            <label class="col-sm-3 control-label">Issued Date</label>
                                            <div class="col-sm-9">
                                                <input type="text" disabled="" data-bind="value: issued_date" placeholder="15 Feb 2018" class="form-control">
                                            </div>
                                        </div>
                                                    <div class="form-group">
                                            <label class="col-sm-3 control-label">Expire Date</label>
                                            <div class="col-sm-9">
                                                <input type="text" disabled="" data-bind="value: expire_date" placeholder="30 Mar 2018" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Coupon Value</label>
                                            <div class="col-sm-9">
                                              <input type="text" disabled=""  data-bind="value: coupon_value" placeholder="Rs 1250" class="form-control">
                                          </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8" data-bind="visible:isCouponAvailble">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Redeem Now</h3>
                                </div>
                                <div class="panel-body">
                                    <form class="form-horizontal" id="redeem_form">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Coupon Code</label>
                                            <div class="col-sm-9">
                                                <input type="text" disabled="" placeholder="XUR32GK9" data-bind="value: coupenCode" name ="txtRedeemCoupenCode" class="form-control">
                                            </div>
                                        </div>
                                                    <div class="form-group">
                                            <label class="col-sm-3 control-label">Bill Amount</label>
                                            <div class="col-sm-9">
                                                <input type="text" name ="txtBillAmount" class="form-control"  data-bind="value: billAmount,event:{'keyup':billAmountChange}"><small class="help-block-none">Customer Needs to Pay Rs <span data-bind="text:customerPayble"></span></small>
                                          </div>
                                        </div>
                                                    <div class="form-group">
                                            <label class="col-sm-3 control-label">Total Members</label>
                                            <div class="col-sm-9">
                                                <input type="text" data-bind="value: member"  name ="txtMember" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-4 offset-sm-3">
                                                <button type="button" class="btn btn-primary" data-bind="click:redeemCoupen">Redeem</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8" data-bind="visible:isNoCouponAvailble">    
                            <h3 class="alert alert-danger">Coupon Code Or Email Id may Wrong </h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>		
    @section("script")
        <script type="text/javascript">
            var couponDetailApiPath = "{{config('constant.api.coupon.coupon')}}";
            var redeemCouponApiPath = "{{config('constant.api.coupon.redeem')}}";
            var couponWebPath = "{{route('coupon')}}";
        </script>
        <script type="text/javascript" src="{{config('brand_constant.script.coupon.coupon')}}"></script>
    @endsection
@endsection