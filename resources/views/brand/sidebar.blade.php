<!-- Aside Starts -->
<aside class="left-panel">
    <div class="logo">
        <a href="{{route('dashboard')}}" class="logo-expanded">
            <span class="nav-label">Offers</span>
        </a>
    </div>
    <nav class="navigation">
        <ul class="list-unstyled">
            <li class="has-submenu active" id="dashboardTab">
                <a href="{{route('dashboard')}}">
                    <i class="ion-home"></i> 
                    <span class="nav-label">Dashboard</span>
                </a>
            </li>
            <li class="has-submenu" id="offersTab">
                <a href="#">
                    <i class="fa fa-gift"></i>
                    <span class="nav-label">My Offers</span>
                </a>
                 <ul class="list-unstyled">
                    <li id="addOfferTab">
                        <a href="{{route('addoffer')}}">Create Offer</a>
                    </li>
                    <li id="listOfferTab">
                        <a href="{{route('brandoffer')}}">Offers Listing</a>
                    </li>
                    <li id="pastOfferTab">
                        <a href="{{route('pastoffer')}}">Past Offers</a>
                    </li>
                </ul>
            </li>
            <li class="has-submenu active" id="redeemCouponTab">
                <a href="{{route('coupon')}}">
                    <i class="fa fa-tags"></i> 
                    <span class="nav-label">Redeem Coupon</span>
                </a>
            </li>
            <li class="has-submenu active" id="orderHistoryTab">
                <a href="{{route('orderhistory')}}">
                    <i class="fa fa-history"></i>
                    <span class="nav-label">Order History</span>
                </a>
            </li>
            <li class="has-submenu" id="profileTab">
                <a href="#">
                    <i class="fa fa-user" aria-hidden="true"></i>
                    <span class="nav-label">My Profile</span>
                </a>
                 <ul class="list-unstyled">
                    <li id="viewProfileTab">
                        <a href="{{route('viewprofile')}}">View Profile</a>
                    </li>
                    <li id="editProfileTab">
                        <a href="{{route('editprofile')}}">Edit Profile</a>
                    </li>
                    <li id="changePasswordTab">
                        <a href="{{route('changepassword')}}">Change Password</a>
                    </li>
                </ul>
            </li>
        </ul>
    </nav>
</aside>
<!-- Aside Ends -->