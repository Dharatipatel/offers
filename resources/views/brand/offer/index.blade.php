@extends('brand.master')
    @section("title")
        Offers List
    @endsection   
    @section("content")
        <div class="wraper container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-color panel-primary">
                        <div class="panel-heading"> 
                            <h3 class="panel-title">My Offers</h3> 
                        </div> 
                        <div class="panel-body">
                            <div>
                                <a href="" data-bind="click:offerActiveClick">Active</a> | 
                                <a href="" data-bind="click:offerUpcomingClick">Upcoming</a> |
                                <a href="" data-bind="click:offerPendingClick">Pending</a>
                            </div>
                            <table class="table table-striped table-bordered" data-bind="visible:offerList">
                                  <thead>
                                      <tr>
                                          <th>#</th>
                                          <th>Offer Name</th>
                                          <th>Offer Value</th>
                                          <th>Valid Upto</th>
                                          <th>Total</th>
                                          <th>Issued</th>
                                          <th>Redeem</th>
                                          <th>Action</th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                      <!-- ko foreach: offers() -->
                                      <tr>
                                        <td scope="row"><span data-bind="text:$data.id"></span></td>
                                        <td><span data-bind="text: $data.name"></td>
                                        <td><span data-bind="text: $data.value_show()"></td>
                                        <td><span data-bind="text: $data.valid_to"></td>
                                        <td><span data-bind="text: $data.count"></td>
                                        <td></td>
                                        <td><a href="#"></a></td>
                                        <td><a href="#">End</a></td>
                                      </tr>
                                     <!-- /ko -->
                                  </tbody>
                            </table>
                            <div data-bind="visible:offerNoData" class="text-center">
                                <h1>No Data Available</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>           
        @section("script")
            <script type="text/javascript">
                var id = "{{ Session::get('id')}}";
                var bid = "{{ Session::get('bid')}}";
                var token = "{{ Session::get('token')}}";
                var addressListApiPath ="{{config('constant.api.country.address')}}";
                var offerListApiPath ="{{config('constant.api.offer.list')}}";
                var addOfferApiPath ="{{config('constant.api.offer.add')}}";
                var updateOfferApiPath ="{{config('constant.api.offer.update')}}";
                var deleteOfferApiPath ="{{config('constant.api.offer.delete')}}";
                var activeOfferApiPath ="{{config('constant.api.offer.active')}}";
                var deactiveOfferApiPath ="{{config('constant.api.offer.deactive')}}";
                var path ="{{URL::to('/')}}"+"/uploads/offers/";
                var type = "{{$type}}";
            </script>
            <script type="text/javascript" src="{{config('brand_constant.script.offer.offer')}}"></script>
            <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script> 
        @endsection
@endsection