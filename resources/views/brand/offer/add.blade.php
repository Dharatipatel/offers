@extends('brand.master')
    @section("title")
        Create Offer
    @endsection
    @section("css")
        <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />
    @endsection     
    @section('content')
    <div class="wraper container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-color panel-primary">
                    <div class="panel-heading"> 
                        <h3 class="panel-title">Create Offer</h3> 
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <form class="form-horizontal" id="form_Offer" enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Offer Title</label>
                                        <div class="col-md-4">
                                            <input type="text" name="txtTitle" id="txtTitle" class="form-control" placeholder="Enter Offer Title">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Offer Description</label>
                                        <div class="col-md-4">
                                            <textarea class="form-control" name="txtDescription" id="txtDescription" placeholder="Enter Offer Description"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Offer Type</label>
                                        <div class="col-sm-9" >
                                            <div>
                                                <input id="txtOfferType1" type="radio" value="1" name="txtOfferType" data-bind="checked: offerTypeValue" >
                                                <label for="txtOfferType1">Fix Value</label>
                                            </div>
                                            <div>
                                                <input id="txtOfferType2" type="radio" value="2" name="txtOfferType" data-bind="checked: offerTypeValue" >
                                                <label for="txtOfferType2">Percentage(%) Based</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Offer Value</label>
                                        <div class="col-md-3">
                                            <input type="text" name="txtValue" id="txtValue" placeholder="Enter Actual Value" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Offer Applicable On<br><small class="text-primary">Specify applicable dates</small></label>
                                        <div class="col-sm-9" >
                                            <div>
                                                <input id="txtApplicableOn1" type="radio" checked="checked" value="1" name="txtApplicableOn">
                                                <label for="txtApplicableOn1">Fix Date</label>
                                            </div>
                                            <div>
                                                <input id="txtApplicableOn2" type="radio" value="2" name="txtApplicableOn">
                                                <label for="txtApplicableOn2">Week Days (Mon-Fri)</label>
                                            </div>
                                            <div>
                                                <input id="txtApplicableOn3" type="radio" value="3" name="txtApplicableOn">
                                                <label for="txtApplicableOn3">Weekends  (Sat-Sun)</label>
                                            </div>
                                            <div>
                                                <input id="txtApplicableOn4" type="radio"  value="4" name="txtApplicableOn">
                                                <label for="txtApplicableOn4">All Days</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Publish Date</label>
                                        <div class="col-md-4">
                                            <input type="text" id="publishDate" name="publishDate" placeholder="Select Date" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group" data-bind="visible:fixDateVisibilty">
                                        <label class="col-sm-3 control-label">Select Fix Date</label>
                                        <div class="col-md-4">
                                            <input type="text" id="fixDate" name="fixDate" placeholder="Select Date" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group" data-bind="visible:validToVisibilty">
                                        <label class="col-sm-3 control-label">Offer Valid Upto</label>
                                        <div class="col-md-4">
                                            <input type="text" id="validTo" name="validTo" placeholder="Select Date" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Offer Applicable At<br><small class="text-primary">Specify applicable branches</small></label>
                                        <div class="col-sm-9">
                                            <!-- ko foreach: address -->
                                            <div>
                                                <input type="checkbox" data-bind="attr: { 'id': 'address' + $index() } ,checkedValue: value, checked: $root.choosenAddress" name="address[]" />
                                                <label data-bind="attr: { 'for': 'address' + $index() },text: textaddress"></label>
                                            </div>
                                            <!-- /ko -->
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Offer Limit/Count</label>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control" id="count" name="count" placeholder="Enter Number"><small class="help-block-none">Maximun count upto which coupon can be issue</small>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Important Notes</label>
                                        <div class="col-md-4">
                                            <textarea class="form-control" id="notes" name="notes" placeholder="Enter Important Notes related to this offer"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Offer Image</label>
                                        <div class="col-md-4">
                                            <input id="fileInput" type="file" name="fileInput" accept="image/gif, image/jpeg, image/png" class="form-control-file">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-4 col-sm-offset-4">
                                            <button type="button" data-bind="click:createOfferClick" class="btn btn-primary">Create Offer</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> 
@endsection
@section("script")
<script type="text/javascript">
    var addressListApiPath = "{{config('constant.api.country.address')}}";
    var offerListApiPath = "{{config('constant.api.offer.list')}}";
    var addOfferApiPath = "{{config('constant.api.offer.add')}}";
    var updateOfferApiPath = "{{config('constant.api.offer.update')}}";
    var deleteOfferApiPath = "{{config('constant.api.offer.delete')}}";
    var activeOfferApiPath = "{{config('constant.api.offer.active')}}";
    var deactiveOfferApiPath = "{{config('constant.api.offer.deactive')}}";
    var offerList = "{{route('brandoffer')}}";
    var path = "{{URL::to('/')}}" + "/uploads/offers/";
    var type = "{{$type}}";
</script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="{{config('brand_constant.script.offer.offer')}}"></script>
@endsection