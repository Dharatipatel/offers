@extends('brand.master')
    @section('title')
        Profile View
    @endsection
@section('content')
    <div class="wraper container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-color panel-primary">
                    <div class="panel-heading"> 
                        <h3 class="panel-title">My Profile</h3> 
                    </div> 
                    <div class="panel-body">
                        {!! Form::open(['id'=>'form_brand', 'class'=>"form-horizontal","role"=>"form"]) !!}
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Brand Name</label>
                            <div class="col-sm-4">
                                <input class="form-control" placeholder="Brand Name" name="txtBrandName" id="txtBrandName" type="text" data-bind="value:brand" disabled="disabled">
                            </div>
                            <div class="col-sm-4">
                                <img class="img-circle" data-bind="attr: { src: imagePath() + profile() }" height="100" width="100">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Select Category</label>
                            <div class="col-sm-4">
                                <select class="form-control" disabled="disabled" name="txtCategory" id="txtCategory" data-bind="
                                  value: selectedCategory,
                                  options: availbleCategory,
                                  optionsText: 'text',
                                  optionsValue: 'value',
                                  valueAllowUnset: true,
                                  optionsCaption: 'Select Category'
                                ">
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Brand Description</label>
                            <div class="col-sm-4">
                                <textarea class="form-control" data-bind="value:description" disabled="disabled" rows="5" placeholder="Brand Description" name="txtDescription" id="txtDescription" type="text" value=""></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Contact Person</label>
                            <div class="col-sm-4">
                                <input class="form-control" data-bind="value:personname" disabled="disabled" placeholder="Contact Person Name" name="txtPersonName" id="txtPersonName" type="text" value="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Email</label>
                            <div class="col-sm-4">
                                <input class="form-control"  data-bind="value:email" disabled="disabled" placeholder="Email" name="txtEmail" id="txtEmail" type="text" value="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Mobile</label>
                            <div class="col-sm-4">
                                <input class="form-control number-only" data-bind="value:contact" disabled="disabled" placeholder="Mobile" name="txtContact" id="txtContact" type="text" value="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Business Hours</label>
                            <div class="col-sm-4">
                                <textarea class="form-control" data-bind="value:business_hours" disabled="disabled" placeholder="Business Hours" name="txtBusinessHours" id="txtBusinessHours" type="text" value=""></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Website Url</label>
                            <div class="col-sm-4">
                                <input class="form-control"  data-bind="value:website_url" disabled="disabled" placeholder="Website Url" name="txtWebsiteUrl" id="txtWebsiteUrl" type="text" value="">
                            </div>
                        </div> 
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Facebook Page</label>
                            <div class="col-sm-4">
                                <input class="form-control" data-bind="value:facebook_page" disabled="disabled" placeholder="Facebook Page" name="txtFacebookPage" id="txtFacebookPage" type="text" value="">
                            </div>
                        </div>
                        <div class="form-group">
                            <div data-bind="foreach: { data: fields, as: 'field' }">
                                <div data-bind="attr: { id: 'addressDiv'+$index()},foreach: { data: items, as: 'item' }" class="form-group row">
                                    <label class="col-sm-3 control-label" data-bind="text:'Address'+($parentContext.$index()+1)"></label>
                                    <div class="col-sm-9" >
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <input class="form-control" placeholder="Address" 
                                              data-bind="value:item.address,attr: { id:'txtAddress'+$parentContext.$index() ,name: 'txtAddress'+$parentContext.$index()}" disabled="disabled" type="text" >
                                            </div>
                                            <div class="col-sm-3">
                                                <input class="form-control pincode" placeholder="Pincode" data-bind="value:item.pincode,attr: { id:'txtPincode'+$parentContext.$index() ,name: 'txtPincode'+$parentContext.$index()}" disabled="disabled" type="text" value="">
                                            </div>
                                            <div class="col-sm-3">
                                                <select class="form-control city"  disabled="disabled" data-bind="
                                                value: item.city,
                                                options: $root.Cities()[$parentContext.$index()],
                                                optionsText: 'text',
                                                optionsValue: 'value',
                                                valueAllowUnset: true,
                                                optionsCaption: 'Select City Name',
                                                attr: { id:'txtCity'+$parentContext.$index() ,name: 'txtCity'+$parentContext.$index(),'data-id':$parentContext.$index()}
                                                ">
                                                </select>
                                            </div>                                              
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>     
            </div>
        </div>
    </div>
    @section('script')
        <script type="text/javascript">
            var type = "{{ $type }}";
            var viewProfileApiPath = "{{ config('brand_constant.api.profile.view')}}";
            var getCategoriesApiPath = "{{config('constant.api.category.list')}}";
            var addressListApiPath = "{{config('constant.api.country.address')}}";
            var cityListApiPath = "{{config('constant.api.city.list')}}";
            var path ="{{URL::to('/')}}"+"/uploads/brand/";
        </script>
        <script type="text/javascript" src="{{config('brand_constant.script.profile.view')}}"></script>
    @endsection
@endsection