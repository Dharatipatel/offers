@extends('user.layouts.master')
@section("title")
{{ @$title. ' - '}}
@stop
@section("content")
<div class="wraper container-fluid">
    <div class="page-title"> 
        <h3 class="title">{{ @$title }}</h3> 
    </div>
    <div class="row">

        @foreach(@$offers as $o)
        <div class="col-lg-3 col-sm-6">
            <div class="panel">
                <div class="panel-body p-t-10">
                    <div class="media-main">
                        <a class="pull-left" href="{{route("user.offers.brand", ["id"=>$o->brand_id])}}">
                            <img class="thumb-lg img-circle bx-s" src="{{url("/")}}/uploads/brand/{{$o->brand_logo}}" alt="">
                        </a>
                        <div class="info">
                            <a href="{{route("user.offers.brand", ["id"=>$o->brand_id])}}"><h4>{{$o->offers_count}}</h4></a>
                            <p class="text-muted">{{$o->brand_name}}</p>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div> <!-- panel-body -->
            </div> <!-- panel -->
        </div> <!-- end col -->
        @endforeach
    </div>
</div>
@endsection