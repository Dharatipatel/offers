@extends('user.layouts.master')
@section("title")
{{ @$title. ' - '}}
@stop
@section("content")
<div class="wraper container-fluid">
    <div class="page-title"> 
        <h3 class="title">{{ @$title }}</h3> 
    </div>
    <div class="row">

        @foreach(@$offers as $o)
        <div class="col-lg-4 col-sm-6">
            <div class="panel">
                <div class="panel-body p-t-10">
                    <div class="media-main">
                        <a class="pull-left" href="javascript:;">
                            <img class="thumb-lg img-circle bx-s" src="{{url("/")}}/uploads/offers/{{$o->image}}" alt="">
                        </a>
                        <div class="info">
                            <h4>{{$o->offer_name}}</h4>
                            <p class="text-muted">{!! $o->description !!}</p>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <hr/>
                    <div class="text-right">
                        <button class="btn btn-xs btn-primary code_btn"  data-target="code" data-code="{{$o->code}}">Coupon Code</button>
                    </div>
                </div> <!-- panel-body -->
            </div> <!-- panel -->
        </div> <!-- end col -->
        @endforeach
    </div>
</div>
<div class="modal fade" id="coupon_code_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Offer Coupon Code</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <h1>Coupon Code: <span id="code" class="code_container">XXXXXX</span></h1>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
    $(document).ready(function () {

        $(".code_btn").on('click', function () {
            $("#coupon_code_modal").modal("hide");
            $(".code_container").html("XXXXXX");
            var $ele = $(this);
            var code = $ele.data("code");
            var target = $ele.data("target");
            $("#" + target).html(code);
            $("#coupon_code_modal").modal("show");
        });
    });
</script>
@stop