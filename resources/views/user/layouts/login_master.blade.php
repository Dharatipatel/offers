<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
       <meta name="title" content="{{env("PROJECT_NAME")}}">
        <title>@yield("title"){{env("PROJECT_NAME")}}</title>
        <script>
            var baseURL = "{{url('/').'/'}}";
            var _token = "{{csrf_token()}}";
        </script>
        <link rel="stylesheet" href="{{ config("user_constant.css.bootstrap.min")}}">
        <link rel="stylesheet" href="{{ config("user_constant.css.bootstrap.reset")}}">
        <link rel="stylesheet" href="{{ config("user_constant.css.animate")}}">
        <link rel="stylesheet" href="{{ config("user_constant.css.font-awesome")}}">
        <link rel="stylesheet" href="{{ config("user_constant.css.ionicons")}}">
        <link rel="stylesheet" href="{{ config("user_constant.css.sweet-alert")}}">
        <link rel="stylesheet" href="{{ config("user_constant.css.style")}}">
        <link rel="stylesheet" href="{{ config("user_constant.css.helper")}}">
        <link rel="stylesheet" href="{{ config("user_constant.css.dataTables")}}">
        <link rel="stylesheet" href="{{ config("user_constant.css.wysihtml5")}}">
        <link rel="stylesheet" href="{{ config("user_constant.css.dropzone")}}">
        @yield("css")
    </head>
    <body  class="login-bg">
        
        @yield("content")
        <script type="text/javascript" src="{{ config("user_constant.script.jquery")}}"></script>
        <script type="text/javascript" src="{{ config("user_constant.script.bootstrap")}}"></script>
        <script type="text/javascript" src="{{ config("user_constant.script.modernizr")}}"></script>
        <script type="text/javascript" src="{{ config("user_constant.script.pace")}}"></script>
        <script type="text/javascript" src="{{ config("user_constant.script.wow")}}"></script>
        <script type="text/javascript" src="{{ config("user_constant.script.scrollTo")}}"></script>
        <script type="text/javascript" src="{{ config("user_constant.script.nicescroll")}}"></script>
        <script type="text/javascript" src="{{ config("user_constant.script.moment")}}"></script>
        <script type="text/javascript" src="{{ config("user_constant.script.waypoints")}}"></script>
        <script type="text/javascript" src="{{ config("user_constant.script.counterup")}}"></script>
        <script type="text/javascript" src="{{ config("user_constant.script.datatables.jquery")}}"></script>
        <script type="text/javascript" src="{{ config("user_constant.script.datatables.bootstrap")}}"></script>
        <script type="text/javascript" src="{{ config("user_constant.script.validate")}}"></script>
        <script type="text/javascript" src="{{ config("user_constant.script.sweet-alert.min")}}"></script>
        <script type="text/javascript" src="{{ config("user_constant.script.sweet-alert.init")}}"></script>
        <script type="text/javascript" src="{{ config("user_constant.script.app")}}"></script>
        <script type="text/javascript" src="{{ config("user_constant.script.chat")}}"></script>
        <script type="text/javascript" src="{{ config("user_constant.script.main")}}"></script>
        <script type="text/javascript" src="{{ config("user_constant.script.todo")}}"></script>
        @yield("script")
    </body>
</html>
