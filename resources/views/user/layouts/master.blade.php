<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="title" content="{{env("PROJECT_NAME")}}">
        <title>@yield("title"){{env("PROJECT_NAME")}}</title>
        <script>
            var baseURL = "{{url('/').'/'}}";
            var _token = "{{csrf_token()}}";
        </script>
        <link rel="stylesheet" href="{{ config("user_constant.css.bootstrap.min")}}">
        <link rel="stylesheet" href="{{ config("user_constant.css.bootstrap.reset")}}">
        <link rel="stylesheet" href="{{ config("user_constant.css.animate")}}">
        <link rel="stylesheet" href="{{ config("user_constant.css.font-awesome")}}">
        <link rel="stylesheet" href="{{ config("user_constant.css.ionicons")}}">
        <link rel="stylesheet" href="{{ config("user_constant.css.sweet-alert")}}">
        <link rel="stylesheet" href="{{ config("user_constant.css.style")}}">
        <link rel="stylesheet" href="{{ config("user_constant.css.helper")}}">
        <link rel="stylesheet" href="{{ config("user_constant.css.dataTables")}}">
        <link rel="stylesheet" href="{{ config("user_constant.css.wysihtml5")}}">
        <link rel="stylesheet" href="{{ config("user_constant.css.dropzone")}}">
        @yield("css")
    </head>
    <body>
        <!-- Aside Start-->
        <aside class="left-panel">
            <!-- brand -->
            <div class="logo">
                <a href="{{url("/")}}" class="logo-expanded">
                    <span class="nav-label">{{env("PROJECT_NAME")}}</span>
                </a>
            </div>
            <!-- / brand -->
            <!-- Navbar Start -->
            <nav class="navigation">
                <ul class="list-unstyled">
                    <li class="@if(@$menu=="offers") active @endif"><a href="{{route("user.offers") }}"><i class="fa fa-tags"></i> <span class="nav-label">Offers</span></a></li>
                </ul>
            </nav>

        </aside>
        <!-- Aside Ends-->
        <!--Main Content Start -->
        <section class="content">
            <!-- Header -->
            <header class="top-head container-fluid">
                <button type="button" class="navbar-toggle pull-left">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <nav class=" navbar-default" role="navigation">
                    <!-- Right navbar -->
                    <ul class="nav navbar-nav navbar-right top-menu top-right-menu">  
                        
                        <!-- user login dropdown start-->
                        <li class="dropdown text-center">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <!--<img alt="" src="{{asset("assets")}}/velonic/img/avatar-2.jpg" class="img-circle profile-img thumb-sm">-->
                                <span class="username">{{Auth::user()->first_name}} {{Auth::user()->last_name}}</span> <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu pro-menu fadeInUp animated" tabindex="5003" style="overflow: hidden; outline: none;">
                                <li><a href="{{route("auth.user.logout")}}"><i class="fa fa-sign-out"></i> Log Out</a></li>
                            </ul>
                        </li>
                        <!-- user login dropdown end -->       
                    </ul>
                    <!-- End right navbar -->
                </nav>
            </header>
            <!-- Header Ends -->
            <!-- Page Content Start -->
            <!-- ================== -->
            @yield("content")
            <!-- Page Content Ends -->
            <!-- ================== -->
            <!-- Footer Start -->
            <footer class="footer">
                © {{date("Y")}}, <a href="{{url('/')}}">{{env("PROJECT_NAME")}}</a>, All rights reserved
            </footer>
            <!-- Footer Ends -->
        </section>
        <script type="text/javascript" src="{{ config("user_constant.script.jquery")}}"></script>
        <script type="text/javascript" src="{{ config("user_constant.script.bootstrap")}}"></script>
        <script type="text/javascript" src="{{ config("user_constant.script.modernizr")}}"></script>
        <script type="text/javascript" src="{{ config("user_constant.script.pace")}}"></script>
        <script type="text/javascript" src="{{ config("user_constant.script.wow")}}"></script>
        <script type="text/javascript" src="{{ config("user_constant.script.scrollTo")}}"></script>
        <script type="text/javascript" src="{{ config("user_constant.script.nicescroll")}}"></script>
        <script type="text/javascript" src="{{ config("user_constant.script.moment")}}"></script>
        <script type="text/javascript" src="{{ config("user_constant.script.waypoints")}}"></script>
        <script type="text/javascript" src="{{ config("user_constant.script.counterup")}}"></script>
        <script type="text/javascript" src="{{ config("user_constant.script.datatables.jquery")}}"></script>
        <script type="text/javascript" src="{{ config("user_constant.script.datatables.bootstrap")}}"></script>
        <script type="text/javascript" src="{{ config("user_constant.script.validate")}}"></script>
        <script type="text/javascript" src="{{ config("user_constant.script.sweet-alert.min")}}"></script>
        <script type="text/javascript" src="{{ config("user_constant.script.sweet-alert.init")}}"></script>
        <script type="text/javascript" src="{{ config("user_constant.script.app")}}"></script>
        <script type="text/javascript" src="{{ config("user_constant.script.chat")}}"></script>
        <script type="text/javascript" src="{{ config("user_constant.script.main")}}"></script>
        <script type="text/javascript" src="{{ config("user_constant.script.todo")}}"></script>
        @yield("script")
    </body>
</html>