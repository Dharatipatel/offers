@extends('admin.master')
@section("title")
{{ @$title. ' - '}}
@stop
@section("css")
<!-- DataTables -->
@endsection
@section("content")
<div class="wraper container-fluid">
    <div class="row">
        <div class="col-lg-12" id="divMailTemplateAdd">
            <div class="panel panel-color panel-primary">
                <div class="panel-heading"> 
                    <h3 class="panel-title"><span data-bind="text:action_title"></span>{{ $title }}</h3> 
                </div> 
                <div class="panel-body">
                  
                    {!! Form::open(["url"=>route('admin.banner.save'),"method"=>"POST", 'class'=>"form-horizontal","role"=>"form", 'id'=>'mailtemplate_form', 'files'=>true]) !!}
                    {!! Form::hidden("id", @$banner->id) !!}
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Banner</label>
                        <div class="col-sm-8">
                            <label>Image:</label>
                            <span class="btn btn-primary" id="clicker">Select Banner</span>
                            <input type="file" name="file" class="hidden" id="file" accept="image/*">
                            <br/>                                <br/>

                            <img id="image" width="400px" src="{{asset("uploads/banner/".@$banner->image)}}">
                            @if($errors->has('file'))
                            <label class="text-danger">{{ $errors->first('file') }}</label>
                            @endif
                        </div>
                    </div>


                    <div class="form-group m-b-0">
                        <div class="col-sm-offset-3 col-sm-8">
                            <button type="submit" class="btn btn-purple">Save</button>
                            <a href="{{ route('admin.banner.index') }}" class="btn btn-danger">Cancel</a>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    @endsection
    @section('script')
    <script>
$(".has-submenu").removeClass("active");
$("#bannerTab").addClass("active");
    </script>
    @stop