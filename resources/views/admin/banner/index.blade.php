@extends('admin.master')
@section("title")
{{ @$title. ' - '}}
@stop
@section("css")
<!-- DataTables -->
<link rel="stylesheet" type="text/css" href="{{config('admin_constant.css.datatable')}}" />
@endsection
@section("content")
<div class="wraper container-fluid">
    <div class="row">
        <!-- corporate Listing -->
        <div class="col-lg-12">
            <div class="panel panel-color panel-primary">
                <div class="panel-heading"> 
                    <h3 class="panel-title">{{ @$title }}</h3> 
                </div> 
                <div class="panel-body">
                    <a href="{{ route('admin.banner.add')}}" class="btn btn-primary m-b-5">Add Banner</a>
                    <table id="banner_list" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Image</th>
                                <th class="col-md-2">Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>



    </div>
    @endsection
    @section('script')

    <script type="text/javascript" src="{{config('constant.script.datatables.jquery')}}"></script>
    <script type="text/javascript" src="{{config('constant.script.datatables.bootstrap')}}"></script>
    <script type="text/javascript" src="{{config('constant.script.ko-model')}}"></script>
    <script type="text/javascript" src="{{config('constant.script.common.jquerymd5')}}"></script>
    <script type="text/javascript" src="{{config('constant.script.common.helper')}}"></script>
    <script type="text/javascript">
$('document').ready(function () {
    $('#banner_list').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{{ route("admin.banner.list") }}',
        columns: [
            {data: 'image', name: 'image', "bSearchable": false, sortable:false},
            {data: 'action', name: 'action', "bSearchable": false, sortable:false}
        ]
    });
});



$(".has-submenu").removeClass("active");
$("#bannerTab").addClass("active");
    </script>
    @endsection