@extends('admin.master')
@section("title")
{{ @$title. ' - '}}
@stop
@section("css")
    <!-- DataTables -->
    <link rel="stylesheet" type="text/css" href="{{config('admin_constant.css.datatable')}}" />
@endsection
@section("content")
<div class="wraper container-fluid">
    <div class="row">
        @include('admin.corporate.index')
        
        @include('admin.corporate.manage')
        <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h1 class="modal-title" id="exampleModalLongTitle">Delete Corporate</h1>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    Are you sure to delete Corporate ?
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" data-bind ="click:deleteConfirm" class="btn btn-primary">Yes</button>
                  </div>
                </div>
            </div>
        </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
    var id = "{{ Session::get('id')}}";
    var token = "{{ Session::get('token')}}";
    var type = "{{$type}}";
    var corporateListApiPath ="{{config('constant.api.corporate.list')}}";
    var addCorporateApiPath ="{{config('constant.api.corporate.add')}}";
    var updateCorporateApiPath ="{{config('constant.api.corporate.update')}}";
    var deleteCorporateApiPath ="{{config('constant.api.corporate.delete')}}";
    var activeCorporateApiPath ="{{config('constant.api.corporate.active')}}";
    var deactiveCorporateApiPath ="{{config('constant.api.corporate.deactive')}}";
    var getCategoriesApiPath = "{{config('constant.api.category.list')}}";
    var employeeWebList = "{{route('employee')}}";
    var corporateWebList = "{{route('corporatelist')}}";
    var path ="{{URL::to('/')}}"+"/uploads/corporate/";
    var pathEmployee ="{{URL::to('/')}}"+"/uploads/employee/";
    var employeeListApiPath = "{{config('corporate_constant.api.employee.list')}}";
    var employeeAddApi  = "{{config('corporate_constant.api.employee.add')}}";
    var employeeUpdateApi  = "{{config('corporate_constant.api.employee.update')}}";
</script>
<script type="text/javascript" src="{{config('constant.script.datatables.jquery')}}"></script>
<script type="text/javascript" src="{{config('constant.script.datatables.bootstrap')}}"></script>
<script type="text/javascript" src="{{config('constant.script.ko-model')}}"></script>
<script type="text/javascript" src="{{config('constant.script.common.jquerymd5')}}"></script>
<script type="text/javascript" src="{{config('constant.script.common.helper')}}"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="{{config('constant.script.corporate')}}"></script>
@endsection