<div class="panel-body"  data-bind="visible:employeeViewModel.employeeList">
    <a href="" class="btn btn-primary m-b-5" data-bind="click:employeeViewModel.addEmployeeClick">Add Employee</a>
    <table class="table table-striped table-bordered" >
        <thead>
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>Designation</th>
              <th>Mobile</th>
              <th>Action</th>
            </tr>
        </thead>
        <tbody>
          <!-- ko foreach:employeeViewModel.employees -->
            <tr>
                <th scope="row"><span data-bind="text:id"></span></th>
                <td><a href=""><span data-bind="text:first_name()+' '+last_name()"></span></a></td>
                <td><span data-bind="text:designation"></span></td>
                <td><span data-bind="text:contact"></span></td>
                <td>
                    <a href="#" data-bind="click:$root.employeeViewModel.editEmployeeClick">Edit</a>
                </td>
            </tr>
          <!-- /ko -->
        </tbody>
    </table>
</div>
<div class="panel-body" data-bind="visible:employeeViewModel.employeeNoData">
  <a href="" class="btn btn-primary m-b-5" data-bind="click:employeeViewModel.addEmployeeClick">Add Employee</a>
      <p>No Data Availble</p>
</div>
<div class="panel-body" data-bind="visible:employeeViewModel.addEmployeeContainer">
     <form class="form-horizontal" action="" name="form_employee" id="form_employee">
          <div class="form-group row">
              <label class="col-sm-4 form-control-label">First Name</label>
              <div class="col-sm-8">
                  <input type="text" name="txtFirstName" id="txtFirstName" placeholder="Enter First Name" class="mr-3 form-control">
              </div>
          </div>
          <div class="form-group row">
              <label class="col-sm-4 form-control-label">Last Name</label>
                <div class="col-sm-8">
                    <input type="text" name="txtLastName" id="txtLastName" placeholder="Enter Last Name" class="mr-3 form-control">
                </div>
          </div>
          <div class="form-group row">
              <label class="col-sm-4 form-control-label">Gender</label>
              <div class="col-sm-8">
                  <label class="checkbox-inline">
                      <input id="radioMale" type="radio" value="Male" name="gender" class="radio-template" data-bind="checked: employeeViewModel.selectedGender"> Male 
                  </label> 
                  <label class="checkbox-inline">
                      <input id="radioFemale" type="radio" value="Female" name="gender" class="radio-template" data-bind="checked: employeeViewModel.selectedGender"> Female
                  </label>

              </div>
          </div>
          <div class="form-group row">
              <label class="col-sm-4 form-control-label">Designation</label>
              <div class="col-sm-8">
                  <input type="text" name="txtDesignation" id="txtDesignation" placeholder="Enter Designation" class="mr-3 form-control">
              </div>
          </div>
          <div class="form-group row">
              <label class="col-sm-4 form-control-label">Email Address</label>
              <div class="col-sm-8">
                <input type="text" name="txtEmailEmployee" id="txtEmailEmployee" placeholder="Enter Email Address" class="mr-3 form-control">
              </div>
          </div>
          <div class="form-group row">
              <label class="col-sm-4 form-control-label">Mobile Number</label>
              <div class="col-sm-8">
                  <input type="text" name="txtContact" id="txtContact" placeholder="Enter Mobile Number" class="mr-3 form-control">
              </div>
          </div>
          <div class="form-group row">
              <label class="col-sm-4 form-control-label">Birth Date</label>
              <div class="col-sm-8">
                  <input type="text" name="dateBirth" id="dateBirth" placeholder="Select Birth date" class="mr-3 form-control">
              </div>
          </div>
          <div class="form-group row">
              <label class="col-sm-4 form-control-label">Date of Joining</label>
              <div class="col-sm-8">
                  <input type="text" name="dateJoin" id="dateJoin" placeholder="Select Joining Date" class="mr-3 form-control">
              </div>
          </div>
          <div class="form-group row">
              <label class="col-sm-4 form-control-label">Upload Pic</label>
              <div class="col-sm-8">
                  <input id="fileInputEmployee" type="file" accept="image/gif, image/jpeg, image/png" class="form-control-file">
                    <!-- ko if :employeeViewModel.action() =="edit" -->
                        <img data-bind="attr: { src: employeeViewModel.imagePathEmployee() + employeeViewModel.profile() }" height="200" width="200">

                    <!-- /ko -->
            </div>
          </div>
          <div class="form-group row">
            <div class="col-sm-4 offset-sm-4">
                <button type="button" data-bind="click:employeeViewModel.createEmployeeClick" class="btn btn-primary"><span data-bind="text:employeeViewModel.button_title"></span></button>
            </div>
          </div>
    </form>
</div>