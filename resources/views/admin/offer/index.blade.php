<!-- Brand Listing -->
        <div class="col-lg-12" id="divOfferList" data-bind="visible:offerListContainer">
            <div class="panel panel-color panel-primary">
                <div class="panel-heading"> 
                    <h3 class="panel-title">{{ $title }}</h3> 
                </div> 
                <div class="panel-body">
                    <!-- <a href="" class="btn btn-primary m-b-5" data-bind="click:$root.addBrandClick">Add Brand</a> -->
                    <table id="offer_tbl" class="table table-striped table-bordered" data-bind="visible:offerList">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Offer Name</th>
                                <th>Offer Value</th>
                                <th>Buy Price</th>
                                <th>Publish Date</th>
                                <th>End Date</th>
                                <th>Total</th>
                                <th>Brand Name</th>
                                <th>status</th>
                                <th>Ads Count</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <!-- ko foreach: offers() -->
                              <tr>
                              <td><span data-bind="text: $data.id"></span></th>
                              <td><span data-bind="text: $data.name"></td>
                              <td><span data-bind="text: $data.value"></td>
                              <td><span data-bind="text: $data.price"></td>
                              <td><span data-bind="text: $data.publish_date"></td>
                              <td><span data-bind="text: $data.valid_to"></td>
                              <td><span data-bind="text: $data.count"></td>
                              <td><span data-bind="text: $data.brand_name"></td>
                              <td><span data-bind="text: $data.statusText()"></td>
                              <td><span data-bind="text: $data.ads_count()"></td>
                              <td>
                                  <!-- ko if: $data.status() == 3 -->
                                  <a href="" data-bind="click:$root.approveClick" class="on-default remove-row">
                                  Approve
                                  </a>
                                  <!-- /ko -->
                                  <!-- ko if: $data.status() != 0 -->
                                  <a href="" data-bind="click:$root.deactiveClick" class="on-default remove-row">
                                    Deactive
                                  </a>
                                  <!-- /ko -->
                                  <br/>
                                  <!-- ko if: $data.is_best_offer() == 1 -->
                                    <input type='checkbox' checked ="checked" class="bestoffer" id="bestoffer" onchange = "bestOffer(this)" data-bind="attr:{'data-id':$data.id}"> Best Offer
                                  <!-- /ko -->
                                  <!-- ko if: $data.is_best_offer() == 0 -->
                                    <input type='checkbox' class="bestoffer" id="bestoffer" onchange = "bestOffer(this)" data-bind="attr:{'data-id':$data.id}"> Best Offer
                                  <!-- /ko -->
                                  <!-- ko if: $data.is_banner_offer() == 1 -->
                                    <input type='checkbox' checked ="checked" class="banneroffer" id="banneroffer" onchange = "bannerOffer(this)" data-bind="attr:{'data-id':$data.id}"> Banner Offer
                                  <!-- /ko -->
                                  <!-- ko if: $data.is_banner_offer() == 0 -->
                                    <input type='checkbox' class="banneroffer" id="banneroffer" onchange = "bannerOffer(this)" data-bind="attr:{'data-id':$data.id}"> Banner Offer
                                  <!-- /ko -->
                              </td>

                              </tr>
                            <!-- /ko -->
                            </tbody>
                    </table>
                    <div data-bind="visible:offerNoData" class="text-center"><h1>No Data Available</h1></div>
                </div>
            </div>
        </div>