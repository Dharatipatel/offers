@extends('admin.master')
@section("title")
{{ @$title. ' - '}}
@stop
@section("css")
<!-- DataTables -->
<link rel="stylesheet" type="text/css" href="{{config('admin_constant.css.datatable')}}" />
@endsection
@section("content")
<div class="wraper container-fluid">
    <div class="row">
        <!-- corporate Listing -->
        <div class="col-lg-12" id="divMailTemplateList">
            <div class="panel panel-color panel-primary">
                <div class="panel-heading"> 
                    <h3 class="panel-title">{{ @$title }}</h3> 
                </div> 
                <div class="panel-body">
                    <a href="{{ route('admin.slider.create')}}" class="btn btn-primary m-b-5">Add Slider</a>
                    <table id="slider_list" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Url</th>
                                <th>Description</th>
                                <th class="col-md-2">Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @endsection
    @section('script')
    <script type="text/javascript">
        var type = "{{@$type}}";
        var dt_url = '{{ route("admin.slider.list") }}';
    </script>
    <script type="text/javascript" src="{{config('constant.script.datatables.jquery')}}"></script>
    <script type="text/javascript" src="{{config('constant.script.datatables.bootstrap')}}"></script>
    <script type="text/javascript" src="{{config('admin_constant.script.slider')}}"></script>
    <script>
        $('#slider_list').DataTable({
            processing: true,
            serverSide: true,
            ajax: dt_url,
            columns: [
                {data: 'name', name: 'name'},
                {data: 'url', name: 'url'},
                {data: 'description', name: 'description'},
                {data: 'action', name: 'action', "bSearchable": false, searchable: false},
            ],
        });
    </script>
    @endsection