@extends('admin.master')
@section("title")
{{ @$title. ' - '}}
@stop
@section("css")
<!-- DataTables -->
@endsection
@section("content")
<div class="wraper container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-color panel-primary">
                <div class="panel-heading"> 
                    <h3 class="panel-title">{{ $title }}</h3> 
                </div> 
                <div class="panel-body">
                    @if(isset($slider->id))
                    {!! Form::open(["url"=>route('admin.slider.update',['id'=>$slider->id]),"method"=>"POST", 'class'=>"form-horizontal","role"=>"form", 'id'=>'slider_form', 'files'=>true]) !!}
                    @else
                    {!! Form::open(["url"=>route('admin.slider.save'),"method"=>"POST", 'class'=>"form-horizontal","role"=>"form", 'id'=>'slider_form', 'files'=>true]) !!}
                    @endif
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Name</label>
                        <div class="col-sm-8">
                            <input class="form-control" placeholder="Name" name="name"  type="text" value="{{@$slider->name}}">
                            @if($errors->has('name'))
                            <label class="text-danger">{{ $errors->first('name') }}</label>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">URL</label>
                        <div class="col-sm-8">
                            <input class="form-control" placeholder="URL" name="url"  type="text" value="{{@$slider->url}}">
                            @if($errors->has('url'))
                            <label class="text-danger">{{ $errors->first('url') }}</label>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Description</label>
                        <div class="col-sm-8">
                            {{ Form::textarea('description',@$slider->description,['class'=>'form-control', 'placeholder'=>'Description']) }} 
                            @if($errors->has('description'))
                            <label class="text-danger">{{ $errors->first('description') }}</label>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Image</label>
                        <div class="col-sm-8">
                            <span class="btn btn-primary" id="clicker">Change Image</span>
                            <input type="file" name="file" class="hidden" id="file" accept="image/*">
                            <br/>                                <br/>
                            <img id="image" height="200px" src="{{base_path()."/uploads/slider/".@$slider->image}}">
                            @if($errors->has('file'))
                            <label class="text-danger">{{ $errors->first('file') }}</label>
                            @endif
                        </div>
                    </div>

                    <div class="form-group m-b-0">
                        <div class="col-sm-offset-3 col-sm-8">
                            <button type="submit" class="btn btn-purple">Save</button>
                            <a href="{{ route('admin.slider.index') }}" class="btn btn-danger">Cancel</a>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    @endsection
    @section('script')
    <script type="text/javascript">
        var type = "{{@$type}}";
    </script>
    <script type="text/javascript" src="{{config('constant.script.validate.js')}}"></script>
    <script type="text/javascript" src="{{config('admin_constant.script.slider')}}"></script>
    <script>
        var rules = {

            txtSliderName: "required",
            txtDescription: "required",
            txtSliderUrl: "required",
        };
        var messages = {
            txtSliderName: "Please enter a Slider name",
            txtDescription: "Please enter a description",
            txtSliderUrl: "Please enter URL",
        };
        $("#slider_form").validate({
            rules: rules,
            messages: messages,
        });
    </script>
    @stop