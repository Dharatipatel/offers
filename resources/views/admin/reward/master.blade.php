@extends('admin.master')
@section("title")
{{ @$title. ' - '}}
@stop
@section("css")
<!-- DataTables -->
<link rel="stylesheet" type="text/css" href="{{config('admin_constant.css.datatable')}}" />
@endsection
@section("content")
<div class="wraper container-fluid">
    <div class="row">

        @include('admin.reward.index')

    </div>
    @endsection
    @section('script')
    <script type="text/javascript">
        var id = "{{ Session::get('id')}}";
        var token = "{{ Session::get('token')}}";
        var type = "{{$type}}";
        var rewardListApi = "{{config('constant.api.reward.list')}}";
    </script>
    <script type="text/javascript" src="{{config('constant.script.datatables.jquery')}}"></script>
    <script type="text/javascript" src="{{config('constant.script.datatables.bootstrap')}}"></script>
    <script type="text/javascript" src="{{config('constant.script.ko-model')}}"></script>
    <script type="text/javascript" src="{{config('constant.script.common.jquerymd5')}}"></script>
    <script type="text/javascript" src="{{config('constant.script.common.helper')}}"></script>
    <script type="text/javascript" src="{{config('constant.script.reward.list')}}"></script>
    @endsection