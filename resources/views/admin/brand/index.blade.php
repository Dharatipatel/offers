<!-- Brand Listing -->

        <div class="col-lg-12" id="divBrandList" data-bind="visible:brandListContainer">
            <div class="panel panel-color panel-primary">
                <div class="panel-heading"> 
                    <h3 class="panel-title">{{ $title }}</h3> 
                </div> 
                <div class="panel-body">
                    <!-- <a href="" class="btn btn-primary m-b-5" data-bind="click:$root.addBrandClick">Add Brand</a> -->
                    <table id="brand_tbl" class="table table-striped table-bordered" data-bind="visible:brandList">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Category</th>
                                <!-- <th>Email</th> -->
                                <th>Mobile</th>
                                <th>Person Name</th>
                                <!-- <th>Description</th> -->
                                <!-- <th>Business hours</th> -->
                                <!-- <th>website url</th> -->
                                <!-- <th>Facebook page</th> -->
                                <!-- <th>Address</th>
                                <th>Pincode</th> -->
                                <th>Offer Count</th>
                                <th>Added On</th>
                                <!-- <th>Status</th> -->
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <!-- ko foreach: brands() -->
                              <tr>
                                <td><span data-bind="text:$data.id"></span></td>
                                <td><span data-bind="text: $data.name"></span></td>
                                <td><span data-bind="text: $data.category"></span></td>
                                <!-- <td><span data-bind="text: $data.email"></span></td> -->
                                <td><span data-bind="text: $data.contact"></span></td>
                                <td><span data-bind="text: $data.contact_person_name"></span></td>
                                <!-- <td><span data-bind="text: $data.description"></span></td> -->
                                <!-- <td><span data-bind="text: $data.business_hours"></span></td> -->
                                <!-- <td><span data-bind="text: $data.website_url"></span></td> -->
                                <!-- <td><span data-bind="text: $data.facebook_page"></span></td> -->
                                <!-- <td><span data-bind="text: $data.address"></span></td>
                                <td><span data-bind="text: $data.pincode"></span></td> -->
                                <td><a href="#" data-bind="click:$root.offerListClick" ><u><span data-bind="text: $data.offer_count()"></span></u></a></td>
                                <td>
                                    <a href="#" >
                                        <span data-bind="text: $data.created"></span>
                                    </a>
                                </td>
                                <!-- <td><span data-bind="text: $data.statusText()"></span></td> -->
                                <td>
                                    <a href="#" data-bind="click:$root.editBrandClick" class="on-default edit-row"><i class="text-danger fa fa-pencil"></i></a>
                                    <a href="#" data-bind="click:$root.deleteBrandClick" class="on-default remove-row"><i class="text-success fa fa-trash-o"></i></a>
                                    <a title="Send Email" href="#" data-bind="click:$root.emailBrandClick" class="on-default send-email-row"><i class="text-info fa fa-envelope"></i></a>
                                    <!-- ko if: $data.statusText() == "Deactive" -->
                                    <a href="" data-bind="click:$root.activeClick" class="on-default remove-row">      Active
                                    </a>
                                    <!-- /ko -->
                                    <!-- ko if: $data.statusText() == "Active" -->
                                    <a href="" data-bind="click:$root.deactiveClick" class="on-default remove-row">      Deactive
                                    </a>
                                     <!-- /ko -->
                                </td>
                              </tr>
                            <!-- /ko -->
                            </tbody>
                    </table>
                    <div data-bind="visible:brandNoData" class="text-center"><h1>No Data Available</h1></div>
                </div>
            </div>
        </div>