<div data-bind="visible:offerViewModel.actionContainer">
    <a href="" class="btn btn-primary m-b-5" data-bind="click:offerViewModel.addOfferClick">Add 
      offer</a>
    <div>
          <a href="" data-bind="click:offerViewModel.offerActiveClick">Active</a> | 
          <a href="" data-bind="click:offerViewModel.offerPastClick">Past</a> |
          <a href="" data-bind="click:offerViewModel.offerPendingClick">Pending</a>
    </div>
</div>
<div class="panel-body" data-bind="visible:offerViewModel.offerListContainer">
    <table id="offer_tbl" class="table table-striped table-bordered" data-bind="visible:offerViewModel.offerList">
        <thead>
        <tr>
            <th>ID</th>
            <th>Offer Name</th>
            <th>Offer Value</th>
            <th>Publish Date</th>
            <th>End Date</th>
            <th>Total</th>
            <th>Brand Name</th>
            <th>status</th>
            <th>Ads Count</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <!-- ko foreach: offerViewModel.offers() -->
          <tr>
          <td><span data-bind="text:$data.id"></span></td>
          <td><span data-bind="text: $data.name"></span></td>
          <td><span data-bind="text: $data.value_show()"></span></td>
          <td><span data-bind="text: $data.publish_date"></span></td>
          <td><span data-bind="text: $data.valid_to"></span></td>
          <td><span data-bind="text: $data.count"></span></td>
          <td><span data-bind="text: $data.brand_name"></span></td>
          <td><span data-bind="text: $data.statusText()"></span></td>
          <td><span data-bind="text: $data.ads_count()"></span></td>
          <td>
              <!-- ko if: $data.status() == 3 -->
              <a href="" data-bind="click:$root.offerViewModel.approveClick" class="on-default remove-row">
              Approve
              </a>
              <!-- /ko -->
              <!-- ko if: $data.status() != 0 -->
              <a href="" data-bind="click:$root.offerViewModel.deactiveClick" class="on-default remove-row">      Deactive
              </a>
              <!-- /ko -->
        
          </td>

          </tr>
        <!-- /ko -->
        </tbody>
    </table>
</div>
<div class="panel-body" data-bind="visible:offerViewModel.offerNoData">
  <div  class="text-center"><h1>No Data Available</h1></div>
</div>
<div class="panel-body" data-bind="visible:offerViewModel.offerAddContainer" >
   <div class="container-fluid">
        <div class="row">
            <!-- Form Elements -->
            <div class="col-lg-12">
            <div class="card">
              <div class="card-close">
                
              </div>
              <div class="card-header d-flex align-items-center">
                <h3 class="h4">Create Offer</h3>
              </div>
              <div class="card-body">
                <form class="form-horizontal" id="form_Offer">
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Offer Title</label>
                    <div class="col-md-4">
                      <input type="text" name="txtTitle" id="txtTitle" class="form-control" placeholder="Enter Offer Title">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Offer Description</label>
                    <div class="col-md-4">
                      <textarea class="form-control" name="txtOfferDescription" id="txtOfferDescription" placeholder="Enter Offer Description"></textarea>
                    </div>
                  </div>
                  <div class="form-group row">
                      <label class="col-sm-3 form-control-label">Offer Type</label>
                      <div class="col-sm-9" >
                          <div>
                              <input id="txtOfferType1" type="radio" value="1" name="txtOfferType" data-bind="checked: offerViewModel.offerTypeValue" >
                              <label for="txtOfferType1">Fix Value</label>
                          </div>
                          <div>
                              <input id="txtOfferType2" type="radio" value="2" name="txtOfferType" data-bind="checked: offerViewModel.offerTypeValue" >
                              <label for="txtOfferType2">Percentage(%) Based</label>
                          </div>
                      </div>
                  </div>  
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Offer Value</label>
                    <div class="col-sm-9">
                      <div class="row">
                        <div class="col-md-3">
                          <input type="text" name="txtValue" id="txtValue" placeholder="Enter Actual Value" class="form-control">
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="line"></div>
            <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Offer Applicable On<br><small class="text-primary">Specify applicable dates</small></label>
                    <div class="col-sm-9" >
                      
                      <div>
                        <input id="txtApplicableOn1" type="radio" checked="checked" value="1" name="txtApplicableOn">
                        <label for="txtApplicableOn1">Fix Date</label>
                      </div>
                      <div>
                        <input id="txtApplicableOn2" type="radio" value="2" name="txtApplicableOn">
                        <label for="txtApplicableOn2">Week Days (Mon-Fri)</label>
                      </div>
            <div>
                        <input id="txtApplicableOn3" type="radio" value="3" name="txtApplicableOn">
                        <label for="txtApplicableOn3">Weekends  (Sat-Sun)</label>
                      </div>
            <div>
                        <input id="txtApplicableOn4" type="radio"  value="4" name="txtApplicableOn">
                        <label for="txtApplicableOn4">All Days</label>
                      </div>

                    </div>
                  </div>
                  <div class="line"></div>
                   <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Publish Date</label>
                    <div class="col-md-4">
                      <input type="text" id="publishDate" name="publishDate" placeholder="Select Date" class="form-control">
                    </div>
                  </div>
            <div class="form-group row" data-bind="visible:offerViewModel.fixDateVisibilty">
                    <label class="col-sm-3 form-control-label">Select Fix Date</label>
                    <div class="col-md-4">
                      <input type="text" id="fixDate" name="fixDate" placeholder="Select Date" class="form-control">
                    </div>
            <div class="line"></div>
                  </div>
                  <div class="form-group row" data-bind="visible:offerViewModel.validToVisibilty">
                    <label class="col-sm-3 form-control-label">Offer Valid Upto</label>
                    <div class="col-md-4">
                      <input type="text" id="validTo" name="validTo" placeholder="Select Date" class="form-control">
                    </div>
            <div class="line"></div>
                  </div>
            <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Offer Applicable At<br><small class="text-primary">Specify applicable branches</small></label>
                    <div class="col-sm-9">
              <!-- ko foreach: offerViewModel.address -->
              <div>
                  <input type="checkbox" data-bind="attr: { 'id': 'address' + $index() } ,checkedValue: value, checked: $root.offerViewModel.choosenAddress" name="address[]" />
                  <label data-bind="attr: { 'for': 'address' + $index() },text:textaddress"></label>
                </div>
              <!-- /ko -->
                    </div>

                  </div>
                  <div class="line"></div>

            
                
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Offer Limit/Count</label>
                    <div class="col-md-4">
                      <input type="text" class="form-control" id="count" name="count" placeholder="Enter Number"><small class="help-block-none">Maximun count upto which coupon can be issue</small>
                    </div>
                  </div>
            <div class="line"></div>
           
                  <div class="line"></div>
                
            <div class="form-group">
                    <label class="col-sm-3 form-control-label">Important Notes</label>
                    <div class="col-md-4">
            <textarea class="form-control" id="notes" name="notes" placeholder="Enter Important Notes related to this offer"></textarea>
                    </div>
                  </div>
           <!--  <div class="form-group">
                <label class="col-sm-3 form-control-label">Offer Image</label>
                <div class="col-md-4">
                    <input id="fileInput" type="file" name="fileInput" accept="image/gif, image/jpeg, image/png" class="form-control-file">
                </div>
            </div> -->
            <div class="form-group">
                 <label class="col-sm-3 form-control-label">Ads Count</label>
                <div class="col-md-4">
                    <input id="txtAds_count" type="number" min="1" name="fileInput" class="form-control" >
                </div>
            </div>
              
                  <div class="form-group row">
                    <div class="col-sm-4 offset-sm-3">
                      <!--<button type="submit" class="btn btn-secondary">Cancel</button>-->
                      <button type="button" data-bind="click:offerViewModel.createOfferClick" class="btn btn-primary">Create Offer</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
    </div>
</div>