<!-- Aside Starts -->
<aside class="left-panel">
    <div class="logo">
        <a href="{{route('admindashboard')}}" class="logo-expanded">
            <span class="nav-label">Perksvilla</span>
        </a>
    </div>
    <nav class="navigation">
        <ul class="list-unstyled">
            <li class="has-submenu active" id="dashboardTab">
                <a href="{{route('admindashboard')}}">
                    <i class="ion-home"></i> 
                    <span class="nav-label">Dashboard</span>
                </a>
            </li>
            <li class="has-submenu" id="brandTab">
                <a href="#">
                    <i class="ion-flask"></i> 
                    <span class="nav-label">Brand</span>
                </a>
                 <ul class="list-unstyled">
                    <li id="brandAddTab"><a href="{{route('brandadd')}}">Add Brand</a></li>
                    <li id="brandListTab"><a href="{{route('brandlist')}}">Brand List</a></li>
                </ul>
            </li>
            <li class="has-submenu" id="offerTab">
                <a href="#">
                    <i class="ion-flask"></i> 
                    <span class="nav-label">Offer</span>
                </a>
                <ul class="list-unstyled">
                    <li id="offerPendingTab"><a href="{{route('offerpending')}}">Pending for Approval</a></li>
                    <li id="offerActiveTab"><a href="{{route('offeractive')}}">Active Offers</a></li>
                    <li id="offerUpcomingTab"><a href="{{route('offerupcoming')}}">Upcoming Offers</a></li>
                    <li id="offerPastTab"><a href="{{route('offerpast')}}">Past Offers</a></li>
                </ul>
            </li>
            <li class="has-submenu" id="employeeTab">
                <a href="{{route('employeeadmin')}}">
                    <i class="ion-person"></i> 
                    <span class="nav-label">Employees</span>
                </a>
            </li>
            <li class="has-submenu" id="categoryTab">
                <a href="#">
                    <i class="ion-home"></i> 
                    <span class="nav-label">Global Category</span>
                </a>
                <ul class="list-unstyled">
                    <li id="categoryBrandTab"><a href="{{route('brandcategory')}}">Brand</a></li>
                    <li id="categoryRewardTab"><a href="{{route('rewardcategory')}}">Reward</a></li>
                </ul>
            </li>
            <li class="has-submenu" id="locationTab">
                <a href="#">
                    <i class="ion-location"></i> 
                    <span class="nav-label">Location</span>
                </a>
                <ul class="list-unstyled">
                    <li id="locationCountryTab"><a href="{{route('country')}}">Country</a></li>
                    <li id="locationStateTab"><a href="{{route('state')}}">State</a></li>
                    <li id="locationCityTab"><a href="{{route('city')}}">City</a></li>
                </ul>
            </li>
            <li class="has-submenu" id="MailTemplateTab">
                <a href="#">
                    <i class="ion-email"></i> 
                    <span class="nav-label">Mail Templates</span>
                </a>
                <ul class="list-unstyled">
                    <li id="addMailTemplateTab">
                        <a href="{{route('admin.mailtemplates.index')}}">Add Mail Template</a>
                    </li>
                    <li id="listMailTemplateTab">
                        <a href="{{route('admin.mailtemplates.index')}}">Mail Template List</a>
                    </li>
                </ul>
            </li>
            <li class="has-submenu" id="sms_templateTab">
                <a href="#">
                    <i class="ion-email"></i> 
                    <span class="nav-label">SMS Templates</span>
                </a>
                <ul class="list-unstyled">
                    <li id="addSms_templateTab">
                        <a href="{{route('sms.templates')}}">Add SMS Template</a>
                    </li>
                    <li id="listSms_templateTab">
                        <a href="{{route('sms.templates')}}">SMS Template List</a>
                    </li>
                </ul>
            </li>
            <li class="has-submenu" id="sliderTab">
                <a href="#">
                    <i class="ion-flask"></i> 
                    <span class="nav-label">Slider</span>
                </a>
                <ul class="list-unstyled">
                    <li id="sliderAddTab"><a href="{{route('admin.slider.create')}}">Add Slider</a></li>
                    <li id="sliderListTab"><a href="{{route('admin.slider.index')}}">Slider List</a></li>
                </ul>
            </li>
            
            <li class="has-submenu" id="bannerTab">
                <a href="#">
                    <i class="ion-flask"></i> 
                    <span class="nav-label">Banner</span>
                </a>
                <ul class="list-unstyled">
                    <li id="bannerAddTab"><a href="{{route('admin.banner.add')}}">Add Banner</a></li>
                    <li id="bannerListTab"><a href="{{route('admin.banner.index')}}">Banner List</a></li>
                </ul>
            </li>
        </ul>
    </nav>
</aside>
<!-- Aside Ends -->