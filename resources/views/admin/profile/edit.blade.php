@extends('admin.master')
@section('content')
 <div class="wraper container-fluid">
    <div class="row">
        <div class="col-lg-12" id="" data-bind="">
            <div class="panel panel-color panel-primary">
                <div class="panel-heading"> 
                    <h3 class="panel-title">{{ $title }}</h3> 
                </div> 
                <div class="panel-body">
                    <form class="form-horizontal" id ="form_admin">
                        <div class="form-group row">
                            <label class="col-sm-3 form-control-label">Email </label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name ="txtEmail" data-bind="value:email" placeholder="Enter Email Address" >
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-4 offset-sm-3">
                                <button type="button" data-bind="click:updateProfileClick" class="btn btn-primary">Update</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
    @section('script')
        <script type="text/javascript">
          var id = "{{ Session::get('id')}}";
          var token = "{{ Session::get('token')}}";
          var viewProfileApiPath = "{{ config('constant.api.profile.view')}}";
          var updateProfileApiPath = "{{ config('constant.api.profile.update')}}";
        </script>
        <script type="text/javascript" src="{{config('constant.script.profile.view')}}"></script>
    @endsection
@endsection