<!-- Employee Listing -->
<div class="col-lg-12" id="divEmployeeList" data-bind="visible:employeeListContainer">
    <div class="panel panel-color panel-primary">
        <div class="panel-heading"> 
            <h3 class="panel-title">{{ $title }}</h3> 
        </div> 
        <div class="panel-body">
            <table id="employee_tbl" class="table table-striped table-bordered" data-bind="visible:employeeList">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Gender</th>
                        <th>Designation</th>
                        <th>Mobile</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <!-- ko foreach: employees() -->
                    <tr>
                        <td><span data-bind="text:$data.id"></span></td>
                        <td><span data-bind="text:$data.first_name"></span></td>
                        <td><span data-bind="text:$data.last_name"></span></td>
                        <td><span data-bind="text:$data.gender"></span></td>
                        <td><span data-bind="text:$data.designation"></span></td>
                        <td><span data-bind="text:$data.contact"></span></td>
                        <td>
                            <!-- ko if: $data.statusText() == "Deactive" -->
                            <a href="" data-bind="click:$root.activeClick" class="on-default remove-row">      Active
                            </a>
                            <!-- /ko -->
                            <!-- ko if: $data.statusText() == "Active" -->
                            <a href="" data-bind="click:$root.deactiveClick" class="on-default remove-row">      Deactive
                            </a>
                            <!-- /ko -->
                        </td>
                    </tr>
                    <!-- /ko -->
                </tbody>
            </table>
            <div data-bind="visible:employeeNoData" class="text-center"><h1>No Data Available</h1></div>
        </div>
    </div>
</div>