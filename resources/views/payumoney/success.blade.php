<?php

$status = $data["status"];
$firstname = $data["firstname"];
$amount = $data["amount"];
$txnid = $data["txnid"];
$posted_hash = $data["hash"];
$key = $data["key"];
$productinfo = $data["productinfo"];
$email = $data["email"];
$salt = "";

// Salt should be same Post Request 

If (isset($data["additionalCharges"])) {
    $additionalCharges = $data["additionalCharges"];
    $retHashSeq = $additionalCharges . '|' . $salt . '|' . $status . '|||||||||||' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;
} else {
    $retHashSeq = $salt . '|' . $status . '|||||||||||' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;
}
$hash = hash("sha512", $retHashSeq);
if ($hash != $posted_hash) {
    echo "Invalid Transaction. Please try again";
} else {
    echo "<h3>Thank You. Your order status is " . $status . ".</h3>";
    echo "<h4>Your Transaction ID for this transaction is " . $txnid . ".</h4>";
    echo "<h4>We have received a payment of Rs. " . $amount . ". Your order will soon be shipped.</h4>";
}
?>	