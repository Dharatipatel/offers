@extends('user.layouts.login_master')
@section("title")
Register - 
@stop
@section("content")
<div class="wrapper-page animated fadeInDown">
    <div class="panel panel-color panel-primary">
        <div class="panel-heading"> 
            <h3 class="text-center m-t-10"> Register to <strong>{{env("PROJECT_NAME")}}</strong> </h3>
        </div> 
        {!! Form::open(["url"=>route('auth.user.register'),"method"=>"POST", 'id'=>'login_form', 'class'=>"form-horizontal m-t-40"]) !!}
        {!! Form::hidden("refferal_code", @Request::get('referral_code')) !!}
        <div class="form-group ">
            <div class="col-xs-12">
                {{ Form::text('first_name',null,['class'=>'form-control','placeholder'=>'First Name','id'=>"first_name", "autofocus"=>true]) }}
                @if($errors->has("first_name"))
                <span class="text-danger flash">{{ $errors->first("first_name") }}</span>
                @endif
            </div>
        </div>
        <div class="form-group ">
            <div class="col-xs-12">
                {{ Form::text('last_name',null,['class'=>'form-control','placeholder'=>'Last Name','id'=>"last_name"]) }}
                @if($errors->has("last_name"))
                <span class="text-danger flash">{{ $errors->first("last_name") }}</span>
                @endif
            </div>
        </div>
        <div class="form-group ">
            <div class="col-xs-12">
                {{ Form::email('email',null,['class'=>'form-control','placeholder'=>'E-mail address','id'=>"email"]) }}
                @if($errors->has("email"))
                <span class="text-danger flash">{{ $errors->first("email") }}</span>
                @endif
            </div>
        </div>
        <div class="form-group ">
            <div class="col-xs-12">
                {{ Form::password('password', ['class'=>'form-control','placeholder'=>'Password','id'=>"password"]) }}
                @if($errors->has("password"))
                <span class="text-danger flash">{{ $errors->first("password") }}</span>
                @endif
            </div>   

            @if($errors->has("error"))
            <div class="col-xs-12">
                <span class="text-danger flash">{{ $errors->first("error") }}</span>
            </div>
            @endif
        </div>
        <div class="form-group text-right">
            <div class="col-xs-12">
                <button class="btn login-btn w-md" type="submit">Register</button>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection

@section("script")
<script>
    $(document).ready(function () {
        $("#login_form").validate({
            rules: {
                first_name: {
                    required: true,
                },
                last_name: {
                    required: true,
                },
                email: {
                    required: true,
                    email: true
                },
                password: {
                    required: true,
                }
            },
            messages: {
                first_name: {
                    required: "Please enter first name",
                },
                last_name: {
                    required: "Please enter last name",
                },
                email: {
                    required: "Please enter an email",
                    email: "Please enter a valid email"
                },
                password: {
                    required: "Please enter password",
                }
            }
        });
    });
</script>
@stop