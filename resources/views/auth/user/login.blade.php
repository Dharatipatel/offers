@extends('user.layouts.login_master')
@section("title")
Login - 
@stop
@section("content")
<div class="wrapper-page animated fadeInDown">
    <div class="panel panel-color panel-primary">
        <div class="panel-heading"> 
            <h3 class="text-center m-t-10"> Login to <strong>{{env("PROJECT_NAME")}}</strong> </h3>
        </div> 
        
        {!! Form::open(["url"=>route('auth.user.login'),"method"=>"POST", 'id'=>'login_form', 'class'=>"form-horizontal m-t-40"]) !!}
        <div class="form-group ">
            @if(session()->has("success"))
            <div class="col-xs-12">
                <span class="text-success flash">{{ session()->get("success") }}</span>
            </div>
            @endif
            <div class="col-xs-12">
                {{ Form::email('email',null,['class'=>'form-control','placeholder'=>'E-mail address','id'=>"email", "autofocus"=>true]) }}
                @if($errors->has("email"))
                <span class="text-danger flash">{{ $errors->first("email") }}</span>
                @endif
            </div>
        </div>
        <div class="form-group ">
            <div class="col-xs-12">
                {{ Form::password('password', ['class'=>'form-control','placeholder'=>'Password','id'=>"password"]) }}
                @if($errors->has("password"))
                <span class="text-danger flash">{{ $errors->first("password") }}</span>
                @endif
            </div>

            @if($errors->has("error"))
            <div class="col-xs-12">
                <span class="text-danger flash">{{ $errors->first("error") }}</span>
            </div>
            @endif
        </div>
        <div class="form-group text-right">
            <div class="col-xs-12">
                <button class="btn login-btn w-md" type="submit">Login</button>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection
@section("script")
<script>
$(document).ready(function () {
    $("#login_form").validate({
        rules: {
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
            }
        },
        messages: {
            email: {
                required: "Please enter an email",
                email: "Please enter a valid email"
            },
            password: {
                required: "Please enter password",
            }
        }
    });
});
</script>
@stop