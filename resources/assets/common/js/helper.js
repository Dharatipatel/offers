function validateEmail(email) {
    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    if (filter.test(email)) {
        return true;
    } else {
        return false;
    }
}
function apiAjax(config) {

    var apisetting = {
        statusCode: {
            401: function () {
                logout();
            }
        },
        type: config.type || "GET",
        url: config.url,
        data: config.data,
        datatype: config.datatype || "json",
        cache: config.cache || false,
        async: config.async || true,
        success: config.success,
        error: config.error,
        complete: config.complete
    };
    if (typeof config.contentType != "undefined") {
        apisetting.contentType = config.contentType;
    }
    if (typeof config.processData != "undefined") {
        apisetting.processData = config.processData;
    }
    if (config.token) {
        apisetting.beforeSend = function (xhr) {
            xhr.setRequestHeader("Authorization", config.token);
        };
    }
    return apiajax = $.ajax(apisetting);
}

function logout() {
    location.href = logoutUrl;
}
function preventGoBack() {
    history.pushState(null, null, location.href);
    window.onpopstate = function (event) {
        history.go(1);
    };
}

function deleteRow(id) {
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this imaginary file!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel plx!",
        closeOnConfirm: false,
        closeOnCancel: true
    },
            function (isConfirm) {
                if (isConfirm) {
                    window.location = $("#del_" + id).data('url');
                }
            });
}


$("#clicker").on("click", function () {
    $("#file").trigger("click");
});

$("#file").change(function ()
{
    thumbnail(this);
});

function thumbnail(input) {
    if (input.files && input.files[0])
    {
        var reader = new FileReader();
        reader.onload = function (e)
        {
            $('#image').attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    } else
    {
        /*$('#image').attr('src', "");*/
    }
}
