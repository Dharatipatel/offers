function CategoryModel(data) {
    self = this;
    self.id = ko.observable(data.id);
    self.name = ko.observable(data.name);
    self.status = ko.observable(data.status);
    self.description = ko.observable(data.description);
    self.statusText = function () {
        if (data.status == 0) {
            return "Deactive";
        } else if (data.status == 1) {
            return "Active";
        }
    }
}
function BrandModel(data) {
    self = this;

    self.id = ko.observable(data.id);
    self.uid = ko.observable(data.uid);
    self.name = ko.observable(data.name);
    self.categoryid = ko.observable(data.brand_category);
    self.category = ko.observable(data.brand_category_name);
    self.contact = ko.observable(data.contact);
    self.email = ko.observable(data.email);
    self.contact_person_name = ko.observable(data.contact_person_name);
    self.description = ko.observable(data.description);
    self.business_hours = ko.observable(data.business_hours);
    self.website_url = ko.observable(data.website_url);
    self.facebook_page = ko.observable(data.facebook_page);
    self.address = ko.observable(data.address);
    self.pincode = ko.observable(data.pincode);
    self.logo = ko.observable(data.logo);
    self.status = ko.observable(data.status);
    self.country = ko.observable(data.country);
    self.state = ko.observable(data.state);
    self.city = ko.observable(data.city);
    self.offer_count = ko.observable(data.offer_count);
    self.created = ko.observable(data.created);
    self.statusText = function () {
        if (data.status == 0) {
            return "Deactive";
        } else if (data.status == 1) {
            return "Active";
        }
    }
}
function OfferModel(data) {
    self = this;

    self.id = ko.observable(data.id);
    self.name = ko.observable(data.title);
    self.value = ko.observable(data.value);
    self.value_show =   function(){
        if(data.offer_type==1){
            return data.value+" Rs";
        } else if(data.offer_type==2){
            return data.value + " %";
        }
    }
    self.ads_count = function(){
        if (data.ads_count  != null) {
            return data.ads_count;
        } 
        return 0;
    }
    self.price = ko.observable(data.price);
    self.apply_on = ko.observable(data.apply_on);
    self.valid_to = ko.observable(data.valid_to);
    self.publish_date = ko.observable(data.publish_date);
    self.count = ko.observable(data.count);
    self.issued = ko.observable("");
    self.redeem = ko.observable("");
    self.status = ko.observable(data.offer_status_id);
    self.brand_name = ko.observable(data.brand_name);
    self.is_best_offer = ko.observable(data.is_best_offer);
    self.is_banner_offer = ko.observable(data.is_banner_offer);
    self.statusText = function () {
        if (data.offer_status_id == 0) {
            return "Deactive";
        } else if (data.offer_status_id == 1) {
            return "Active";
        } else if (data.offer_status_id == 2) {
            return "Upcoming";
        } else if (data.offer_status_id == 3) {
            return "Pending";
        } else if (data.offer_status_id == 4) {
            return "Canceled";
        } else if (data.offer_status_id == 5) {
            return "Deleted";
        } else if (data.offer_status_id == 6) {
            return "Expired";
        }

    }
    self.applicable_date = function () {
        if (data.apply_on == 1) {
            return "Applicable on " + data.valid_to + " Only";
        } else {
            return "Applicable UpTo " + data.valid_to;
        }
    }
    self.expiries_text = function () {
        days = (new Date(data.valid_to) - new Date()) / (1000 * 60 * 60 * 24);
        if (Math.round(days) < 0) {
            return "Expired";
        } else {
            return "Expires in " + Math.round(days) + " days";
        }
    }
}
function EmployeeModel(data) {
    self = this;
    self.id = ko.observable(data.id);
    self.birth_date = ko.observable(data.birth_date);
    self.contact = ko.observable(data.contact);
    self.corporate_id = ko.observable(data.corporate_id);
    self.designation = ko.observable(data.designation);
    self.email = ko.observable(data.email);
    self.first_name = ko.observable(data.first_name);
    self.last_name = ko.observable(data.last_name);
    self.joining_date = ko.observable(data.joining_date);
    self.profile_pic = ko.observable(data.profile_pic);
    self.status = ko.observable(data.status);
    self.gender = ko.observable(data.gender);
    self.statusText = function () {
        if (data.status == 0) {
            return "Deactive";
        } else if (data.status == 1) {
            return "Active";
        }

    }
}
function CorporateModel(data) {
    self = this;

    self.id = ko.observable(data.id);
    self.uid = ko.observable(data.uid);
    self.name = ko.observable(data.name);
    // self.categoryid =ko.observable(data.brand_category);
    self.category =ko.observable(data.corporate_category_name);
    self.contact = ko.observable(data.contact);
    self.email = ko.observable(data.email);
    self.contact_person_name = ko.observable(data.contact_person_name);
    self.description = ko.observable(data.description);
    self.website_url = ko.observable(data.website_url);
    self.facebook_page = ko.observable(data.facebook_page);
    self.linkedin_page = ko.observable(data.linkedin_page);
    self.address = ko.observable(data.address);
    self.logo = ko.observable(data.logo);
    self.company_size = ko.observable(data.company_size);
    self.status = ko.observable(data.status);
    self.employee_count = ko.observable(data.employee_count);
    self.statusText = function () {
        if (data.status == 0) {
            return "Deactive";
        } else if (data.status == 1) {
            return "Active";
        }
    }
}
function CountryModel(data) {
    self = this;
    self.id = ko.observable(data.id);
    self.name = ko.observable(data.name);
    self.status = ko.observable(data.status);
    self.statusText = function () {
        if (data.status == 0) {
            return "Deactive";
        } else if (data.status == 1) {
            return "Active";
        }
    }
}
function StateModel(data) {
    self = this;
    self.id = ko.observable(data.id);
    self.country_id = ko.observable(data.country_id);
    self.country_name = ko.observable(data.country_name);
    self.name = ko.observable(data.name);
    self.status = ko.observable(data.status);
    self.statusText = function () {
        if (data.status == 0) {
            return "Deactive";
        } else if (data.status == 1) {
            return "Active";
        }
    }
}
function CityModel(data) {
    self = this;
    self.id = ko.observable(data.id);
    self.state_id = ko.observable(data.state_id);
    self.state_name = ko.observable(data.state_name);
    self.status = ko.observable(data.status);
    self.name = ko.observable(data.name);
    self.statusText = function () {
        if (data.status == 0) {
            return "Deactive";
        } else if (data.status == 1) {
            return "Active";
        }
    }
}

function brandOfferModel(data) {
    console.log(data);
    self = this;
    self.brand_id = ko.observable(data.brand_id);
    self.brand_logo = ko.observable(data.brand_logo);
    self.brand_name = ko.observable(data.brand_name);
    self.category = ko.observable(data.brand_category_name);
    self.freecoupen = ko.observable(data.freecoupen);
    self.fixcoupen = ko.observable(data.fixcoupen);
}

function RewardModel(data) {
    self = this;
    self.brand_name = ko.observable(data.brand_name);
    self.category_id = ko.observable(data.category_id);
    self.category_name = ko.observable(data.category_name);
    self.coupen_value   = ko.observable(data.coupon_value);
    self.first_name = ko.observable(data.first_name);
    self.last_name = ko.observable(data.last_name);
    self.issue_date  = ko.observable(data.issue_date);
    self.offer_id  = ko.observable(data.offer_id);
    self.redeem_date  = ko.observable(data.redeem_date);
    self.designation  = ko.observable(data.designation);
    self.bill_amount  = ko.observable(data.bill_amount);
    self.created  = ko.observable(data.created);
    if(data.coupen){
       self.coupon  = ko.observable(data.coupen); 
    }

}

function SMSTemplateModel(data) {
    self = this;
    self.id = ko.observable(data.id);
    self.title = ko.observable(data.title);
    self.text = ko.observable(data.text);

}
function TransactionModel(data){
    self = this;
    self.id = ko.observable(data.id);
    self.created = ko.observable(data.created);
    self.bank = ko.observable("");
    self.user_id = ko.observable(data.user_id);
    self.withdraw_amount = ko.observable(data.withdraw_amount);
    self.status = ko.observable(data.status);
    self.statusText = function(){
        if(data.status==1){
            return "Success";
        } else if(data.status==0) {
            return "Pending";
        }
    }
}