var viewModelName;
var couponViewModel;

$(document).ready(function(){
	initializeKnockout();
    $('.has-submenu').removeClass('active');
    $('#redeemCouponTab').addClass('active');
    $("#coupen_form").validate({
        rules: {
            txtCoupenCode : "required",
            txtEmailPhone: "required",
        },
        messages: {
            txtCoupenCode: "Please enter a Coupen Code",
            txtEmailPhone: "Please enter an Email Id or Mobile Number",
        },
    });
     $("#redeem_form").validate({
        rules: {
            txtBillAmount : "required",
            txtMember: {
                
                required: true,
                min: 1
            },
            txtRedeemCoupenCode: "required",
        },
        messages: {
            txtBillAmount: "Please enter a Bill Amount",
            txtMember :{
              'required'  : "Please enter a Member",
              
              'min'  : "It Should be Minimum 1",
            },
            txtRedeemCoupenCode: "Please enter Coupen Code",
        },
    });
});

function initializeKnockout(){
	couponViewModel = new CouponViewModel();
	viewModelName  = couponViewModel;
	ko.applyBindings(couponViewModel);
}

function CouponViewModel(){
    self = this;
    self.coupenCode = ko.observable();
    self.emailPhone = ko.observable();
    self.isNoCouponAvailble = ko.observable(false);
    self.isCouponAvailble = ko.observable(false);
    self.issued_by = ko.observable();
    self.issued_for = ko.observable();
    self.issued_date = ko.observable();
    self.expire_date = ko.observable();
    self.coupon_value = ko.observable(0);
    self.billAmount = ko.observable(0);
    self.member = ko.observable("");
    self.customerPayble = ko.observable(0);
    self.reward_id = ko.observable("");
    self.searchCoupenDetails = function(){
        if(!$("#coupen_form").valid()){
            return false;
        }
        getCoupenDetails();        
    }
    self.billAmountChange = function(){
        couponViewModel.customerPayble(couponViewModel.billAmount()-couponViewModel.coupon_value());
    }
    self.redeemCoupen = function(){
        if(!$("#redeem_form").valid()){
            return false;
        }
        redeemCoupon();
    }
}

function getCoupenDetails(){
    var setting = {};
    couponViewModel.coupenCode($("#txtCoupenCode").val());
    couponViewModel.emailPhone($("#txtEmailPhone").val());
    var datavalue = {
        'txtCoupenCode' : couponViewModel.coupenCode(),
        'txtEmailPhone' : couponViewModel.emailPhone()
    };
    setting.url  = couponDetailApiPath;
    setting.type = 'POST';
    setting.data = datavalue;
    setting.token = window.btoa(id+"|"+token);
    setting.success = function(response){
        var coupen_detail = response.data.data;
        if(coupen_detail.length>0){
            $.each(coupen_detail, function(i,item) {
               couponViewModel.isCouponAvailble(true);
               couponViewModel.isNoCouponAvailble(false);
               couponViewModel.issued_by(coupen_detail[i].company_name);
               couponViewModel.issued_for(coupen_detail[i].first_name +" "+coupen_detail[i].last_name);
               couponViewModel.issued_date(coupen_detail[i].issue_date);
               couponViewModel.expire_date(coupen_detail[i].expiry_date);
               couponViewModel.coupon_value(coupen_detail[i].coupen_value);
               couponViewModel.reward_id(coupen_detail[i].reward_id);
            });
        }
        else{
            couponViewModel.isCouponAvailble(false);
            couponViewModel.isNoCouponAvailble(true);
            swal("Coupon Code/Email/Phone not exist Or Coupen May be Redeemed" , "","error")
            resetall();
        }
    };
    setting.error = function(jqXHR, textStatus, error){
        couponViewModel.isCouponAvailble(false);
        couponViewModel.isNoCouponAvailble(true);
        swal("Coupon Code/Email/Phone not exist" , "","error")
        
    };
    apiAjax(setting);
}
function redeemCoupon(){
    var setting = {};
    couponViewModel.coupenCode($("#txtCoupenCode").val());
    couponViewModel.emailPhone($("#txtEmailPhone").val());
    var datavalue = {
        'txtRedeemCoupenCode' : couponViewModel.coupenCode(),
        'txtBillAmount' : couponViewModel.billAmount(),
        'txtMember' : couponViewModel.member(),
        'reward_id' : couponViewModel.reward_id()
    };
    setting.url  = redeemCouponApiPath;
    setting.type = 'POST';
    setting.data = datavalue;
    setting.token = window.btoa(id+"|"+token);
    setting.success = function(response){
        swal("Redeemed Coupon SuccessFully","","error");
        location.href = couponWebPath;    
    };
    setting.error = function(jqXHR, textStatus, error){
       swal(jqXHR.responseJSON.message,"","error");
       resetall();
       return false;
    };
    apiAjax(setting);
}
function resetall(){
    couponViewModel.coupenCode("");
    couponViewModel.emailPhone("");
    couponViewModel.issued_by("");
    couponViewModel.issued_for("");
    couponViewModel.issued_date("");
    couponViewModel.expire_date("");
    couponViewModel.coupon_value("");
    couponViewModel.billAmount("");
    couponViewModel.member("");
}