var viewModelName;
var orderHistoryViewModel;
$(document).ready(function(){
    $('.has-submenu').removeClass('active');
    $('#orderHistoryTab').addClass('active');
    initializeKnockout();
    getOrderHistory();
});
function initializeKnockout(){
    orderHistoryViewModel = new OrderHistoryViewModel();
    viewModelName  = orderHistoryViewModel;
    ko.applyBindings(orderHistoryViewModel);
}

function OrderHistoryViewModel(){
    self = this;
    self.total = ko.observable("");
    self.orderhistory = ko.observableArray([]);
    self.reward_issued= ko.observable(0);
    self.total_save = ko.observable(0);
    self.total_invest = ko.observable(0);
    self.orderHistoryNoData = ko.observable(false);
    self.orderHistoryList = ko.observable(false);
    
   
}

function getOrderHistory(){
    var setting = {};
    var datavalue = {
       'redeem':1,
    };
    
    setting.url  = brandOrderHistoryApiPath;
    setting.type = 'GET';
    setting.data = datavalue;
    setting.token = window.btoa(id+"|"+token);
    setting.success = function(response){
        var order_list = response.data.data;
            orderHistoryViewModel.orderhistory([]);
        if(order_list.length>0){
            $.each(order_list, function(i,item) {
                orderHistoryViewModel.orderhistory.push(new RewardModel(item));
            });
            orderHistoryViewModel.orderHistoryList(true);
            orderHistoryViewModel.orderHistoryNoData(false);
        } else {
            orderHistoryViewModel.orderHistoryList(false);
            orderHistoryViewModel.orderHistoryNoData(true);
        }
    };
    setting.error = function(jqXHR, textStatus, error){
        swal(jqXHR.responseJSON.message,"","error");
        return false;
    };
    apiAjax(setting);

}
function getOrderHistoryStatistic(){
    var setting = {};
    var datavalue = {
       'cid':cid,
    };
    
    setting.url  = orderHistoryStatisticApi;
    setting.type = 'GET';
    setting.data = datavalue;
    setting.token = window.btoa(id+"|"+token);
    setting.success = function(response){
        var order_list = response.data;
        if(order_list){
           orderHistoryViewModel.reward_issued(order_list.total_reward);
           orderHistoryViewModel.total_save(order_list.total_saved);
           orderHistoryViewModel.total_invest(order_list.total_invested);
        } 
    };
    setting.error = function(jqXHR, textStatus, error){
        swal(jqXHR.responseJSON.message,"","error");
        return false;
    };
    apiAjax(setting);
}