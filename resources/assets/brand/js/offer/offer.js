var viewModelName;
var offerViewModel;

$(document).ready(function () {
    $('.has-submenu').removeClass('active');
    $('#offersTab').addClass('active');
    initializeKnockout();
    if(type=='list'){
        offerViewModel.status(status);
        $('#listOfferTab').addClass('active');
        if(status==3){
            $('.offerstatus').removeClass('active');
            $('#pendingTab').addClass('active');
        }
    } else if(type=='add'){
        $('#addOfferTab').addClass('active');
    } else if(type=='past'){
        offerViewModel.status(6);
        $('#pastOfferTab').addClass('active');
    }
    $("#form_Offer").validate({
        rules: {
            txtTitle: "required",
            txtDescription: "required",
            txtValue: {
                'required': true,
                'digits': true,
                'min':1
            },
            txtApplicableOn: "required",
            txtOfferType: "required",
            fixDate: {
                required: '#txtApplicableOn1[value=1]:checked',
            },
            validTo: {
                required: '#txtApplicableOn2[value=2]:checked',
            },
            'address[]': {
                required: true,
            },
            count: {
                "required": true,
                "digits": true,
            },
            publishDate: "required",
            notes: "required",
        },
        messages: {
            txtTitle: "Please enter Offer title",
            txtDescription: "Please enter Offer description",
            txtValue: {
                'required': 'Please Enter Offer Value',
                'digits': 'Please enter Offer Value in digits',
                'min':"Offer value should be grater than 0"
            },
            txtApplicableOn: "Please select Applicable Place",
            txtOfferType: "Please select Offer Type",
            fixDate: "Please select Fix date",
            validTo: "Please select Valid To date",
            'address[]': {
                required: "You must check at least 1 Address",
            },
            count: {
                'required': 'Please Enter Offer Count',
                'digits': 'Offer Count should be only in digits'
            },
            publishDate: "Please Select Publish Date",
            notes: "Please Enter Important Notes",

        },
    });
    $('#fixDate').datepicker({startDate: '-0d', format: 'yyyy-m-d', autoclose: true,
        todayHighlight: true});
    $('#validTo').datepicker({startDate: '-0d', format: 'yyyy-m-d', autoclose: true,
        todayHighlight: true});
    $('#publishDate').datepicker({startDate: '-0d', format: 'yyyy-m-d', autoclose: true,
        todayHighlight: true});
    offerViewModel.status(1);
    getOffers();
    getAddress();

    $('#fileInput').change(function () {
        var file = $('#fileInput').prop('files')[0];
        var ImageURL;
        var FR = new FileReader();
        FR.addEventListener("load", function () {
                    ImageURL = FR.result;
            var block = ImageURL.split(";");
            // Get the content type of the image
            var contentType = block[0].split(":")[1];// In this case "image/gif"
            // get the real base64 content of the file
            var realData = block[1].split(",")[1];// In this case "R0lGODlhPQBEAPeoAJosM...."

            offerViewModel.imageUri(realData);
            }, false);
            if (file) {
            FR.readAsDataURL(file);
            }
    });
});


function initializeKnockout() {
    offerViewModel = new OfferViewModel();
    viewModelName = offerViewModel;
    ko.applyBindings(offerViewModel);
}

function OfferViewModel() {
    self = this;
    self.status = ko.observable(0);
    self.offerTypeValue = ko.observable("1");
    self.total = ko.observable(0);
    self.offers = ko.observableArray([]);
    self.offerList = ko.observable(false);
    self.offerNoData = ko.observable(false);
    self.fixDateVisibilty = ko.observable(true);
    self.validToVisibilty = ko.observable(false);
    self.address = ko.observableArray([]);
    self.choosenAddress = ko.observableArray([]);
    
    /*Image Upload*/
    self.imageUri = ko.observable("");
    self.imagePath = ko.observable(path);
    self.offerActiveClick = function () {
        offerViewModel.status(1);
        getOffers();
    }
    self.offerUpcomingClick = function () {
        offerViewModel.status(2);
        getOffers();
    }
    self.offerPendingClick = function () {
        offerViewModel.status(3);
        getOffers();
    }
    self.offerExpiredClick = function () {
        offerViewModel.status(6);
        getOffers();
    }
    self.offerCancelledClick = function () {
        offerViewModel.status(5);
        getOffers();
    }
    self.offerDeletedClick = function () {
        offerViewModel.status(4);
        getOffers();
    }
    self.createOfferClick = function () {
        if (!$("#form_Offer").valid()) {
            return false;
        }
        var datavalue = {};
        var applicableDate = '';
        var title = $('#txtTitle').val();
        var description = $('#txtDescription').val();
        var value = $('#txtValue').val();
        var count = $('#count').val();
        var publish_date = $('#publishDate').val();
        var notes = $('#notes').val();
        var applicableOn = $("input[name='txtApplicableOn']:checked").val();
        var offerType = $("input[name='txtOfferType']:checked").val();
        if (applicableOn == 1) {
            applicableDate = $('#fixDate').val();
        } else {
            applicableDate = $('#validTo').val();
        }
        if(!moment(applicableDate, 'YYYY-MM-DDTHH:mm:ssZ').isAfter(moment($('#publishDate').val(),'YYYY-MM-DDTHH:mm:ssZ'))){
             
            $("#publishDate").after(function() {
                return "<label id='publishDate'-error' class='error' for='publishDate'>Publish Date can not grater than Fix date or Expiry Date</label>";
            });
            return false;        
        }
        var setting = {};
        datavalue['title'] = title;
        datavalue['description'] = description;
        datavalue['value'] = value;
        datavalue['brand_address_id'] = offerViewModel.choosenAddress();
        datavalue['offer_type'] = offerType;
        datavalue['apply_on'] = applicableOn;
        datavalue['valid_to'] = applicableDate;
        datavalue['count'] = count;
        datavalue['publish_date'] = publish_date;
        datavalue['notes'] = notes;
        datavalue['brand_id'] = bid;
        datavalue['image'] = offerViewModel.imageUri();

        setting.url = addOfferApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id + "|" + token);
        setting.success = function (response) {

            resetAll();
            location.href = offerList;
        };
        setting.error = function (jqXHR, textStatus, error) {
            swal(jqXHR.responseJSON.message, "", 'error');
            return false;
        };
        apiAjax(setting);
    }

}

function resetAll() {
    $('#txtTitle').val("");
    $('#txtDescription').val("");
    $('#txtValue').val("");
    $('#txtPrice').val("");
    $('#validTo').val("");
    $('#count').val("");
    $('#publishDate').val("");
    $('#notes').val("");
}
function getOffers() {
    var setting = {};
    var datavalue = {
        'bid': bid,
        'status': offerViewModel.status(),
    };
    setting.url = offerListApiPath;
    setting.type = 'GET';
    setting.data = datavalue;
    setting.token = window.btoa(id + "|" + token);
    setting.success = function (response) {
        console.log(response);
        offerViewModel.total(response.data.total);
        var offer_list = response.data.data;
        if (offer_list.length > 0) {
            offerViewModel.offers([]);
            $.each(offer_list, function (i, item) {
                offerViewModel.offers.push(new OfferModel(item));
            });

            offerViewModel.offerList(true);
            offerViewModel.offerNoData(false);

        } else {
            offerViewModel.offerList(false);
            offerViewModel.offerNoData(true);
        }
    };
    setting.error = function (jqXHR, textStatus, error) {
        swal(jqXHR.responseJSON.message, "", "error");
        return false;
    };
    apiAjax(setting);

}
function getAddress() {
    var setting = {};
    var datavalue = {
        'bid': bid,
    };
    setting.url = addressListApiPath;
    setting.type = 'GET';
    setting.data = datavalue;
    setting.token = window.btoa(id + "|" + token);
    setting.success = function (response) {
        var address_list = response.data.data;
        if (address_list.length > 0) {
            $.each(address_list, function (i, item) {
                offerViewModel.address.push(new availableAddress(item.id, item.address, item.city, item.state, item.country, item.pincode));
            });
        }
    };
    setting.error = function (jqXHR, textStatus, error) {
        swal(jqXHR.responseJSON.message, "", "error");
        return false;
    };
    apiAjax(setting);
}
$('input[type=radio][name=txtApplicableOn]').change(function () {
    if (this.value == 1) {
        offerViewModel.fixDateVisibilty(true);
        offerViewModel.validToVisibilty(false);
    } else {
        offerViewModel.fixDateVisibilty(false);
        offerViewModel.validToVisibilty(true);
    }
});
function availableAddress(k, address, city, state, country, pincode) {
    var self = this;
    self.value = ko.observable(k);
    self.textaddress = ko.observable(address + ", " + city + " ," + state + " ," + country + " ," + pincode);
}