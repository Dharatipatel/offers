var viewModelName;
var walletViewModel;

$(document).ready(function(){
	initializeKnockout();
    getWalletTransaction();
    getWalletAmount();
    var rules = {
            txtAmount: {
                'required' :true,
                'digits': true
            },
        };
    var messages = {
            txtAmount: {
                'required' : 'Please Enter Amount',
                'digits' : 'Amount should be only in digits'
            },
        };
    $("#form_withdraw").validate({
        rules:rules ,
        messages: messages,
    });
});



function initializeKnockout(){
	walletViewModel = new WalletViewModel();
	viewModelName  = walletViewModel;
	ko.applyBindings(walletViewModel);
}

function WalletViewModel(){
	self = this;
    self.amount = ko.observable('');
    self.availble_balance = ko.observable(0);
    self.transactionList = ko.observable(false);
    self.transactionNoData = ko.observable(false);
    self.transactions = ko.observableArray([]);
	self.withDrawButton = function(){
        if(!$('#form_withdraw').valid()){
            return false;
        }
        if(walletViewModel.amount()>walletViewModel.availble_balance()){
            $('#txtAmount').addClass('error');
            $('#txtAmount').parent().append('<label id="txtAmount-error" class="error" for="txtAmount">You have not sufficient Balance</label>');
            return false;
        }
        var setting = {};
        var datavalue = {
            'user_id' : id,
            'brand_id' : bid,
            'txtAmount' : walletViewModel.amount()
        };
        setting.url  = withDrawAmountApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id+"|"+token);
        setting.success = function(response){
            walletViewModel.amount("");
            getWalletTransaction();
            getWalletAmount();
        };
        setting.error = function(jqXHR, textStatus, error){
            swal(jqXHR.responseJSON.message,"","error");
            return false;
        };
        apiAjax(setting);
    }

}

function getWalletTransaction(){
    var setting = {};
    var datavalue = {
        'user_id' : id,
        'brand_id' : bid,
    };
    setting.url  = withDrawTranscationApiPath;
    setting.type = 'GET';
    setting.data = datavalue;
    setting.token = window.btoa(id+"|"+token);
    setting.success = function(response){
        var transaction_list = response.data.data;
        if(transaction_list.length>0){
            walletViewModel.transactions([]);
            $.each(transaction_list, function(i,item) {
                walletViewModel.transactions.push(new TransactionModel(item));
            });

            walletViewModel.transactionList(true);
            walletViewModel.transactionNoData(false);

        } else {
            walletViewModel.transactionList(false);
            walletViewModel.transactionNoData(true);
        }
    };
    setting.error = function(jqXHR, textStatus, error){
        swal(jqXHR.responseJSON.message,"","error");
        return false;
    };
    apiAjax(setting);
}

function getWalletAmount(){
    var setting = {};
    var datavalue = {
        'user_id' : id,
    };
    setting.url  = brandBalanceApiPath;
    setting.type = 'GET';
    setting.data = datavalue;
    setting.token = window.btoa(id+"|"+token);
    setting.success = function(response){
        
        var amount_list = response.data.data;
        if(amount_list.length>0){
            $.each(amount_list, function(i,item) {
                walletViewModel.availble_balance(item.balance);
            });
        } else {
                walletViewModel.availble_balance(0);
        }
    };
    setting.error = function(jqXHR, textStatus, error){
        swal(jqXHR.responseJSON.message,"","error");
        return false;
    };
    apiAjax(setting);
}