var viewModelName;
var corporateViewModel;
var corporateApiPath;
$(document).ready(function(){
    $('.has-submenu').removeClass('active');
    $('#corporateTab').addClass('active');
    initlizeKnockout();
    getCategories();
    if(type == 'add'){
        $('#corporateAddTab').addClass('active');
        $('#corporateListTab').removeClass('active');
        corporateViewModel.corporateListContainer(false);
        corporateViewModel.corporateAddContainer(true);
        corporateViewModel.action_title("Add");
        corporateViewModel.action_name("Add Corporate");
        corporateViewModel.action("add");
    } else if(type == 'list'){
        $('#corporateListTab').addClass('active');
        $('#corporateAddTab').removeClass('active');
        corporateViewModel.corporateAddContainer(false);
        corporateViewModel.corporateListContainer(true);
        getCorporates();
    }
    var rules = {
            
            txtCorporateName : "required",
            txtCompany_size :{
                'required' :true,
                'digits': true
            },
            txtDescription : "required",
            txtCategory : "required",
            txtEmail: {
                'required' :true,
                'email' :true,
            },
            txtMobile: {
                'required' :true,
                'digits': true
            },
            txtPassword : "required",
            txtBusinessHours: {
                'required' :true,
            },
            txtPersonName : "required",
            txtWebsiteUrl :{
                "required" :true,
            },
            txtFacebookPage : "required",
            txtLinkedInPage : "required",
            txtAddress : "required",
            txtStatus : "required",
        };
    var messages = {
            txtBrandName: "Please enter a Corporate name",
            txtCompany_size: {
                'required' : 'Please Enter Company size',
                'digits' : 'Company size should be only in digits'
            },
            txtDescription: "Please enter  a description",
            txtCategory: "Please select category",
            txtEmail: {
                'required' : 'Please Enter Email',
                'email' : 'Please enter a valid email address'
            },
            txtMobile: {
                'required' : 'Please Enter mobile',
                'digits' : 'Mobile should be only in digits'
            },
            txtPassword: "Please enter password",
            txtPersonName: "Please enter person name",
            txtWebsiteUrl: {
                'required':"Please Enter url",
            },
            txtFacebookPage: "Please Enter facebook page",
            txtLinkedInPage: "Please Enter LinkedIn page",
            txtAddress: "Please Enter address",
            txtStatus: "Please select  a Status",
        };
    $("#form_corporate").validate({
        rules:rules ,
        messages: messages,
    });
    $("#form_employee").validate({
        rules:{
            txtFirstName : "required",
            txtLastName : "required",
            gender : "required",
            txtDesignation : "required",
            txtEmailEmployee: {
                'required' :true,
                'email' :true,
            },
            txtContact: {
                'required' :true,
                'digits': true
            },
            dateBirth : { 
                required:true,
            },
            dateJoin : { 
                required :true,
            },
        },
        messages: {
            txtFirstName: "Please enter First name",
            txtLastName: "Please enter Last name",
            gender: "Please select gender ",
            txtDesignation: "Please enter Designation",
            txtEmailEmployee: {
                'required' : 'Please Enter Email',
                'email' : 'Please enter valid Email'
            },
            txtContact: {
                'required' : 'Please Enter Mobile Number',
                'digits' : 'Mobile Number should be only in digits'
            },
            dateBirth : "Please select Date of Birth",
            dateJoin : "Please select Date of join",
        },
    });
    $('#dateBirth').datepicker({format: 'yyyy-m-d', autoclose: true, 
        todayHighlight: true});
    $('#dateJoin').datepicker({format: 'yyyy-m-d', autoclose: true, 
        todayHighlight: true});
});
function initlizeKnockout(){

    corporateViewModel = new corporateViewModel();
    viewModelName = corporateViewModel;
    ko.applyBindings(corporateViewModel);
}
function EmployeeViewModel(){
    self= this;
    self.imagePathEmployee = ko.observable(pathEmployee);
    self.current_id = ko.observable(); 
    self.status=ko.observable(0);
    self.total=ko.observable(0);
    self.action=ko.observable("add");
    self.selectedGender =ko.observable("Male");
    self.employee=ko.observableArray([]);
    self.button_title = ko.observable("ADD");
    self.imageUri = ko.observable("");
    self.profile = ko.observable("");
    self.employees = ko.observableArray([]);
    self.employeeList = ko.observable(true);
    self.employeeNoData = ko.observable(false);
    self.addEmployeeContainer = ko.observable(false);
    self.createEmployeeClick = function(){
        if(!$("#form_employee").valid()){
            return false;
        }
        var datavalue = {};
        var setting = {};
        datavalue['first_name'] = $('#txtFirstName').val();
        datavalue['last_name'] = $('#txtLastName').val();
        datavalue['gender'] = corporateViewModel.employeeViewModel.selectedGender();
        datavalue['designation'] = $('#txtDesignation').val();
        datavalue['contact'] = $('#txtContact').val();
        datavalue['email'] = $('#txtEmailEmployee').val();
        datavalue['joining_date'] = $('#dateJoin').val();
        datavalue['birth_date'] = $('#dateBirth').val();
        if(corporateViewModel.employeeViewModel.imageUri()!=""){

            datavalue['profile_pic'] = corporateViewModel.employeeViewModel.imageUri();
        }
        datavalue['corporate_id'] = corporateViewModel.current_id();
        if(corporateViewModel.employeeViewModel.action()=='add'){
            employeeApi = employeeAddApi
        } else if(corporateViewModel.employeeViewModel.action()=='edit'){
            employeeApi = employeeUpdateApi
            datavalue['id'] = corporateViewModel.employeeViewModel.current_id();
        }

        setting.url  = employeeApi;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id+"|"+token);
        setting.success = function(response){
           resetEmployee();
           corporateViewModel.employeeViewModel.employeeList(true);
           corporateViewModel.employeeViewModel.addEmployeeContainer(false);
           getEmployees();
        };
        setting.error = function(jqXHR, textStatus, error){
            swal(jqXHR.responseJSON.message,"",'error');
            return false;
            };
        apiAjax(setting);
    
    }
    self.editEmployeeClick   = function(employee){
    
        corporateViewModel.employeeViewModel.addEmployeeContainer(true);
        corporateViewModel.employeeViewModel.employeeList(false);
        $('#txtFirstName').val(employee.first_name());
        $('#txtLastName').val(employee.last_name());
        $('#txtDesignation').val(employee.designation());
        $('#txtContact').val(employee.contact());
        $('#txtEmailEmployee').val(employee.email());
        $('#dateJoin').val(employee.joining_date());
        $('#dateBirth').val(employee.birth_date());
        corporateViewModel.employeeViewModel.profile(employee.profile_pic());
        corporateViewModel.employeeViewModel.current_id(employee.id());
        corporateViewModel.employeeViewModel.action("edit");
        corporateViewModel.employeeViewModel.button_title("UPDATE");
        corporateViewModel.employeeViewModel.selectedGender(employee.gender());
    }
    self.addEmployeeClick = function(){
        corporateViewModel.employeeViewModel.employeeList(false);
        corporateViewModel.employeeViewModel.employeeNoData(false);
        corporateViewModel.employeeViewModel.addEmployeeContainer(true);
    };
}
function corporateViewModel(){
    self = this;
    self.current_id = ko.observable(""); 
    self.current_uid = ko.observable("");
    self.limit =ko.observable(200);
    self.corporates = ko.observableArray([]);
    self.selectedCategory= ko.observable("");
    self.availbleCategory= ko.observableArray([]);
    self.selectedStatus= ko.observable(0);
    self.availbleStatus= [{
        text: "Active",
        value: 1
      }, {
        text: "Deactive",
        value: 0
      }];
    self.total = ko.observable(0);
    self.corporateListContainer =  ko.observable(true);
    self.corporateList =  ko.observable(false);
    self.corporateNoData = ko.observable(true);
    self.corporateAddContainer =ko.observable(false);
    self.addCorporateClick = function(){
        corporateViewModel.corporateListContainer(false);
        corporateViewModel.corporateAddContainer(true);
        corporateViewModel.action_title("Add");
        corporateViewModel.action_name("Add Corporate");
        corporateViewModel.action("add");
    };
    self.action =ko.observable("");
    self.action_title = ko.observable("");
    self.employeeCountClick = function(corporate){
        location.href=employeeWebList+"?id="+corporate.id();
    }
    self.editCorporateClick =function(corporate){
        corporateViewModel.corporateListContainer(false);
        corporateViewModel.corporateAddContainer(true);
        corporateViewModel.action_title("Edit");
        corporateViewModel.action_name("Edit Corporate");
        corporateViewModel.action("edit");
        $('#txtCorporateName').val(corporate.name());
        $('#txtCompany_size').val(corporate.company_size());
        $('#txtDescription').val(corporate.description());
        $('#txtEmail').val(corporate.email());
        $('#txtMobile').val(corporate.contact());
        $('#txtPassword').val("");
        $('#txtPassword').attr("disabled","disabled");
       
        $('#txtPersonName').val(corporate.contact_person_name());
        $('#txtWebsiteUrl').val(corporate.website_url());
        $('#txtFacebookPage').val(corporate.facebook_page());
        $('#txtAddress').val(corporate.address());

        corporateViewModel.selectedCategory(corporate.category());
        corporateViewModel.selectedStatus(corporate.status());
        corporateViewModel.current_id(corporate.id());
        corporateViewModel.current_uid(corporate.uid());
        corporateViewModel.profile(corporate.logo());
        $("#employeetab").attr("data-toggle", "tab");
        getEmployees();
    };
    self.deleteCorporateClick = function(corporate){
        corporateViewModel.current_id(corporate.id());
        $('#deleteModal').modal('show');
    }
    self.deleteConfirm = function(){
        var setting = {};
        var datavalue = {
            'id' : corporateViewModel.current_id(),
        };
        setting.url  = deleteCorporateApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id+"|"+token);
        setting.success = function(response){
            $('#deleteModal').modal('hide');
            swal(response.message,"","success");
            // getCorporates();
            location.href = corporateWebList;

            
        };
        setting.error = function(jqXHR, textStatus, error){
            swal(jqXHR.responseJSON.message,"","error");
            return false;
        };
        apiAjax(setting);
    };

    self.createCorporateClick = function(){
        if(!$("#form_corporate").valid()){
            return false;
        }
            
        var datavalue = {};
        
       
        if(corporateViewModel.action()=="edit"){
            datavalue['id'] = corporateViewModel.current_id();
            datavalue['uid'] = corporateViewModel.current_uid();
            corporateApiPath = updateCorporateApiPath;
        } else if(corporateViewModel.action()=="add"){
            corporateApiPath = addCorporateApiPath;
            datavalue['password'] = $.md5($('#txtPassword').val());
        }
        var setting = {};
        datavalue['name'] = $('#txtCorporateName').val();
        datavalue['company_size'] = $('#txtCompany_size').val();
        datavalue['description'] = $('#txtDescription').val();
        datavalue['email'] = $('#txtEmail').val();
        datavalue['contact'] = $('#txtMobile').val();
        datavalue['person_name'] = $('#txtPersonName').val();
        datavalue['website_url'] = $('#txtWebsiteUrl').val();
        datavalue['facebook_page'] = $('#txtFacebookPage').val();
        datavalue['linkedin_page'] = $('#txtLinkedInPage').val();
        datavalue['address'] = $('#txtAddress').val();
        datavalue['category'] = corporateViewModel.selectedCategory();
        datavalue['status'] = corporateViewModel.selectedStatus();
        if(corporateViewModel.imageUri()!=""){
            datavalue['image'] = corporateViewModel.imageUri();
        }
        setting.url  = corporateApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id+"|"+token);
        setting.success = function(response){
           // getCorporates();
            if(corporateViewModel.action()=="edit"){
                location.href = corporateWebList;
            }
           // resetAll();
            // corporateViewModel.current_id(response.data.id);
            // $("#employeetab").attr("data-toggle", "tab");
            // $("#employee").addClass("in active");
            // $("#employeetab").parent().addClass("active");
            // $("#corporate_details").removeClass("in active");
            // $("#corporate_detailstab").parent().removeClass("active");
            // getEmployees();
        };
        setting.error = function(jqXHR, textStatus, error){
                if(jqXHR.status==422){
                    
                    $.each(jqXHR.responseJSON.errors,function(i,item){
                        var fieldname;
                        tail = i.substring( 1 );
                        fieldName = i.substring( 0, 1 ).toUpperCase() + tail; 
                        fieldname ="txt"+fieldName;
                        $("#"+fieldname ).after(function() {
                            return "<label id='"+fieldname+"'-error' class='error' for='"+fieldname+"'>"+item+"</label>";
                        });
                    })
                } else {
                    swal(jqXHR.responseJSON.message,"","error");
                    return false;
                }
            };
        apiAjax(setting);
    };
    self.activeClick =function(corporate){
        var setting = {};
        var datavalue = {
            'id' : corporate.uid(),
        };
        setting.url  = activeCorporateApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id+"|"+token);
        setting.success = function(response){
            swal(response.message,"","success");
            // getCorporates();
            location.href = corporateWebList;
            
        };
        setting.error = function(jqXHR, textStatus, error){
            swal(jqXHR.responseJSON.message,"","error");
            return false;
        };
        apiAjax(setting);
    }
    self.deactiveClick =function(corporate){
        var setting = {};
        var datavalue = {
            'id' : corporate.uid(),
        };
        setting.url  = deactiveCorporateApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id+"|"+token);
        setting.success = function(response){
            swal(response.message,"","success");
            // getCorporates();
            location.href = corporateWebList;
            
        };
        setting.error = function(jqXHR, textStatus, error){
            swal(jqXHR.responseJSON.message,"","error");
            return false;
        };
        apiAjax(setting);
    }
    self.action_name = ko.observable("");
     /*Image Upload*/
    self.imageUri = ko.observable("");
    self.imagePath = ko.observable(path);
    self.profile = ko.observable("");
    /*Employee*/
    self.employeeViewModel = new EmployeeViewModel;
    
}
function resetAll(){
    $('#txtCorporateName').val("");
    $('#fileInput').val("");
    $('#txtCategory').val("");
    $('#txtCompany_size').val("");
    $('#txtDescription').val("");
    $('#txtPersonName').val("");
    $('#txtEmail').val("");
    $('#txtMobile').val("");
    $('#txtPassword').removeAttr("disabled");
    $('#txtPassword').val("");
    $('#txtWebsiteUrl').val("");
    $('#txtLinkedInPage').val("");
    $('#txtFacebookPage').val("");
    $('#txtAddress').val("");
}
function getCorporates(){
    var setting = {};
    var datavalue = {
        'limit' : corporateViewModel.limit(),
    };
    setting.url  = corporateListApiPath;
    setting.type = 'GET';
    setting.data = datavalue;
    setting.token = window.btoa(id+"|"+token);
    setting.success = function(response){
        corporateViewModel.total(response.data.total);
        var corporate_list = response.data.data;
        if(corporate_list.length>0){
            corporateViewModel.corporates([]);
            $.each(corporate_list, function(i,item) {
                corporateViewModel.corporates.push(new CorporateModel(item));
            });

            $("#corporate_tbl").DataTable( { responsive: true } );
            corporateViewModel.corporateListContainer(true);
            corporateViewModel.corporateAddContainer(false);
            corporateViewModel.corporateList(true);
            corporateViewModel.corporateNoData(false);

        } else {
            corporateViewModel.corporateListContainer(true);
            corporateViewModel.corporateAddContainer(false);
            corporateViewModel.corporateList(false);
            corporateViewModel.corporateNoData(true);
        }
    };
    setting.error = function(jqXHR, textStatus, error){
        swal(jqXHR.responseJSON.message,"","error");
        return false;
    };
    apiAjax(setting);
}
$('#fileInput').change(function(){
    var file = $('#fileInput').prop('files')[0];
    var ImageURL;
    var FR = new FileReader();
    FR.addEventListener("load", function () {
        ImageURL = FR.result;
        var block = ImageURL.split(";");
        // Get the content type of the image
        var contentType = block[0].split(":")[1];// In this case "image/gif"
        // get the real base64 content of the file
        var realData = block[1].split(",")[1];// In this case "R0lGODlhPQBEAPeoAJosM...."
        
        corporateViewModel.imageUri(realData);
    }, false);
    if (file) {
        FR.readAsDataURL(file);
    }
});
$('#fileInputEmployee').change(function(){
   var file = $('#fileInputEmployee').prop('files')[0];
   var ImageURL;
   var FR = new FileReader();
   FR.addEventListener("load", function () {
        ImageURL = FR.result;
        var block = ImageURL.split(";");
        // Get the content type of the image
        var contentType = block[0].split(":")[1];// In this case "image/gif"
        // get the real base64 content of the file
        var realData = block[1].split(",")[1];// In this case "R0lGODlhPQBEAPeoAJosM...."
        // Convert it to a blob to upload
        // var blob = b64toBlob(    , contentType);
        corporateViewModel.employeeViewModel.imageUri(realData);
   }, false);
   if (file) {
        FR.readAsDataURL(file);
   }
});
function getEmployees() {
    var setting = {};
    var datavalue = {
        'limit': 1000,
        'cid':corporateViewModel.current_id()
    };
    
    setting.url = employeeListApiPath;
    setting.type = 'GET';
    setting.data = datavalue;
    setting.token = window.btoa(id + "|" + token);
    setting.success = function (response) {
        var employee_list = response.data.data;
        if (employee_list.length > 0) {
            corporateViewModel.employeeViewModel.employees([]);
            $.each(employee_list, function (i, item) {
                corporateViewModel.employeeViewModel.employees.push(new EmployeeModel(item));
            });
            corporateViewModel.employeeViewModel.employeeList(true);
            corporateViewModel.employeeViewModel.employeeNoData(false);

        } else {
            corporateViewModel.employeeViewModel.employeeList(false);
            corporateViewModel.employeeViewModel.employeeNoData(true);
        }
    };
    setting.error = function (jqXHR, textStatus, error) {
        swal(jqXHR.responseJSON.message, "", "error");
        return false;
    };
    apiAjax(setting);
}
function resetEmployee(){
    $('#txtFirstName').val("");
    $('#txtLastName').val("");
    $('#txtDesignation').val("");
    $('#txtContact').val("");
    $('#txtEmail').val("");
    $('#dateJoin').val("");
    $('#dateBirth').val("");
    $('#fileInput').val("");
}
function getCategories() {
    var setting = {};
    setting.url = getCategoriesApiPath;
    setting.type = 'GET';
    setting.data = {'limit': 1000, 'type': 2, 'status': 1};
    setting.token = window.btoa(id + "|" + token);
    setting.success = function (response) {
        var category_list = response.data.data;
        if (category_list.length > 0) {
            $.each(category_list, function (i, item) {
                corporateViewModel.availbleCategory.push(new availableCategory(item.id, item.name));
            });
        }
    };
    setting.error = function (jqXHR, textStatus, error) {
        swal(jqXHR.responseJSON.message, "", "error");
        return false;
    };
    apiAjax(setting);
}
function availableCategory(k, v) {

    var self = this;
    self.value = ko.observable(k);
    self.text = ko.observable(v);

}