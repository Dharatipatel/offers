var viewModelName;
var brandViewModel;
var brandApiPath;
$(document).ready(function () {
    $('.has-submenu').removeClass('active');
    $('#brandTab').addClass('active');

    initlizeKnockout();
    if (type == 'add') {
        $('#brandAddTab').addClass('active');
        $('#brandListTab').removeClass('active');
        brandViewModel.brandListContainer(false);
        brandViewModel.brandAddContainer(true);
        brandViewModel.action_title("Add");
        brandViewModel.action_name("Add Brand");
        brandViewModel.action("add");
        getCountries(0);
    } else if (type == 'list') {
        $('#brandListTab').addClass('active');
        $('#brandAddTab').removeClass('active');
        brandViewModel.brandAddContainer(false);
        brandViewModel.brandListContainer(true);
        getBrands();
    }
    getCategories();
    var rules = {
        txtBrandName: "required",
        txtCategory: "required",
        txtDescription: "required",
        txtEmail: {
            'required': true,
            'email': true,
        },
        txtContact: {
            'required': true,
            'digits': true
        },
        txtPassword: "required",
        txtBusinessHours: "required",
        txtPersonName: "required",
        txtWebsiteUrl: {
            "required": true,
        },
        txtFacebookPage: "required",
        txtStatus: "required",
    };
    var messages = {
        txtBrandName: "Please Enter a brand name",
        txtCategory: "Please select  a category",
        txtDescription: "Please Enter  a description",
        txtEmail: {
            'required': 'Please Enter Email',
            'email': 'Please Enter a valid email address'
        },
        txtContact: {
            'required': 'Please Enter mobile',
            'digits': 'Mobile should be only in digits'
        },
        txtPassword: "Please Enter password",
        txtBusinessHours: "Please Enter Business Hours",
        txtPersonName: "Please Enter person name",
        txtWebsiteUrl: {
            'required': "Please Enter url",
        },
        txtFacebookPage: "Please Enter facebook page",
        txtStatus: "Please select  a Status",
    };
    $("#form_brand").validate({
        rules: rules,
        messages: messages,
    });
    $("#form_location").validate({
        rules: {
            txtAddress: "required",
            txtPincode: "required",
            txtCountry: "required",
            txtState: "required",
            txtCity: "required",
        },
        messages: {
            txtAddress: "Please Enter address",
            txtPincode: "Please Enter pincode",
            txtCountry: "Please Enter Country name",
            txtState: "Please Enter State name",
            txtCity: "Please Enter City name",
        },
    });
    $("#form_Offer").validate({
        rules:{
            txtTitle : "required",
            txtOfferDescription : "required",
            txtValue: {
                'required' :true,
                'digits' :true,
            },
            txtPrice: {
                'required' :true,
                'digits': true
            },
            txtApplicableOn:"required",
            fixDate : { 
                required:'#txtApplicableOn1[value=1]:checked',
            },
            validTo : { 
                required :'#txtApplicableOn2[value=2]:checked',
            },
            'address[]' :{
                  required: true,
            },
            count :{
                "required" :true,
                "digits" :true,
            },
            publishDate : "required",
            notes : "required",
        },
        messages: {
            txtTitle: "Please enter Offer title",
            txtOfferDescription: "Please enter Offer description",
            txtValue: {
                'required' : 'Please Enter Offer Value',
                'digits' : 'Please enter Offer Value in digits'
            },
            txtPrice: {
                'required' : 'Please Enter Offer Price',
                'digits' : 'Offer Price should be only in digits'
            },
            txtApplicableOn : "Please select Applicable Place",
            fixDate : "Please select Fix date",
            validTo : "Please select Valid To date",
            'address[]': {
                required: "You must check at least 1 Address",
            },
            count:{
                'required' : 'Please Enter Offer Count',
                'digits' : 'Offer Count should be only in digits'
            },
            publishDate:"Please Select Publish Date",
            notes:"Please Enter Important Notes",

        },
    });
    $('#fixDate').datepicker({startDate: '-0d',format: 'yyyy-m-d', autoclose: true, 
        todayHighlight: true});
    $('#validTo').datepicker({startDate: '-0d',format: 'yyyy-m-d', autoclose: true, 
        todayHighlight: true});
    $('#publishDate').datepicker({startDate: '-0d',format: 'yyyy-m-d', autoclose: true, 
        todayHighlight: true});
});
function initlizeKnockout() {
    brandViewModel = new BrandViewModel();
    viewModelName = brandViewModel;
    ko.applyBindings(brandViewModel);
}
function OfferViewModel(){
    self=this;
    self.status=ko.observable(1);
    self.offerListContainer =ko.observable(true);
    self.offerAddContainer = ko.observable(false);
    self.offers=ko.observableArray([]);
    self.offerList = ko.observable(false);
    self.offerNoData = ko.observable(false);
    self.fixDateVisibilty =ko.observable(true);
    self.validToVisibilty =ko.observable(false);
    self.address = ko.observableArray([]);
    self.choosenAddress = ko.observableArray([]);
    self.actionContainer = ko.observable(true);
    self.offerTypeValue = ko.observable("1");
    self.createOfferClick = function(){
        if(!$("#form_Offer").valid()){
            return false;
        }
        var datavalue = {};
        var applicableDate = '';
        var title = $('#txtTitle').val();
        var description = $('#txtOfferDescription').val();
        var value = $('#txtValue').val();
        var count = $('#count').val();
        var publish_date = $('#publishDate').val();
        var notes = $('#notes').val();
        var applicableOn = $("input[name='txtApplicableOn']:checked").val()
        var offerType = $("input[name='txtOfferType']:checked").val();
            if(applicableOn == 1){
                applicableDate = $('#fixDate').val();
            } else {
                applicableDate = $('#validTo').val();
            }
        if(!moment(applicableDate, 'YYYY-MM-DDTHH:mm:ssZ').isAfter(moment($('#publishDate').val(),'YYYY-MM-DDTHH:mm:ssZ'))){
             
            $("#publishDate").after(function() {
                return "<label id='publishDate'-error' class='error' for='publishDate'>Publish Date can not grater than Fix date or Expiry Date</label>";
            });
            return false;        
        }
        var setting = {};
        datavalue['title'] = title;
        datavalue['description'] = description;
        datavalue['value'] = value;
        datavalue['offer_type'] = offerType;
        datavalue['brand_address_id'] = brandViewModel.offerViewModel.choosenAddress();
        datavalue['apply_on'] = applicableOn;
        datavalue['valid_to'] = applicableDate;
        datavalue['count'] = count;
        datavalue['publish_date'] = publish_date;
        datavalue['notes'] = notes;
        datavalue['brand_id'] = brandViewModel.current_id();
        datavalue['ads_count'] = $('#txtAds_count').val();
       

        setting.url  = addOfferApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id+"|"+token);
        setting.success = function(response){
           resetAll();
           getOffers();
           brandViewModel.offerViewModel.offerAddContainer(false);
            brandViewModel.offerViewModel.offerListContainer(true);
            brandViewModel.offerViewModel.actionContainer(true);
        };
        setting.error = function(jqXHR, textStatus, error){
            swal(jqXHR.responseJSON.message,"",'error');
            return false;
            };
        apiAjax(setting);
    };
    self.addOfferClick = function(){
        brandViewModel.offerViewModel.offerAddContainer(true);
        brandViewModel.offerViewModel.offerListContainer(false);
        brandViewModel.offerViewModel.actionContainer(false);
    };    
    self.offerActiveClick = function(){
        brandViewModel.offerViewModel.status(1);
        getOffers();
    }
    self.offerPastClick = function(){
        brandViewModel.offerViewModel.status(6);
        getOffers();
    }
    self.offerPendingClick = function(){
        brandViewModel.offerViewModel.status(3);
        getOffers();
    }
    self.approveClick = function(offer){
        var setting = {};
        var datavalue = {
            'id' : offer.id(),
        };
        setting.url  = offerApproveApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id+"|"+token);
        setting.success = function(response){
            swal(response.message,"",'success');
            getOffers();
            
        };
        setting.error = function(jqXHR, textStatus, error){
            swal(jqXHR.responseJSON.message,"","error");
            return false;
        };
        apiAjax(setting);
    }
    self.deactiveClick = function(offer){
        var setting = {};
        var datavalue = {
            'id' : offer.id(),
        };
        setting.url  = offerDeactiveApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id+"|"+token);
        setting.success = function(response){
            swal(response.message,"",'success');
            getOffers();
        };
        setting.error = function(jqXHR, textStatus, error){
            swal(jqXHR.responseJSON.message,"","error");
            return false;
        };
        apiAjax(setting);
    }
}
function BrandViewModel() {
    self = this;
    self.current_id = ko.observable("");
    self.current_uid = ko.observable("");
    self.limit = ko.observable(200);
    self.brands = ko.observableArray([]);
    self.selectedCategory = ko.observable("");
    self.availbleCategory = ko.observableArray([]);
    self.selectedCountry = ko.observable("");
    self.availbleCountries = ko.observableArray([]);
    self.Countries = ko.observableArray([]);
    self.States = ko.observableArray([]);
    self.Cities = ko.observableArray([]);
    self.address = ko.observableArray([]);
    self.selectedState = ko.observable("");
    self.availbleStates = ko.observableArray([]);
    self.selectedCity = ko.observable("");
    self.availbleCities = ko.observableArray([]);
    self.selectedStatus = ko.observable(0);
    self.availbleStatus = [{
            text: "Active",
            value: 1
        }, {
            text: "Deactive",
            value: 0
        }];

    self.total = ko.observable(0);
    self.brandListContainer = ko.observable(true);
    self.brandList = ko.observable(false);
    self.brandNoData = ko.observable(true);
    self.brandAddContainer = ko.observable(true);
    self.action = ko.observable("");
    self.action_title = ko.observable("");
    self.actionAddress = ko.observable("Add Address");
    
    self.emailBrandClick = function (brand) {
        var id = brand.id();
        $("#labelEmail").html(brand.email());
        $("#hiddenBrandId").val(id);
        $("#emailModal").modal("toggle");
        $("#txtSendPassword").val("");
    };
    
    self.emailSendClick = function (brand) {
        var setting = {};
        var datavalue = {
            'id': $("#hiddenBrandId").val(),
            "password": $("#txtSendPassword").val()
        };
        setting.url = emailSendApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id + "|" + token);
        setting.success = function (response) {
            $('#emailModal').modal('toggle');
            swal(response.message, "", "success");
        };
        setting.error = function (jqXHR, textStatus, error) {
            swal(jqXHR.responseJSON.message, "", "error");
            return false;
        };
        apiAjax(setting);
    };
    self.addBrandClick = function () {

        brandViewModel.brandListContainer(false);
        brandViewModel.brandAddContainer(true);
        brandViewModel.action_title("Add");
        brandViewModel.action_name("Add Brand");
        brandViewModel.action("add");
        getCountries(0);

    };

    self.editBrandClick = function (brand) {
        brandViewModel.brandListContainer(false);
        brandViewModel.brandAddContainer(true);
        brandViewModel.action_title("Edit");
        brandViewModel.action_name("Edit Brand");
        brandViewModel.actionAddress("Edit Address");
        brandViewModel.action("edit");
        $('#txtBrandName').val(brand.name());
        $('#txtDescription').val(brand.description());
        $('#txtEmail').val(brand.email());
        $('#txtContact').val(brand.contact());
        $('#txtPassword').val("");
        $('#txtPassword').attr("disabled", "disabled");
        $('#txtBusinessHours').val(brand.business_hours());
        $('#txtPersonName').val(brand.contact_person_name());
        $('#txtWebsiteUrl').val(brand.website_url());
        $('#txtFacebookPage').val(brand.facebook_page());
        $("#locationtab").attr("data-toggle", "tab");
        $("#offerstab").attr("data-toggle", "tab");
        getAddress(brand.id());
        brandViewModel.selectedCategory(brand.categoryid());
        brandViewModel.selectedStatus(brand.status());
        brandViewModel.current_id(brand.id());
        brandViewModel.current_uid(brand.uid());
        brandViewModel.profile(brand.logo());
        getOffers();
        getAddressList();
    };
    self.offerListClick = function (brand) {
        location.href = offerListWebPath + "?id=" + brand.id();
    }
    self.removeAddress = function (index) {
        brandViewModel.fields.splice(index, 1);
    }
    self.deleteBrandClick = function (category) {
        brandViewModel.current_id(category.id());
        $('#deleteModal').modal('show');
    }
    self.deleteConfirm = function () {
        var setting = {};
        var datavalue = {
            'id': brandViewModel.current_id(),
        };
        setting.url = deleteBrandApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id + "|" + token);
        setting.success = function (response) {
            $('#deleteModal').modal('hide');
            swal(response.message, "", "success");
            // getBrands();
            location.href = brandListWebPath;
        };
        setting.error = function (jqXHR, textStatus, error) {
            swal(jqXHR.responseJSON.message, "", "error");
            return false;
        };
        apiAjax(setting);
    };
    self.createAddressClick = function () {
        // if (!$("#form_location").valid()) {
        //     return false;
        // }
        if (brandViewModel.action() == "edit") {
            brandAddressApiPath = updateAddressApiPath;
        } else if (brandViewModel.action() == "add") {
            datavalue['password'] = $.md5(password);
            brandAddressApiPath = addAddressApiPath;
        }
        var address_fields = $("[id^=addressDiv]").length;
        var address = {};
        for (var i = 0; i < address_fields; i++) {
            address[i] = {
                "address": brandViewModel.fields()[i].items[0].address(),
                "pincode": brandViewModel.fields()[i].items[0].pincode(),
                "state": brandViewModel.fields()[i].items[0].state(),
                "city": brandViewModel.fields()[i].items[0].city(),
                "country": brandViewModel.fields()[i].items[0].country()
            };
        }
        var setting = {};
        var datavalue = {};
        datavalue['bid'] = brandViewModel.current_id();
        datavalue['addressArray'] = address;

        setting.url = brandAddressApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id + "|" + token);
        setting.success = function (response) {
            // getBrands();
            // location.href = brandListWebPath;
            $("#location").removeClass("in active");
            $("#locationtab").parent().removeClass("active");
            $("#offers").addClass("in active");
            $("#offerstab").parent().addClass("active");
            // resetAll();
        };
        setting.error = function (jqXHR, textStatus, error) {
            if (jqXHR.status == 422) {
                $.each(jqXHR.responseJSON.errors, function (i, item) {
                    var fieldname;
                    tail = i.substring(1);
                    fieldName = i.substring(0, 1).toUpperCase() + tail;
                    fieldname = "txt" + fieldName;
                    $("#" + fieldname).after(function () {
                        return "<label id='" + fieldname + "'-error' class='error' for='" + fieldname + "'>" + item + "</label>";
                    });
                })
                return false;
            } else {
                swal(jqXHR.responseJSON.message, "", 'error');
                return false;
            }
        };
        apiAjax(setting);
    }
    self.createBrandClick = function () {

        if (!$("#form_brand").valid()) {
            return false;
        }
        var datavalue = {};
        var brand_name = $('#txtBrandName').val();
        var category_name = brandViewModel.selectedCategory();
        var description = $('#txtDescription').val();
        var email = $('#txtEmail').val();
        var mobile = $('#txtContact').val();
        var password = $('#txtPassword').val();
        var business_hours = $('#txtBusinessHours').val();
        var person_name = $('#txtPersonName').val();
        var website_url = $('#txtWebsiteUrl').val();
        var facebook_page = $('#txtFacebookPage').val();
        if (brandViewModel.action() == "edit") {
            datavalue['id'] = brandViewModel.current_id();
            datavalue['uid'] = brandViewModel.current_uid();
            brandApiPath = updateBrandApiPath;
        } else if (brandViewModel.action() == "add") {
            datavalue['password'] = $.md5(password);
            brandApiPath = addBrandApiPath;
        }
        var setting = {};
        datavalue['brand_name'] = brand_name;
        datavalue['category_name'] = category_name;
        datavalue['description'] = description;
        datavalue['email'] = email;
        datavalue['contact'] = mobile;
        datavalue['business_hours'] = business_hours;
        datavalue['person_name'] = person_name;
        datavalue['website_url'] = website_url;
        datavalue['facebook_page'] = facebook_page;
        datavalue['status'] = brandViewModel.selectedStatus();
        datavalue['image'] = brandViewModel.imageUri();
        setting.url = brandApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id + "|" + token);
        setting.success = function (response) {
            // getBrands();
            location.href = brandListWebPath;
            resetAll();
            // getOffers();
        };
        setting.error = function (jqXHR, textStatus, error) {
            if (jqXHR.status == 422) {
                $.each(jqXHR.responseJSON.errors, function (i, item) {
                    var fieldname;
                    tail = i.substring(1);
                    fieldName = i.substring(0, 1).toUpperCase() + tail;
                    fieldname = "txt" + fieldName;
                    $("#" + fieldname).after(function () {
                        return "<label id='" + fieldname + "'-error' class='error' for='" + fieldname + "'>" + item + "</label>";
                    });
                })
                return false;
            } else {
                swal(jqXHR.responseJSON.message, "", 'error');
                return false;
            }
        };
        apiAjax(setting);
    };
    self.activeClick = function (brand) {
        var setting = {};
        var datavalue = {
            'id': brand.uid(),
        };
        setting.url = activeBrandApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id + "|" + token);
        setting.success = function (response) {
            swal(response.message, "", 'success');
            location.href = brandListWebPath;
        };
        setting.error = function (jqXHR, textStatus, error) {
            swal(jqXHR.responseJSON.message, "", "error");
            return false;
        };
        apiAjax(setting);
    }
    self.deactiveClick = function (brand) {
        var setting = {};
        var datavalue = {
            'id': brand.uid(),
        };
        setting.url = deactiveBrandApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id + "|" + token);
        setting.success = function (response) {
            swal(response.message, "", 'success');
            location.href = brandListWebPath;

        };
        setting.error = function (jqXHR, textStatus, error) {
            swal(jqXHR.responseJSON.message, "", "error");
            return false;
        };
        apiAjax(setting);
    }
    self.action_name = ko.observable("");
    self.countryChanged = function (c, event) {
        getStates($("#" + event.target.id).val(), event.target.getAttribute('data-id'));
    }
    self.stateChanged = function (c, event) {
        getCities($("#" + event.target.id).val(), event.target.getAttribute('data-id'));
    }
    // brandViewModel.fields()[1].items[0].country()
    self.fields = ko.observableArray([
        {
            items: [
                {
                    address: ko.observable(""),
                    pincode: ko.observable(""),
                    country: ko.observable(""),
                    state: ko.observable(""),
                    city: ko.observable("")
                }
            ]
        },
    ]);
    self.index = ko.observable(0);
    self.addLocationClick = function () {

        brandViewModel.fields.push({items: [{address: ko.observable(""), pincode: ko.observable(""), country: ko.observable(""), state: ko.observable(""),
                    city: ko.observable("")}
            ]
        });
        getCountries(brandViewModel.index(brandViewModel.index() + 1));
    }

    /*Image Upload*/
    self.imageUri = ko.observable("");
    self.imagePath = ko.observable(path);
    self.profile = ko.observable("");

    /*offer*/
    self.offerViewModel = new OfferViewModel;
    
}
function resetAll() {
    $('#txtBrandName').val("");
    $('#txtCategory').val("");
    $('#txtDescription').val("");
    $('#txtEmail').val("");
    $('#txtContact').val("");
    $('#txtPassword').val("");
    $('#txtPassword').removeAttr("disabled");
    $('#txtBusinessHours').val("");
    $('#txtPersonName').val("");
    $('#txtWebsiteUrl').val("");
    $('#txtFacebookPage').val("");
    $('#txtAddress').val("");
    $('#txtPincode').val("");
}
function getBrands() {
    var setting = {};
    var datavalue = {
        'limit': brandViewModel.limit(),
    };
    setting.url = brandListApiPath;
    setting.type = 'GET';
    setting.data = datavalue;
    setting.token = window.btoa(id + "|" + token);
    setting.success = function (response) {
        brandViewModel.total(response.data.total);
        var brannd_list = response.data.data;
        if (brannd_list.length > 0) {
            brandViewModel.brands([]);
            $.each(brannd_list, function (i, item) {
                brandViewModel.brands.push(new BrandModel(item));
            });

            $("#brand_tbl").DataTable({responsive: true});
            brandViewModel.brandListContainer(true);
            brandViewModel.brandAddContainer(false);
            brandViewModel.brandList(true);
            brandViewModel.brandNoData(false);

        } else {
            brandViewModel.brandListContainer(true);
            brandViewModel.brandAddContainer(false);
            brandViewModel.brandList(false);
            brandViewModel.brandNoData(true);
        }
    };
    setting.error = function (jqXHR, textStatus, error) {
        swal(jqXHR.responseJSON.message, "", "error");
        return false;
    };
    apiAjax(setting);
}
function getCategories() {
    var setting = {};
    setting.url = getCategoriesApiPath;
    setting.type = 'GET';
    setting.data = {'limit': 1000, 'type': 1, 'status': 1};
    setting.token = window.btoa(id + "|" + token);
    setting.success = function (response) {
        var category_list = response.data.data;
        if (category_list.length > 0) {
            $.each(category_list, function (i, item) {
                brandViewModel.availbleCategory.push(new availableCategory(item.id, item.name));
            });
        }
    };
    setting.error = function (jqXHR, textStatus, error) {
        swal(jqXHR.responseJSON.message, "", "error");
        return false;
    };
    apiAjax(setting);
}
function availableCategory(k, v) {

    var self = this;
    self.value = ko.observable(k);
    self.text = ko.observable(v);

}
function getCountries(index) {

    var setting = {};
    setting.url = countryListApiPath;
    setting.type = 'GET';
    setting.data = {'status': 1};
    setting.token = window.btoa(id + "|" + token);
    setting.success = function (response) {
        var country_list = response.data.data;
        // brandViewModel.availbleCountries([]);
        if (country_list.length > 0) {
            brandViewModel.availbleCountries()[index] = ko.observableArray([]);
            $.each(country_list, function (i, item) {
                brandViewModel.availbleCountries()[index].push(new availbleCountries(item.id, item.name));
            });
            brandViewModel.Countries.splice(index, 0, brandViewModel.availbleCountries()[index]);
        }
    };
    setting.error = function (jqXHR, textStatus, error) {
        swal(jqXHR.responseJSON.message, "", "error");
        return false;
    };
    apiAjax(setting);
}
function getStates(country, index) {
    var setting = {};
    setting.url = stateListApiPath;
    setting.type = 'GET';
    setting.data = {'country': country, 'status': 1};
    setting.token = window.btoa(id + "|" + token);
    setting.success = function (response) {
        var state_list = response.data.data;
        //brandViewModel.availbleStates([]);
        if (state_list.length > 0) {
            brandViewModel.availbleStates()[index] = ko.observableArray([]);
            $.each(state_list, function (i, item) {
                brandViewModel.availbleStates()[index].push(new availbleStates(item.id, item.name));
            });
            brandViewModel.States.splice(index, 0, brandViewModel.availbleStates()[index]);
        }
    };
    setting.error = function (jqXHR, textStatus, error) {
        swal(jqXHR.responseJSON.message, "", "error");
        return false;
    };
    apiAjax(setting);

}
function getCities(state, index) {

    var setting = {};
    setting.url = cityListApiPath;
    setting.type = 'GET';
    setting.data = {'state': state, 'status': 1};
    setting.token = window.btoa(id + "|" + token);
    setting.success = function (response) {

        var city_list = response.data.data;
        // brandViewModel.availbleCities([]);
        if (city_list.length > 0) {
            brandViewModel.availbleCities()[index] = ko.observableArray([]);
            $.each(city_list, function (i, item) {
                brandViewModel.availbleCities()[index].push(new availbleCities(item.id, item.name));
            });
            brandViewModel.Cities.splice(index, 0, brandViewModel.availbleCities()[index]);
        }
    };
    setting.error = function (jqXHR, textStatus, error) {
        swal(jqXHR.responseJSON.message, "", "error");
        return false;
    };
    apiAjax(setting);
}
$('#fileInput').change(function(){
    var file = $('#fileInput').prop('files')[0];
    var ImageURL;
    var FR = new FileReader();
    FR.addEventListener("load", function () {
        ImageURL = FR.result;
        var block = ImageURL.split(";");
        // Get the content type of the image
        var contentType = block[0].split(":")[1];// In this case "image/gif"
        // get the real base64 content of the file
        var realData = block[1].split(",")[1];// In this case "R0lGODlhPQBEAPeoAJosM...."
        
        brandViewModel.imageUri(realData);
    }, false);
    if (file) {
        FR.readAsDataURL(file);
    }
});
function availbleCountries(k, v) {

    var self = this;
    self.value = ko.observable(k);
    self.text = ko.observable(v);

}
function availbleStates(k, v) {

    var self = this;
    self.value = ko.observable(k);
    self.text = ko.observable(v);

}
function availbleCities(k, v) {

    var self = this;
    self.value = ko.observable(k);
    self.text = ko.observable(v);

}
function getAddress(bid) {
    var setting = {};
    var datavalue = {
        'bid': bid,
    };
    setting.url = addressListApiPath;
    setting.type = 'GET';
    setting.data = datavalue;
    setting.token = window.btoa(id + "|" + token);
    setting.success = function (response) {
        var address_list = response.data.data;
        if (address_list.length > 0) {
            $.each(address_list, function (i, item) {
                brandViewModel.address.push(new availableAddress(item.id, item.address, item.city, item.state, item.country, item.pincode, item.city_id, item.state_id, item.country_id));
                brandViewModel.fields.push({items: [{address: ko.observable(""), pincode: ko.observable(""), country: ko.observable(""), state: ko.observable(""),
                            city: ko.observable("")}
                    ]
                });
            });
            brandViewModel.fields.pop();
            var address_fields = brandViewModel.fields().length;
            for (var i = 0; i < address_fields; i++) {
                getCountries(i);
                getStates(brandViewModel.address()[i].country_id(), i);
                getCities(brandViewModel.address()[i].state_id(), i);
                brandViewModel.fields()[i].items[0].address(brandViewModel.address()[i].address()),
                        brandViewModel.fields()[i].items[0].pincode(brandViewModel.address()[i].pincode()),
                        brandViewModel.fields()[i].items[0].state(brandViewModel.address()[i].state_id()),
                        brandViewModel.fields()[i].items[0].city(brandViewModel.address()[i].city_id()),
                        brandViewModel.fields()[i].items[0].country(brandViewModel.address()[i].country_id())
            }

        }
    };
    setting.error = function (jqXHR, textStatus, error) {
        swal(jqXHR.responseJSON.message, "", "error");
        return false;
    };
    apiAjax(setting);
}
function availableAddress(k, address, city, state, country, pincode, city_id, state_id, country_id) {
    var self = this;
    self.value = ko.observable(k);
    self.address = ko.observable(address);
    self.city = ko.observable(city);
    self.state = ko.observable(state);
    self.country = ko.observable(country);
    self.city_id = ko.observable(city_id);
    self.state_id = ko.observable(state_id);
    self.country_id = ko.observable(country_id);
    self.pincode = ko.observable(pincode);
}
function getOffers() {

    var setting = {};
    var datavalue = {};
    datavalue['bid'] = brandViewModel.current_id();  
    datavalue['status'] = brandViewModel.offerViewModel.status();    
    setting.url  = offerListApiPath;

    setting.type = 'GET';
    setting.data = datavalue;
    setting.token = window.btoa(id + "|" + token);
    setting.success = function (response) {
        brandViewModel.total(response.data.total);
        var offer_list = response.data.data;
        if (offer_list.length > 0) {
            brandViewModel.offerViewModel.offers([]);
            $.each(offer_list, function (i, item) {
                brandViewModel.offerViewModel.offers.push(new OfferModel(item));
            });
            brandViewModel.offerViewModel.offerListContainer(true);
            brandViewModel.offerViewModel.offerList(true);
            brandViewModel.offerViewModel.offerNoData(false);

        } else {
            brandViewModel.offerViewModel.offerListContainer(false);
            brandViewModel.offerViewModel.offerList(false);
            brandViewModel.offerViewModel.offerNoData(true);
        }
    };
    setting.error = function (jqXHR, textStatus, error) {
        swal(jqXHR.responseJSON.message, "", "error");
        return false;
    };
    apiAjax(setting);

}
function getAddressList(){
    var setting = {};
    var datavalue = {
        'bid' : brandViewModel.current_id(),
    };
    setting.url  = addressListApiPath;
    setting.type = 'GET';
    setting.data = datavalue;
    setting.token = window.btoa(id+"|"+token);
    setting.success = function(response){
        var address_list = response.data.data;
        if(address_list.length>0){
            $.each(address_list, function(i,item) {
                brandViewModel.offerViewModel.address.push(new availableAddressList(item.id,item.address,item.city,item.state,item.country,item.pincode));
            });
        }
    };
    setting.error = function(jqXHR, textStatus, error){
        swal(jqXHR.responseJSON.message,"","error");
        return false;
    };
    apiAjax(setting);
}
function availableAddressList(k,address,city,state,country,pincode){
    var self = this;
    self.value = ko.observable(k);
    self.textaddress  = ko.observable(address+", "+city+" ,"+state+" ,"+country+" ,"+pincode);
 }

$('input[type=radio][name=txtApplicableOn]').change(function () {
    if (this.value == 1) {
        brandViewModel.offerViewModel.fixDateVisibilty(true);
        brandViewModel.offerViewModel.validToVisibilty(false);
    } else {
        brandViewModel.offerViewModel.fixDateVisibilty(false);
        brandViewModel.offerViewModel.validToVisibilty(true);
    }
});