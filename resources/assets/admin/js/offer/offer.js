var viewModelName;
var offerViewModel;
$(document).ready(function(){
    $('.has-submenu').removeClass('active');
    $('#offerTab').addClass('active');
    initlizeKnockout();
    if(type==1){
        $('#offerActiveTab').addClass('active');
        $('#offerUpcomingTab').removeClass('active');
        $('#offerPendingTab').removeClass('active');
        $('#offerPastTab').removeClass('active');
        offerViewModel.status(1);
        offerListWebPath = offerActiveWebPath;
    }
    else if(type==2){
        $('#offerUpcomingTab').addClass('active');
        $('#offerActiveTab').removeClass('active');
        $('#offerPastTab').removeClass('active');
        $('#offerPendingTab').removeClass('active');
        offerViewModel.status(2);
        offerListWebPath = offerUpcomingWebPath;   
    }
    else if(type==3){
        $('#offerPendingTab').addClass('active');
        $('#offerUpcomingTab').removeClass('active');
        $('#offerActiveTab').removeClass('active');
        $('#offerPastTab').removeClass('active');
        offerViewModel.status(3);
        offerListWebPath = offerPendingWebPath;
    }
    else if(type==6){
        $('#offerPastTab').addClass('active');
        $('#offerActiveTab').removeClass('active');
        $('#offerActiveTab').removeClass('active');
        $('#offerPendingTab').removeClass('active');
        offerViewModel.status(6);
        offerListWebPath = offerPastWebPath;
    }
    getOffers();
   
});
function bestOffer(element){
    var datavalue = {
            'id' : $(element).attr('data-id'),
            
        };
    if($(element).is(":checked")){
        datavalue['bestOffer'] = 1;
    } else {
        datavalue['bestOffer'] = 0;
    }
    var setting = {};

        setting.url  = offerUpdateApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id+"|"+token);
        setting.success = function(response){
            swal(response.message,"",'success');
            location.href = offerListWebPath;
            
        };
        setting.error = function(jqXHR, textStatus, error){
            swal(jqXHR.responseJSON.message,"","error");
            return false;
        };
        apiAjax(setting);

}
function bannerOffer(element){
    var datavalue = {
            'id' : $(element).attr('data-id'),
            
        };
    if($(element).is(":checked")){
        datavalue['bannerOffer'] = 1;
    } else {
        datavalue['bannerOffer'] = 0;
    }
    var setting = {};

        setting.url  = offerUpdateApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id+"|"+token);
        setting.success = function(response){
            swal(response.message,"",'success');
            location.href = offerListWebPath;
            
        };
        setting.error = function(jqXHR, textStatus, error){
            swal(jqXHR.responseJSON.message,"","error");
            return false;
        };
        apiAjax(setting);

}
function initlizeKnockout(){
    offerViewModel = new OfferViewModel();
    viewModelName  = offerViewModel;
    ko.applyBindings(offerViewModel);
}
function OfferViewModel(){
    self=this;
    self.status=ko.observable(1);
    self.total=ko.observable(0);
    self.offers=ko.observableArray([]);
    self.offerList = ko.observable(false);
    self.offerNoData = ko.observable(false);
    self.offerListContainer = ko.observable(true);
    self.approveClick = function(offer){
        var setting = {};
        var datavalue = {
            'id' : offer.id(),
        };
        setting.url  = offerApproveApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id+"|"+token);
        setting.success = function(response){
            swal(response.message,"",'success');
            // getOffers();
            location.href = offerListWebPath;
            
        };
        setting.error = function(jqXHR, textStatus, error){
            swal(jqXHR.responseJSON.message,"","error");
            return false;
        };
        apiAjax(setting);
    };
    self.deactiveClick = function(offer){
        var setting = {};
        var datavalue = {
            'id' : offer.id(),
        };
        setting.url  = offerDeactiveApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id+"|"+token);
        setting.success = function(response){
            swal(response.message,"",'success');
            // getOffers();
            location.href = offerListWebPath;
            
        };
        setting.error = function(jqXHR, textStatus, error){
            swal(jqXHR.responseJSON.message,"","error");
            return false;
        };
        apiAjax(setting);
    };
}
function getOffers(){

    var setting = {};
    var datavalue = {};
    if(brand_id != ""){
        datavalue['bid'] = brand_id;

    }
    datavalue['status'] = offerViewModel.status();
    setting.url  = offerListApiPath;
    setting.type = 'GET';
    setting.data = datavalue;
    setting.token = window.btoa(id+"|"+token);
    setting.success = function(response){
        offerViewModel.total(response.data.total);
        var offer_list = response.data.data;
        if(offer_list.length>0){
            offerViewModel.offers([]);
            $.each(offer_list, function(i,item) {
                offerViewModel.offers.push(new OfferModel(item));
            });
            $("#offer_tbl").DataTable( { responsive: true } );
            offerViewModel.offerList(true);
            offerViewModel.offerNoData(false);

        } else {
            offerViewModel.offerList(false);
            offerViewModel.offerNoData(true);
        }
    };
    setting.error = function(jqXHR, textStatus, error){
        swal(jqXHR.responseJSON.message,"","error");
        return false;
    };
    apiAjax(setting);

}
