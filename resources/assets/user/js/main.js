function ajaxRequest(url, method, type, data, callback) {
    jQuery.ajax({
        url: baseURL + url,
        type: method,
        dataType: type,
        data: data,
    }).done(function (html) {
        callback(html);
    })
            .fail(function (response) {
                var response = JSON.parse(response.responseText);
                var errorString = '<ul>';
                $.each(response, function (key, value) {
                    errorString += '<li>' + value + '</li>';
                });
                errorString += '</ul>';
                showMsg("error", errorString);

            })
            .always(function () {
            });
}

function showMsg(type, msg) {
    if (type == "add") {
        swal("Added!", msg, "success");
    } else if (type == "update") {
        swal("Updated!", msg, "success");
    } else if (type == "deleted") {
        swal("Deleted!", msg, "success");
    } else if (type == "success") {

        swal({
            title: "Success!",
            text: msg,
            html: true,
            type: "success"
        });

    } else if (type == "info") {

        swal({
            title: "Information",
            text: msg,
            html: true,
            type: "info"
        });

    } else {
        swal({
            title: "Error!",
            text: msg,
            html: true,
            type: "error"
        });
    }
}
function copyToClipboard(elementId) {


    var aux = document.createElement("input");
    aux.setAttribute("value", document.getElementById(elementId).innerHTML);
    document.body.appendChild(aux);
    aux.select();
    document.execCommand("copy");

    document.body.removeChild(aux);
    showMsg("success", "Referral link copied!");
}

function deleteRow(id) {
    swal({
        title: "Are you sure?",
        text: "You will not be recover this data!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel plx!",
        closeOnConfirm: false,
        closeOnCancel: true
    },
            function (isConfirm) {
                if (isConfirm) {
                    window.location = $("#del_" + id).data('url');
                }
            });
}

$("#clicker").on("click", function () {
    $("#file").trigger("click");
});

$("#file").change(function ()
{
    thumbnail(this);
});
function thumbnail(input)
{
    if (input.files && input.files[0])
    {
        var reader = new FileReader();
        reader.onload = function (e)
        {
            $('#image').attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    } else
    {
        /*$('#image').attr('src', "");*/
    }
}
setTimeout(function ()
{
    $(".flash").slideUp("fast");
}, 3000);

$("#clicker-2").on("click", function () {
    $("#file-2").trigger("click");
});

$("#file-2").change(function ()
{
    thumbnail(this);
});
function thumbnail(input)
{
    if (input.files && input.files[0])
    {
        var reader = new FileReader();
        reader.onload = function (e)
        {
            $('#image-2').attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    } else
    {
        /*$('#image').attr('src', "");*/
    }
}