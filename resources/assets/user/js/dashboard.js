jQuery(document).ready(function () {
    Highcharts.chart('current-price-graph', {
        chart: {
            type: 'line'
        },
        title: {
            text: 'FXC current price'
        },
        xAxis: {
            categories: chart_data.date
        },
        yAxis: {
            min: 0,
            title: {
                text: 'USD'
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },

        tooltip: {
            headerFormat: '<b>{point.x}</b><br/>',
            pointFormat: '1 FXC = {point.y} USD'
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
                name: 'Date',
                data: chart_data.usd
            }]
    });
});
