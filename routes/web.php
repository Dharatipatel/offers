<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

// Route::get('/', function () {
//     return view('welcome');
// });
Route::namespace('Web\brand')->group(function () {
    Route::get('brand/login', ['as' => 'brandlogin', 'uses' => 'UserController@login']);
});

Route::namespace('Web\brand')->middleware('CheckSessionBrand')->prefix('brand')->group(function () {

    Route::get('dashboard', ['as' => 'dashboard', 'uses' => 'DashboardController@index']);
    Route::get('coupon', ['as' => 'coupon', 'uses' => 'CouponController@index']);
    Route::get('orderhistory', ['as' => 'orderhistory', 'uses' => 'OrderHistoryController@index']);
    Route::prefix('offer')->group(function () {
        Route::get('/', ['as' => 'brandoffer', 'uses' => 'OfferController@index']);
        Route::get('add', ['as' => 'addoffer', 'uses' => 'OfferController@addOffer']);
        Route::get('past', ['as' => 'pastoffer', 'uses' => 'OfferController@pastOffer']);
    });
    Route::prefix('profile')->group(function () {
        Route::get('edit', ['as' => 'editprofile', 'uses' => 'UserController@editProfile']);
        Route::get('view', ['as' => 'viewprofile', 'uses' => 'UserController@viewProfile']);
        Route::get('changepassword', ['as' => 'changepassword', 'uses' => 'UserController@changePassword']);
    });
});

Route::namespace('Web\admin')->group(function () {
    Route::get('admin/login', 'UserController@login');
    Route::get('logout', ['as' => 'logout', 'uses' => 'UserController@logout']);
    Route::post('setsession', 'UserController@setSession');
});

Route::namespace('Web\admin')->middleware('CheckSession')->prefix('admin')->group(function () {
    Route::prefix('category')->group(function () {
        Route::get('brand', ['as' => 'brandcategory', 'uses' => 'CategoryController@brand']);
        Route::get('reward', ['as' => 'rewardcategory', 'uses' => 'CategoryController@reward']);
    });
    Route::prefix('location')->group(function () {
        Route::get('country', ['as' => 'country', 'uses' => 'LocationController@country']);
        Route::get('state', ['as' => 'state', 'uses' => 'LocationController@state']);
        Route::get('city', ['as' => 'city', 'uses' => 'LocationController@city']);
    });
    Route::prefix('brand')->group(function () {
        Route::get('add', ['as' => 'brandadd', 'uses' => 'BrandController@addBrand']);
        Route::get('list', ['as' => 'brandlist', 'uses' => 'BrandController@listBrand']);
    });
    Route::prefix('offer')->group(function () {
        Route::get('pending', ['as' => 'offerpending', 'uses' => 'OfferController@offer']);
        Route::get('active', ['as' => 'offeractive', 'uses' => 'OfferController@offer']);
        Route::get('upcoming', ['as' => 'offerupcoming', 'uses' => 'OfferController@offer']);
        Route::get('past', ['as' => 'offerpast', 'uses' => 'OfferController@offer']);
    });

    Route::get('employee', ['as' => 'employeeadmin', 'uses' => 'EmployeeController@employee']);
    Route::get('sms-templates', ['as' => 'sms.templates', 'uses' => 'SMSTemplateController@index']);
    Route::get('dashboard', ['as' => 'admindashboard', 'uses' => 'DashboardController@index']);
    /*  mail template   */
    Route::prefix('profile')->group(function () {
        Route::get('edit', ['as' => 'adminprofile', 'uses' => 'UserController@editProfile']);
        Route::get('changepassword', ['as' => 'adminchangepassword', 'uses' => 'UserController@changePassword']);
    });
    Route::group(['prefix' => 'mailtemplates'], function () {

        Route::get('/', ['uses' => 'MailtemplateController@index', 'as' => 'admin.mailtemplates.index']);
        Route::get('create', ['uses' => 'MailtemplateController@create', 'as' => 'admin.mailtemplates.create']);
        Route::post('store', ['uses' => 'MailtemplateController@store', 'as' => 'admin.mailtemplates.save']);
        Route::get('list_mailtemplates', ['uses' => 'MailtemplateController@get_list', 'as' => 'admin.mailtemplates.list']);
        Route::get('edit/{id}', ['uses' => 'MailtemplateController@edit', 'as' => 'admin.mailtemplates.edit']);
        Route::post('update/{id?}', ['uses' => 'MailtemplateController@store', 'as' => 'admin.mailtemplates.update']);
        Route::get('delete/{id}', ['uses' => 'MailtemplateController@delete', 'as' => 'admin.mailtemplates.delete']);
        Route::get("preview/{mail_id}", ["uses" => "MailtemplateController@preview", "as" => "admin.mailtemplates.preview"]);
    });

    Route::prefix('slider')->group(function () {
        Route::get('/', ['uses' => 'SliderController@index', 'as' => 'admin.slider.index']);
        Route::get('create', ['uses' => 'SliderController@create', 'as' => 'admin.slider.create']);
        Route::post('store', ['uses' => 'SliderController@store', 'as' => 'admin.slider.save']);
        Route::get('lists', ['uses' => 'SliderController@get_list', 'as' => 'admin.slider.list']);
        Route::get('edit/{id}', ['uses' => 'SliderController@edit', 'as' => 'admin.slider.edit']);
        Route::post('update/{id?}', ['uses' => 'SliderController@store', 'as' => 'admin.slider.update']);
        Route::get('delete/{id}', ['uses' => 'SliderController@delete', 'as' => 'admin.slider.delete']);
    });

    Route::group(['prefix' => 'banner'], function () {

        Route::get('/', ['uses' => 'BannerController@index', 'as' => 'admin.banner.index']);
        Route::get('add', ['uses' => 'BannerController@manage', 'as' => 'admin.banner.add']);
        Route::post('save', ['uses' => 'BannerController@save', 'as' => 'admin.banner.save']);
        Route::get('list', ['uses' => 'BannerController@lists', 'as' => 'admin.banner.list']);
        Route::get('edit/{id}', ['uses' => 'BannerController@manage', 'as' => 'admin.banner.edit']);
        Route::get('delete/{id}', ['uses' => 'BannerController@delete', 'as' => 'admin.banner.delete']);
    });
});

//  sms
Route::group(['prefix' => 'sms'], function () {
    Route::get('/', ['uses' => 'SMSController@index', 'as' => 'sms.index']);
    Route::post('send', ['uses' => 'SMSController@send', 'as' => 'sms.send']);
});

//  payUMoney
Route::group(['prefix' => 'payment'], function () {
    Route::get('/', ['uses' => 'PaymentController@pay', 'as' => 'payment.pay']);
    Route::post('success', ['uses' => 'PaymentController@success', 'as' => 'payment.success']);
    Route::post('fail', ['uses' => 'PaymentController@fail', 'as' => 'payment.fail']);
});

//  Cron
Route::group([], function () {
    Route::get('send-coupon-code', ['uses' => 'CronController@send_coupon_code', 'as' => 'cron.send.coupon.code']);
});

//  User
Route::group([], function () {
    Route::group(['prefix' => 'user'], function () {
        Route::get('login', ['uses' => 'Auth\LoginController@show', 'as' => 'auth.user.login.show']);
        Route::get('logout', ['uses' => 'Auth\LoginController@user_logout', 'as' => 'auth.user.logout']);
        Route::post('login', ['uses' => 'Auth\LoginController@login', 'as' => 'auth.user.login']);

        Route::get('register', ['uses' => 'Auth\RegisterController@show', 'as' => 'auth.user.register.show']);
        Route::post('register', ['uses' => 'Auth\RegisterController@register', 'as' => 'auth.user.register']);

        Route::group(['prefix' => 'offers'], function () {
            Route::get('/', ['uses' => 'User\OfferController@index', 'as' => 'user.offers']);
            Route::get('brand/{id}', ['uses' => 'User\OfferController@brand_offers', 'as' => 'user.offers.brand']);
        });
    });
});

